/* _fileOpen.cpp							2014-01-06

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_file_def.hpp"


bool	_fileOpen(int *fd, const char *filename, fileOpenMode mode, int pMode)
{
    if (!fd) __exNull();
    if (!filename || !*filename) __exNull();

    int _fd=-1;
    switch (mode) {
	case fileOpenMode_read:
	    _fd = open(filename, O_RDONLY);
	    break;
	case fileOpenMode_write:
	    _fd = open(filename, O_WRONLY | O_TRUNC | O_CREAT, pMode);
	    break;
	case fileOpenMode_update:
	    _fd = open(filename, O_RDWR, pMode);
	    break;
	case fileOpenMode_update_or_create:
	    _fd = open(filename, O_RDWR | O_CREAT, pMode);
	    break;
	case fileOpenMode_create:
	    _fd = open(filename, O_RDWR | O_EXCL | O_CREAT, pMode);
	    break;
	case fileOpenMode_append:
	    _fd = open(filename, O_WRONLY | O_APPEND | O_CREAT, pMode);
	    break;

	default: __exAlgorithm();
    }

    if (_fd < 0)
	return false;

    *fd = _fd;
    return true;
}

/* _fileCheck2.cpp							2014-01-06

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_file_def.hpp"


bool	 _fileCheck2(const char *dirname, const char *filename, time_t *t)
{
    if (!dirname || !*dirname) __exNull();
    if (!filename || !*filename) __exNull();
    int ld = strlen(dirname);
    int lf = strlen(filename);
    if ((ld + lf + 2)>FileName_MaxSize) __exAlgorithm();

    char fn[FileName_MaxSize];
    strcpy(fn, dirname);
    if (fn[ld-1] != '/')
	fn[ld++]='/';
    strcpy(fn+ld, filename);

    return _fileCheck(fn, t);
}


bool	 _fileCheck2Null(const char *dirname, const char *filename, time_t *t)
{
    if (!dirname || !*dirname) __exNull();
    if (!filename || !*filename) __exNull();
    int ld = strlen(dirname);
    int lf = strlen(filename);
    if ((ld + lf + 2)>FileName_MaxSize) __exAlgorithm();

    char fn[FileName_MaxSize];
    strcpy(fn, dirname);
    if (fn[ld-1] != '/')
	fn[ld++]='/';
    strcpy(fn+ld, filename);

    return _fileCheckNull(fn, t);
}

/* _dirCheck.cpp							2014-01-06

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_file_def.hpp"


bool	 _dirCheck_or_Create(const char *f, int pMode)
{
    if (!f) __exNull();

    struct stat st;
    if (!stat(f, &st)) {
	if (S_ISDIR(st.st_mode)) 
	    return true;
	return false;
    }
    if (!mkdir(f, pMode))
	return true;

    return false;
}

/* Cam-Tools.cpp									2019-07-01

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "../types.hpp"
#include "../tools.hpp"
#include <sys/stat.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>


#ifdef _WIN32
    struct timeval {
	long    tv_sec;
	long    tv_usec;
    };
    int S_ISDIR(int);
    int S_ISREG(int);
#else
    #include <sys/file.h>  
#endif


void	*FileToMem(const char *fn, int &len, int additive)
{
    if (additive < 0) additive = 0;

    struct stat st;
    if (!stat(fn, &st) && S_ISREG(st.st_mode) && st.st_size > 0) {
	len = st.st_size;
	char *S = (char *)calloc(len + additive + 1, 1);
	if (S) {
	    FILE *f = fopen(fn, "r");
	    if (f) {
		int rLen = (int)fread(S, 1, len, f);
		S[len] = 0;
		fclose(f);
		if (len == rLen) {
		    return (void *)S;
		}
	    }
	    free(S);
	}
    }
    return NULL;
}

bool	MemToFile(const char *fn, const void *s, int len)
{
    if (len < 0) len = 0;
    char fx[FileName_MaxSize];
    sprintf(fx, "%s~", fn);
    FILE *f = fopen(fx, "w");
    if (f) {
	int rLen;
	if (s && len > 0) {
	    rLen = (int)fwrite(s, 1, len, f);
	} else {
	    rLen = 0;
	}
	fclose(f);

	if (len == rLen) {
	    rename(fx, fn);
	    return true;
	}
    }
    return 0;
}

char	*FileToStr(const char *fn, int &len, int additive)
{
    return (char *)FileToMem(fn, len, additive);
}

bool	StrToFile(const char *fn, const char *s)
{
    return MemToFile(fn, (const void *)s, (s && *s)?strlen(s):0);
}


#include "../mem.hpp"


void* FileToMem(mem* M, const char* fn, int& len, int additive)
{
    if (additive < 0) additive = 0;

    struct stat st;
    if (!stat(fn, &st) && S_ISREG(st.st_mode) && st.st_size > 0) {
	len = st.st_size;
	char* S = (char*)M->Calloc(len + additive + 1);
	if (S) {
	    FILE* f = fopen(fn, "r");
	    if (f) {
		int rLen = (int)fread(S, 1, len, f);
		S[len] = 0;
		fclose(f);
		if (len == rLen) {
		    return (void*)S;
		}
	    }
	    free(S);
	}
    }
    return NULL;
}

char* FileToStr(mem* M, const char* fn, int& len, int additive)
{
    return (char*)FileToMem(M, fn, len, additive);
}

/* _dirCheck.cpp							2014-01-06

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_file_def.hpp"


bool	 _dirCheck(const char *f, time_t *t)
{
    if (!f) __exNull();

    struct stat st;
    if (!stat(f, &st) && S_ISDIR(st.st_mode)) {
	if (t) *t = st.st_mtime;
	return true;
    }

    return false;
}

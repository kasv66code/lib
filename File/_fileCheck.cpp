/* _fileCheck.cpp							2014-01-06

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_file_def.hpp"


bool	 _fileCheck(const char *f, time_t *t)
{
    if (!f || !*f) __exNull();

    struct stat st;
    if (!stat(f, &st) && S_ISREG(st.st_mode) && st.st_size > 0) {
	if (t) *t = st.st_mtime;
	return true;
    }

    return false;
}


bool	 _fileCheckNull(const char *f, time_t *t)
{
    if (!f || !*f) __exNull();

    struct stat st;
    if (!stat(f, &st) && S_ISREG(st.st_mode)) {
	if (t) *t = st.st_mtime;
	return true;
    }

    return false;
}

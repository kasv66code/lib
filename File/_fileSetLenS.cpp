/* _fileSetLenS.cpp							2014-01-06

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_file_def.hpp"


bool	 _fileSetLen(const char *filename, i64 length)
{
    if (!filename || !*filename) __exNull();
    if (length<0) __exValue();
    return (truncate(filename, length)==0)?true:false;
}

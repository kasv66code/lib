/* _dirClear.cpp							2014-01-06

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_file_def.hpp"


bool	 _dirClear(const char *dest, bool enaRmDir)
{
    if (!dest) __exNull();

    DIR *dir=opendir(dest);
    if (!dir)
	return false;

    char buf[FileName_MaxSize], *s=buf;
    strcpy(s, dest);
    s+=strlen(s);
    if (*(s-1)!='/')
	*s++ = '/';

    struct dirent *dirent;
    struct stat st;
    bool ok=true;

    while ((dirent=readdir(dir))) {
	if (!dirent->d_name) __exNull();
	if (strcmp(dirent->d_name, ".") && strcmp(dirent->d_name, "..")) {
	    if ((s-buf+strlen(dirent->d_name)+2)>FileName_MaxSize) __exAlgorithm();
	    strcpy(s, dirent->d_name);
	    if (stat(buf, &st) == -1) {
		ok = false;
		break;
	    }
	    if (S_ISDIR(st.st_mode) && enaRmDir) {
		_dirDelete(buf);
		continue;
	    }
	    if (unlink(buf)) {
		ok = false;
		break;
	    }
	}
    }
    closedir(dir);

    return ok;
}

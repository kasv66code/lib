/* _fileUnLock.cpp							2014-01-06

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_file_def.hpp"


void	_fileUnLock(int fd)
{
    if (fd<0) __exValue();
    if (flock(fd, LOCK_UN)<0) __exAlgorithm();
}

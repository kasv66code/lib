/* _fileDelete.cpp							2014-01-06

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "../file.hpp"
#include "../exceptions.hpp"
#include <stdio.h>


bool	 _fileRename(const char *fn_old, const char *fn_new)
{
    if (!fn_old || !*fn_old) __exNull();
    if (!fn_new || !*fn_new) __exNull();

    if (!rename(fn_old, fn_new)) return true;
    return false;
}

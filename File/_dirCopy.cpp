/* _dirCopy.cpp								2014-01-07

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_file_def.hpp"


static bool	 __dirCopy(const char *dest, const char *d_to, fileCopyMode mode)
{
    // проверить наличие папок
    if (!_dirCheck(dest) || !_dirCheck_or_Create(d_to))
	return false;

    DIR *dir=opendir(dest);
    if (!dir)
	return false;

    char buf[FileName_MaxSize], buf_to[FileName_MaxSize], *s, *s_to;

    s = buf;
    strcpy(s, dest);
    s+=strlen(s);
    if (*(s-1)!='/')
	*s++ = '/';

    s_to = buf_to;
    strcpy(s_to, d_to);
    s_to+=strlen(s_to);
    if (*(s_to-1)!='/')
	*s_to++ = '/';

    struct dirent *dirent;
    struct stat st;
    int len;

    while ((dirent=readdir(dir))) {
	if (!dirent->d_name) __exNull();
	if (strcmp(dirent->d_name, ".") && strcmp(dirent->d_name, "..")) {
	    len = strlen(dirent->d_name);
	    if ((s-buf+len+2)>FileName_MaxSize) __exAlgorithm();
	    if ((s_to-buf_to+len+2)>FileName_MaxSize) __exAlgorithm();
	    strcpy(s, dirent->d_name);
	    strcpy(s_to, dirent->d_name);
	    if (stat(buf, &st) == -1) {
		return false;
	    }
	    if (S_ISDIR(st.st_mode)) {
		if (!__dirCopy(buf, buf_to, mode))
		    return false;
		continue;
	    }
	    if (S_ISREG(st.st_mode)) {
		if (!_fileCopy(buf, buf_to, mode))
		    return false;
		continue;
	    }
	}
    }
    closedir(dir);
    return true;
}

bool	 _dirCopy(const char *dest, const char *d_to, fileCopyMode mode)
{
    if (!dest) __exNull();
    if (!d_to) __exNull();

    // проверить чтобы не копировать папку внутрь самой себя
    if (!strncmp(dest, d_to, strlen(dest)))
	return false;

    return __dirCopy(dest, d_to, mode);
}

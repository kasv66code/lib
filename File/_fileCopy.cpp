/* _fileWrite.cpp							2014-01-06

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_file_def.hpp"


#define	buffCopySize	32*kByte


bool	 _fileCopy(const char *fn_from, const char *fn_to, fileCopyMode mode)
{
    if (!fn_from || !*fn_from) __exNull();
    if (!fn_to || !*fn_to) __exNull();

    // проверить чтобы не копировать файл на себя
    if (!strcmp(fn_from, fn_to))
	return false;

    {
	struct stat _st_from, _st_to;
	if (!stat(fn_from, &_st_from) && !S_ISREG(_st_from.st_mode))
	    return false;

	if (!stat(fn_to, &_st_to)) {
	    if (!S_ISREG(_st_to.st_mode))
		return false;

	    if (_st_to.st_size>0) {
		switch (mode) {
		    case fileCopyMode_step:	// если файл есть, пропустить
			return true;
		    case fileCopyMode_checkNew:	// проверить, если существующий файл старее, то заменить
			if (_st_from.st_mtime < _st_to.st_mtime)
			    return true;
			if (_st_from.st_mtime == _st_to.st_mtime && _st_from.st_size == _st_to.st_size)
			    return true;
			break;
		    case fileCopyMode_all:	// заменить в любом случае
			break;
		}
	    }
	}
    }

    bool status=false;
    int fd_from;
    if (_fileOpen(&fd_from, fn_from, fileOpenMode_read) && _fileLock(fd_from, false, false)) {
	int fd_to;
	if (_fileOpen(&fd_to, fn_to, fileOpenMode_update_or_create) && _fileLock(fd_to, true, false)) {
	    _fileSetLen(fd_to, 0);

	    i64 flen=0, l;
	    _fileGetStat(fd_from, &flen, NULL);
	    for(; flen>0; flen-=l) {
		char buff[buffCopySize];
		l = _fileRead(fd_from, buff, ((int)flen < sizeof(buff)) ? (int)flen : sizeof(buff));
		if (l<=0)
		    break;
		_fileWrite(fd_to, buff, (int)l);
	    }
	    if (!flen)
		status = true;

	    _fileClose(fd_to);
	}
	_fileClose(fd_from);
    }
    return status;
}

/* _fileSetPos.cpp							2014-01-06

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_file_def.hpp"


bool	 _fileSetPos(int fd, i64 pos)
{
    if (fd<0) __exValue();
    if (pos<0) __exValue();
    i64 _pos = lseek(fd, (long)pos, SEEK_SET);
    if (_pos<0) __exAlgorithm();
    if (_pos==pos)
	return true;
    return false;
}

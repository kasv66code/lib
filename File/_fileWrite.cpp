/* _fileWrite.cpp							2014-01-06

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_file_def.hpp"


int	 _fileWrite(int fd, const void *buff, int length)
{
    if (fd<0) __exValue();
    if (!buff) __exNull();
    if (length<=0) __exAlgorithm();

    int len = write( fd, buff, length );
    if (len<0) __exAlgorithm();

    return len;
}

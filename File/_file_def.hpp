/* _file_def.hpp								2014-01-06

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#ifndef	_file_def_hpp
#define	_file_def_hpp


#include "../file.hpp"
#include "../exceptions.hpp"

#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>


#ifdef _WIN32
        #include <io.h>
    
	#define off_t	i64
	#define close	_close
	#define lseek	_lseek
	#define open	_open
	#define read	_read

//	int access(const char *pathname, int mode);
	int mkdir(const char *pathname, int mode);
	int rmdir(const char *pathname);
//	int unlink(const char *pathname);
	int flock(int fd, int operation);
	int truncate(const char *path, off_t length); 
	int ftruncate(int fd, off_t length);
	int getpid(void);

	int snprintf(char *str, size_t size, const char *format, ...);

	#define F_OK	1
	#define S_IRUSR	0200
	#define S_IRWXU	0700
	#define S_IWUSR	0400
	#define S_IXUSR	0100
	#define SEEK_SET	0
	#define SEEK_CUR	1
	#define SEEK_END	2

	#define LOCK_SH		1
	#define LOCK_EX		2
	#define LOCK_UN		3

	#define NAME_MAX	128
	typedef struct {
	    int x;
	} DIR;
	struct dirent {
	    long d_ino;                 // inode number
	    off_t d_off;                // offset to this dirent
	    unsigned short d_reclen;    // length of this d_name
	    char d_name[NAME_MAX+1];   // file name (null-terminated)
	};

	DIR *opendir(const char *name);
	struct dirent *readdir(DIR *);
	int S_ISDIR(int);
	int S_ISREG(int);
	int closedir(DIR *dir);

	#define write _write

	int flock(int fd, int operation);
	#define LOCK_SH		1
	#define LOCK_EX		2
	#define LOCK_UN		3
	#define LOCK_NB		8
//	#define EWOULDBLOCK	11

	struct utimbuf {
	    time_t actime;
	    time_t modtime;
	};
	int utime(const char *filename, struct utimbuf *buf);

	#define unlink	_unlink

#else
	#include <unistd.h>
	#include <dirent.h>
	#include <sys/file.h>
	#include <utime.h>
#endif


#endif

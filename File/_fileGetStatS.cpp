/* _fileGetStatS.cpp							2014-01-06

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_file_def.hpp"


bool	 _fileGetStat(const char *filename, i64 *len, time_t *mtime)
{
    if (!filename || !*filename) __exNull();

    struct stat buf;
    if (stat(filename, &buf) || !S_ISREG(buf.st_mode))
	return false;

    if (len)
	*len = buf.st_size;

    if (mtime)
	*mtime = buf.st_mtime;

    return true;
}


bool	 _fileGetStat(const char *filename, int *len, time_t *mtime)
{
    if (!filename || !*filename) __exNull();

    struct stat buf;
    if (stat(filename, &buf) || !S_ISREG(buf.st_mode))
	return false;

    if (len)
	*len = (int)buf.st_size;

    if (mtime)
	*mtime = buf.st_mtime;

    return true;
}

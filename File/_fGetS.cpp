/* _fGetS.cpp									2015-03-16

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "../file.hpp"
#include "../exceptions.hpp"
#include <stdlib.h>


_fGetS::_fGetS(int _buffSize)
{
    if (!_buffSize) _buffSize = 64 * kByte;
    _fGetS::buffSize = _buffSize;
    _fGetS::buffer = (char *)malloc(_buffSize);
    _fGetS::count = 0;
    _fGetS::start = 0;
    _fGetS::fd = -1;
    _fGetS::fileLength = 0;
    _fGetS::countRead = 0;
}

_fGetS::~_fGetS(void)
{
    _fGetS::fileClose();

    if (_fGetS::buffer)
	free((void *)_fGetS::buffer);
    _fGetS::buffer = NULL;
}

bool	_fGetS::fileOpen(const char *fileFullName)
{
    if (!fileFullName || !*fileFullName) __exNull();
    _fGetS::fileClose();
    if (_fileOpen(&(_fGetS::fd), fileFullName, fileOpenMode_read)) {
	i64 len;
	if (_fileGetStat(_fGetS::fd, &len, NULL)) {
	    _fGetS::fileLength = (int)len;
	    _fGetS::countRead = 0;
	    return true;
	}
    }
    return false;
}

void	_fGetS::fileClose(void)
{
    if (_fGetS::fd>=0) {
	_fileClose(_fGetS::fd);
	_fGetS::fd = -1;
    }
}

char	*_fGetS::GetStr(char c)
{
    if (_fGetS::fd<0)
	return NULL;

    while (1) {
	int cnt;
	if (count - start > 0) {
	    char* line = buffer + start;
	    char* eol;
	    if ((eol = (char *)memchr((void *)line, c, count - start))) {
		start = eol + 1 - buffer;
		*eol-- = 0;
		return line;
	    }
	}

	if (count == _fGetS::buffSize)  {
	    if (start > 0) {
		count -= start;
		memmove(buffer, buffer + start, count);
		start = 0;
	    } else {
		__exAlgorithm();	// buffer overflow
		return NULL;
	    }
	}

	if (_fGetS::countRead == _fGetS::fileLength)
	    return NULL;

	cnt = _fileRead(_fGetS::fd, buffer + count, _fGetS::buffSize - count);
	if (cnt <= 0) {
	    count = start = 0;
	    __exAlgorithm();
	    return NULL;
	}
	count += cnt;
	_fGetS::countRead += cnt;
    }
}

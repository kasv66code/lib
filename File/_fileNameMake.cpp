/* _fileNameMake.cpp							2014-01-06

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_file_def.hpp"
#include <stdarg.h>


bool	 _fileNameMake(fileNameMakeMode mode, char *filefullname, ...)
{
    if (!filefullname) __exNull();

    char *so = filefullname;
    const char *s;
    va_list argp;
    va_start(argp, filefullname);
    int i;
    for (i=0; (s=va_arg(argp, const char *)) != NULL; i++) {
	if (!*s) __exAlgorithm();
	if (i>Directory_MaxDepth) __exAlgorithm();
	if (so-filefullname+strlen(s)+2>FileName_MaxSize) __exAlgorithm();
	if (so!=filefullname) {
	    if (*(so-1)!='/') {
		*so++='/';
		*so=0;
	    }
	    if (mode == fileNameMakeMode_dirCheck) {
		if (!_dirCheck(filefullname)) {
		    i=-1;
		    break;
		}
	    }
	    if (mode == fileNameMakeMode_dirCheck_or_Create) {
		if (!_dirCheck_or_Create(filefullname)) {
		    i=-1;
		    break;
		}
	    }
	}
	strcpy(so, s);
	so += strlen(so);
    }
    va_end(argp);

    if (i<0)
	return false;

    if (i<2) __exAlgorithm();

    return true;
}

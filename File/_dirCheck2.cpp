/* _dirCheck2.cpp							2014-01-06

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_file_def.hpp"


bool	 _dirCheck2(const char *dirname1, const char *dirname2, time_t *t)
{
    if (!dirname1) __exNull();
    if (!dirname2) __exNull();

    int ld = strlen(dirname1);
    int lf = strlen(dirname2);
    if ((ld + lf + 3)>FileName_MaxSize) __exAlgorithm();

    char fn[FileName_MaxSize];
    strcpy(fn, dirname1);
    if (fn[ld-1] != '/')
	fn[ld++]='/';
    strcpy(fn+ld, dirname2);

    return _dirCheck(fn, t);
}

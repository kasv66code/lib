/* _fileSetLen.cpp							2014-01-06

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_file_def.hpp"


bool	 _fileSetLen(int fd, i64 length)
{
    if (fd<0) __exValue();
    if (length<0) __exValue();
    return (ftruncate(fd, length)==0)?true:false;
}

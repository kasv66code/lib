/* _dirNameMake.cpp							2014-01-06

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_file_def.hpp"
#include <stdarg.h>


bool	 _dirNameMake(dirNameMakeMode mode, char *dirfullname, ...)
{
    if (!dirfullname) __exNull();

    char *so = dirfullname;
    const char *s;
    va_list argp;
    va_start(argp, dirfullname);
    int i;
    for (i=0; (s=va_arg(argp, const char *)) != NULL; i++) {
	if (!*s) __exAlgorithm();
	if (i>Directory_MaxDepth) __exAlgorithm();
	if (so-dirfullname+strlen(s)+2>FileName_MaxSize) __exAlgorithm();

	strcpy(so, s);
	so += strlen(so);
	if (*(so-1)!='/') {
	    *so++='/';
	    *so=0;
	}
	if (mode == dirNameMakeMode_dirCheck) {
	    if (!_dirCheck(dirfullname)) {
		i=-1;
		break;
	    }
	} else if (mode == dirNameMakeMode_dirCheck_or_Create) {
	    if (!_dirCheck_or_Create(dirfullname)) {
		i=-1;
		break;
	    }
	}
    }
    va_end(argp);

    if (i<0)
	return false;

    if (i<2) __exAlgorithm();

    return true;
}

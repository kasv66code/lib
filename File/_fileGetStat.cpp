/* _fileGetStat.cpp							2014-01-06

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_file_def.hpp"


bool	 _fileGetStat(int fd, i64 *len, time_t *mtime)
{
    if (fd<0) __exValue();

    struct stat buf;
    if (fstat(fd, &buf) || !S_ISREG(buf.st_mode))
	return false;

    if (len)
	*len = buf.st_size;

    if (mtime)
	*mtime = buf.st_mtime;

    return true;
}


bool	 _fileGetStat(int fd, int *len, time_t *mtime)
{
    if (fd<0) __exValue();

    struct stat buf;
    if (fstat(fd, &buf) || !S_ISREG(buf.st_mode))
	return false;

    if (len)
	*len = buf.st_size;

    if (mtime)
	*mtime = (int)buf.st_mtime;

    return true;
}

/* _fileSetMtime.cpp							2014-01-04

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_file_def.hpp"



bool	 _fileSetMtime(const char *filename, time_t t)
{
    struct utimbuf buf;
    memset(&buf, 0, sizeof(buf));
    if (t) {
	buf.actime = t;
	buf.modtime = t;
    }
    return (utime(filename, (t)?&buf:NULL)==0)?true:false;
}

/* _fileDelete.cpp							2014-01-06

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_file_def.hpp"


bool	 _fileDelete(const char *filename)
{
    if (!filename || !*filename) __exNull();
    return (unlink(filename)) ? false: true;
}

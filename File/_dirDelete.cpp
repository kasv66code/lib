/* _dirDelete.cpp							2014-01-07

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_file_def.hpp"


bool	 _dirDelete(const char *dirname)
{
    if (_dirClear(dirname, true))
	return (!rmdir(dirname))?true:false;

    return false;
}

/* _fileClose.cpp							2014-01-06

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_file_def.hpp"


int	_fileClose(int fd)
{
    if (fd<0) __exValue();
    close(fd);
    return -1;
}

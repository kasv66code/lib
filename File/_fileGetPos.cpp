/* _fileGetPos.cpp							2014-01-06

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_file_def.hpp"


i64	 _fileGetPos(int fd)
{
    if (fd<0) __exValue();
    i64 pos = lseek(fd, 0, SEEK_CUR);
    if (pos<0) __exAlgorithm();
    return pos;
}

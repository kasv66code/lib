/* _fileLock.cpp							2014-01-06

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_file_def.hpp"


bool	_fileLock(int fd, bool exclusive, bool no_block)
{
    if (fd<0) __exValue();

    int _lock_mode = (exclusive) ? LOCK_EX : LOCK_SH;
    if (no_block)
	_lock_mode |= LOCK_NB;

    int r = flock(fd, _lock_mode);
    if (r<0) {
	if (errno == EWOULDBLOCK) {
	    return false;
	}
        __exAlgorithm();
    }
    return true;
}

/* _dirLoop.cpp								2014-01-06

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_file_def.hpp"


bool	 _dirLoop(const char *dest, bool (*fc)(const char *dest, const char *name_in_dest, void *var), void *var)
{
    if (!fc) __exNull();
    if (!dest || !*dest) __exNull();

    DIR *dir;
    int n=0;

    if (!_dirCheck(dest))
	return true;

    if (!(dir=opendir(dest)))
	return true;

    bool r = true;
    struct dirent *dirent;
    while ((dirent=readdir(dir))) {
	if (strcmp(dirent->d_name, ".") && strcmp(dirent->d_name, "..")) {
	    if (!fc(dest, dirent->d_name, var)) {
		r = false;
		break;
	    }
	}
    }
    closedir(dir);

    return r;
}

/*  http.hpp										2018-2020

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#ifndef	_HTTP_hpp
#define	_HTTP_hpp


#include "exceptions.hpp"
#include "mem.hpp"
#include "list.hpp"



class HTTP {
    public:
	void *operator new(size_t size, mem *m=NULL);
	void operator delete(void* p, mem* m);
	void operator delete(void *p);

	HTTP(void);
	~HTTP(void);

	// query
	bool	getURL(const char *url, ...);
	void	Clear(void);

	// answer
	int	code;
	char    *status;
	char    *header;
	char    *content;
	int	content_length;

	size_t	AppendHeader(char* ptr, size_t size, size_t nmemb);
	size_t	AppendContent(char* ptr, size_t size, size_t nmemb);

    private:
	mem	*M;

	list	*Header, *Content;

	void	*cUrl;

	void	cUrlClear(void);
	bool	answerLoad(void);
	void	answerClear(void);

	char*	ListToStr(list* L, int* Size = NULL);
};


/*
    "UserAgent", const char *AgentName
    "Referer", const char *URL
    "Cookie", const char *CookiesValues
    "Timeout", int TimeOut
    "Post", const char *PostString
    "IfModified", time_t Timestamp
    "Auth", const char *Auth	    // "user:password"
    "Proxy", const char *Proxy	    // "http://proxy.server:port/"
    "Header", const char *Header
    "Header2", const char *HeaderName, const char *HeaderValue
*/

// -------------------------------------------------------------------------------------
//  #include "http.hpp"
//  
//  HTTP	*reply = new(M) HTTP;
//  reply->getURL(	url,               // request this URL
//  		"UserAgent", "Agent name",	    // specify user agent instead of default mozilla
//  		"Referer", "http://somesite/xxx",   // send referrer header
//  		"Cookie", "Cookies values",	    // send cookie
//  		"Post", "login=guest&pwd=ok",       // post query
//  		"Timeout", 100,			    // set timeout in seconds (def: 30)
//  		"Auth", "user:password",	    // http-authorization
//  		"IfModified", 939820917,	    // timestamp
//  		"Proxy", "http://proxy.server:port/", // use proxy connection
//  
//  		"Header", "Cache-Control: no-cache",
//  		"Header", "Content-Type: application/json; charset=utf-8",
//  		"Header", "Accept: application/json, */*; q = 0.01",
//  		"Header2", "Authorization: Bearer ", Token,
//  
//  		NULL				    // NULL must be last parameter
//      );
//  delete reply; 
//  // -------------------------------------------------------------------------------------



#endif

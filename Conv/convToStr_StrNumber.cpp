/* convToStr_StrNumber.c							        2020-04-30

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/



#include "../exceptions.hpp"
#include "../conv_numb.hpp"
#include <string.h>



// ------------------------------------------------------------------------------------------------
int	convToStr_StrNumber(char *buff, const char *n)
{
    if (!buff) __exNull();
    if (!n) __exNull();

    if (!*n) {
	*buff=0;
	return 0;
    }

    int i, j, k, z;
    char *si = buff;

    i = strlen(n);

    for(k=0; k<i && (n[k]>='0' && n[k]<='9'); k++)
	;

    if (k<=3) {
	strcpy(buff, n);
	return strlen(buff);
    }

    z = k+k/3;
    if (n[k])
	strcpy(si+z, n+k);
    else
	si[z]=0; 

    for(j=3, k--; k>=0; j--) {
	if (!j) {
	    j = 3;
	    z--;
	    si[z] = ' ';
	}
	z--;
	si[z] = n[k--];
    }

    if (z>0)
	strcpy(buff, si+z);
    return strlen(buff);
}

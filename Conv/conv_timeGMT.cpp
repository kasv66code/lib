/* conv_timeGMT.c							                2020-04-30

    Author: Kachkovsky Sergey V.
    kasv@list.ru

	Sun, 06 Nov 1994 08:49:37 GMT

*/


#include "../conv_time.hpp"
#include <stdio.h>
#include <string.h>


// ------------------------------------------------------------------------------------------------
int	convToStr_timeGMT(char *buff, time_t t)
{
    if (!t) {
	*buff = 0;
	return 0;
    }

    struct tm *tmc = gmtime(&t);

    int day	= tmc->tm_mday;
    int month   = tmc->tm_mon;
    int year    = tmc->tm_year+1900;
    int hours   = tmc->tm_hour;
    int minutes = tmc->tm_min;
    int seconds = tmc->tm_sec;
    int wday    = tmc->tm_wday;

    return sprintf(buff, "%s, %02d %s %d %02d:%02d:%02d GMT", masName_weekDay_EnShort[wday], day, masName_month_EnShort[month], year, hours, minutes, seconds );
}


// ------------------------------------------------------------------------------------------------
time_t	convStrTo_timeGMT(const char* s)
{
    char weekday[4], month[4], gmt[4];
    unsigned int DD = 0, YY = 0, MM = 0, hh = 0, mm = 0, ss = 0;

    if (!s || !*s)
        return 0;

    *weekday = 0;
    *month = 0;
    *gmt = 0;

    if (sscanf(s, "%3s, %u %3s %u %u:%u:%u %3s", weekday, &DD, month, &YY, &hh, &mm, &ss, gmt) != 8)
        return 0;

    if (DD <= 0 || DD > 31 || YY <= 1900 || YY > 2100 || hh < 0 || hh>23 || mm < 0 || mm>59 || ss < 0 || ss>59 || strcmp(gmt, "GMT") != 0)
        return 0;

    if (strcmp(month, "Jan") == 0) MM = 1;
    else if (strcmp(month, "Feb") == 0) MM = 2;
    else if (strcmp(month, "Mar") == 0) MM = 3;
    else if (strcmp(month, "Apr") == 0) MM = 4;
    else if (strcmp(month, "May") == 0) MM = 5;
    else if (strcmp(month, "Jun") == 0) MM = 6;
    else if (strcmp(month, "Jul") == 0) MM = 7;
    else if (strcmp(month, "Aug") == 0) MM = 8;
    else if (strcmp(month, "Sep") == 0) MM = 9;
    else if (strcmp(month, "Oct") == 0) MM = 10;
    else if (strcmp(month, "Nov") == 0) MM = 11;
    else if (strcmp(month, "Dec") == 0) MM = 12;
    else return 0;

    return convTime_makeGMT(YY, MM, DD, hh, mm, ss);
}

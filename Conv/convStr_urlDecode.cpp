/*  convStr_urlDecode.cpp								2020-04-30

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "../conv_str.hpp"
#include "../exceptions.hpp"



const char* convStr_nybble(const char* p, int* n)
{
    char c = *p++;

    (*n) *= 16;
    if (c >= '0' && c <= '9')
	(*n) += c - '0';
    else if (c >= 'A' && c <= 'F')
	(*n) += c + 10 - 'A';
    else if (c >= 'a' && c <= 'f')
	(*n) += c + 10 - 'a';

    return (p);
}


static int __conv_unicode2_utf8(unsigned ucs, char *buf)
{
    if (ucs<=0x7f) {
	*buf=ucs;
	return 1;
    } else if (ucs<=0x7ff) {
	*buf++ = 0xc0 | ((ucs>>6) & 0x1f);
	*buf = 0x80 | (ucs & 0x3f);
	return 2;
    } else if (ucs<=0xffff) {
	*buf++=0xe0 | ((ucs>>12) & 0xf);
	*buf++=0x80 | ((ucs>>6) & 0x3f);
	*buf=0x80 | (ucs & 0x3f);
	return 3;
    } else if (ucs<=0x1fffff) {
	*buf++=0xf0 | ((ucs>>18) & 0x7);
	*buf++=0x80 | ((ucs>>12) & 0x3f);
	*buf++=0x80 | ((ucs>>6) & 0x3f);
	*buf++=0x80 | (ucs & 0x3f);
	return 4;
    }
    return -1;
}


int	convStr_urlDecode(char *Po, const char *q)
{
    if (!Po) __exNull();

    char *p=Po;
    if (q && *q) {
	int c, l;

	while (1) {	// 0xA0 - Win32 versions of navigator convert TEXTAREA fillins of &nbsp; to 0xA0.
	    c = *q++;
	    if (!c)
		break;
	    if ((char)c == '+')
		c = ' ';
	    else if (c == '%') {
		c=0;
		if (*q=='u') {
		    q++;
		    q=convStr_nybble(q, &c);
		    q=convStr_nybble(q, &c);
		    q=convStr_nybble(q, &c);
		    q=convStr_nybble(q, &c);
		    l = __conv_unicode2_utf8(c, p);
		    if (l>0)
			p+=l;
		    continue;
		}
		q=convStr_nybble(q, &c);
		q=convStr_nybble(q, &c);
	    }

	    if (c == '\r')	// Ignore CRs we get in TEXTAREAS
		continue;
	    *p++ = c;
	}
    }
    *p=0;
    return p-Po;
}

/* strlenUTF.cpp								2015-04-27

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "../conv_str.hpp"
#include "../exceptions.hpp"
#include <stdlib.h>
#include <string.h>
#include <wctype.h>
#include <wchar.h>


// ------------------------------------------------------------------------------------------------
int	strlenUTF(const char *Si)
{
    if (!Si || !*Si) return 0;

    int count=0;
    if (Si) {
	int length, pos_i=0;
	char cx;

	length = strlen(Si) + 1;
	while (pos_i < length) {
	    cx = Si[pos_i];
	    if (!cx)
		break;
	    if (cx > 0) {
		count++;
		pos_i++;
	    } else {
		wchar_t c;
		int n = mbtowc(&c, Si + pos_i, length - pos_i);
		if (n > 0) {
		    pos_i += n;
		} else {
		    pos_i++;
		}
		count++;
	    }
	}
    }
    return count;
}

/* convStrTo_timeNumb.cpp							        2020-04-30

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "../conv_time.hpp"
#include "../exceptions.hpp"
#include <stdlib.h>
#include <stdio.h>



// ------------------------------------------------------------------------------------------------
time_t	convStrTo_timeNumb(const char *s)
{
    if (!s || !*s) return 0;

    return (time_t)strtoll(s, (char **)NULL, 10);
}


// ------------------------------------------------------------------------------------------------
int	convToStr_timeNumb(char* buff, time_t t)
{
    if (!buff) __exNull();
    return sprintf(buff, "%lld", (i64)t);
}

/* convStr_toLow_UTF.cpp						2014-01-08

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "../conv_str.hpp"
#include "../exceptions.hpp"
#include <stdlib.h>
#include <string.h>
#include <wctype.h>
#include <wchar.h>



// ------------------------------------------------------------------------------------------------
int	convStr_toLow_UTF(char *So, const char *Si)
{
    if (!So || !Si) __exNull();

    int pos_o=0;
    if (Si) {
	int length, pos_i=0;
	char cx;

	length = strlen(Si) + 1;
	while (pos_i < length) {
	    cx = Si[pos_i];
	    if (!cx)
		break;

	    if (cx < 0) {
		wchar_t c;
		int n = mbtowc(&c, Si + pos_i, length - pos_i);
		if (n > 0) {
		    int m = wctomb(So + pos_o, towlower(c));
		    if (m > 0) {
			pos_i += n;
			pos_o += m;
		    } else {
			strncpy(So + pos_o, Si + pos_i, n);
			pos_i += n;
			pos_o += n;
		    }
		    continue;
		}
	    } else if (cx>='A' && cx<='Z') {
		cx += 32;
	    }

	    So[pos_o++] = cx;
	    pos_i++;
	}
    }
    So[pos_o] = 0;
    return pos_o;
}


// ------------------------------------------------------------------------------------------------
int	convWStr_toLow(wchar_t* buff, const wchar_t* str)
{
    if (!buff || !str) __exNull();

    int n = 0;
    for (;*str;) {
	*buff = towlower(*str);
	n++;
    }
    *buff = 0;
    return n;
}


// ------------------------------------------------------------------------------------------------
int	cmpStr_Low_UTF(const char* str1, const char* str2)
{
    if (str1 && *str1 && str2 && *str2) {
	char* A = (char*)malloc(strlen(str1)+10);
	if (!A) __exAlloc();
	char* B = (char*)malloc(strlen(str2)+10);
	if (!B) __exAlloc();

	convStr_toLow_UTF(A, str1);
	convStr_toLow_UTF(B, str2);

	int R = strcmp(A, B);

	free(B);
	free(A);

	return R;
    }

    return 0;
}

/* convStrTo_Numb.c							                2020-04-30

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/



#include "../conv_numb.hpp"
#include <stdlib.h>



// ------------------------------------------------------------------------------------------------
i32	convStrTo_i32(const char* s)
{
    if (!s || !*s) return 0;
    return (int)strtol(s, (char**)NULL, 10);
}


u32	convStrTo_u32(const char* s)
{
    if (!s || !*s) return 0;

    return (u32)strtoul(s, (char**)NULL, 10);
}


i32	convStrTo_x32(const char* s)
{
    if (!s || !*s) return 0;

    return (int)strtol(s, (char**)NULL, 16);
}


// ------------------------------------------------------------------------------------------------
i64	convStrTo_i64(const char* s)
{
    if (!s || !*s) return 0;

    return (i64)strtoll(s, (char**)NULL, 10);
}


u64	convStrTo_u64(const char* s)
{
    if (!s || !*s) return 0;

    return (u64)strtoull(s, (char**)NULL, 10);
}


i64	convStrTo_x64(const char* s)
{
    if (!s || !*s) return 0;

    return (i64)strtoll(s, (char**)NULL, 16);
}


// ------------------------------------------------------------------------------------------------
double	convStrTo_double(const char *s)
{
    if (!s || !*s) return 0;

    return atof(s);
}

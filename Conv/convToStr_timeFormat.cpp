/* convToStr_timeSec.c							2014-01-08

    Author: Kachkovsky Sergey V.
    kasv@list.ru

    формирует строковое представление времени
    формат строки:
	    %y - полный год, %yy - две последние цифры года
	    %m - месяц [1..12], %mm - две цифры месяца [01..12], %ms - подставить наименование месяца из массива строк month[]
	    %d - день месяца [1..31], %dd - две цифры дня месяца [01..31]
	    %h - час [0..23], %hh - две цифры часа [00..23]
	    %n - минута [0..59], %nn - две цифры минут [00..59]
	    %s - секунда [0..59], %ss - две цифры секунд [00..59]
	    %% - подставляет символ '%'
*/


#include "../conv_time.hpp"
#include "../exceptions.hpp"
#include <stdio.h>
#include <string.h>

#ifdef _WIN32
    struct tm* localtime_r(const time_t* timep, struct tm* result);
#endif


#define	_SPRINTF(d)		sprintf(s, (sh[2]==sh[1])?"%02d":"%d", (int)(d)); s+=strlen(s); sh+=(sh[2]==sh[1])?2:1
#define	_SPRINTF_YEAR_(d)	sprintf(s, (sh[2]==sh[1])?"%02d":"%d", (int)((sh[2]==sh[1])?(d%100):(d+1900))); s+=strlen(s); sh+=(sh[2]==sh[1])?2:1
#define	_SPRINTF_MONTH(d,m)	if (sh[2]=='s') {sh+=2; if (m && m[d]) {strcpy(s,month[d]);strcpy(s,m[d]);s+=strlen(s);}} else {_SPRINTF(d+1);}

int	convToStr_timeFormat(char *So, time_t t, const char *sh, const char **month)
{
    if (!So) __exNull();

    if (!sh || !*sh)
	sh = StringTimeFormat_Default;

    struct tm tmc;
    localtime_r(&t, &tmc);
    char *s=So;

    for(; *sh && (s-So<120); sh++) {
	if (*sh=='%') {
	    switch (sh[1]) {
		case 'y':	// год
		    _SPRINTF_YEAR_(tmc.tm_year);
		    break;
		case 'm':	// месяц
		    _SPRINTF_MONTH(tmc.tm_mon, month);
		    break;
		case 'd':	// день
		    _SPRINTF(tmc.tm_mday);
		    break;
		case 'h':	// час
		    _SPRINTF(tmc.tm_hour);
		    break;
		case 'n':	// минуты
		    _SPRINTF(tmc.tm_min);
		    break;
		case 's':	// секунды
		    _SPRINTF(tmc.tm_sec);
		    break;
		case '%':	// символ '%'
		    sh++;
		    *s = *sh;
		    s++;
	    }
	    continue;
	}
	*s = *sh;
	s++;
    }
    *s=0;

    return strlen(So);
}

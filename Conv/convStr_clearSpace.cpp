/*  convStr_clearSpace.cpp								2020-04-30

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "../conv_str.hpp"
#include <string.h>


char	*convStr_clearSpace(char *s)
{
    if (s && *s) {
	for(; *s>0 && *s<=' '; s++)
	    ;
	if (*s) {
	    char *se;
	    for(se=s+strlen(s)-1; se>=s && *se>0 && *se<=' '; se--)
		*se=0;
	}
    }

    return s;
} 

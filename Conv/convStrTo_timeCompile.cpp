/* convStrTo_timeCompile.cpp						                2020-04-30

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "../conv_time.hpp"
#include <stdio.h>
#include <string.h>


// ------------------------------------------------------------------------------------------------
time_t	convStrTo_timeCompile(const char *Sdate, const char *Stime)
{
    if (!Sdate || !*Sdate) return 0;

    const char *s = Sdate;
    char month[4];
    unsigned int DD=0,YY=0,MM=0;

    *month=0;

    if (sscanf(s, "%3s %u %u", month, &DD, &YY) != 3)
	return 0;

    if (DD<=0 || DD>31 || YY<=2013 || YY>2100)
	return 0;

    if (strcmp(month,"Jan")==0) MM=1;
    else if (strcmp(month,"Feb")==0) MM=2;
    else if (strcmp(month,"Mar")==0) MM=3;
    else if (strcmp(month,"Apr")==0) MM=4;
    else if (strcmp(month,"May")==0) MM=5;
    else if (strcmp(month,"Jun")==0) MM=6;
    else if (strcmp(month,"Jul")==0) MM=7;
    else if (strcmp(month,"Aug")==0) MM=8;
    else if (strcmp(month,"Sep")==0) MM=9;
    else if (strcmp(month,"Oct")==0) MM=10;
    else if (strcmp(month,"Nov")==0) MM=11;
    else if (strcmp(month,"Dec")==0) MM=12;
    else return 0;

    unsigned int HH,NN,SS;
    if (!Stime || !*Stime || sscanf(s, "%u:%u:%u", &HH, &NN, &SS) != 3 || HH>23 || NN>59 || SS>59) {
	HH=12;
	NN=0;
	SS=0;
    }

    return convTime_make(YY, MM, DD, HH, NN, SS);
}

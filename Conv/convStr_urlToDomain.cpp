/* convStr_urlToDomain.cpp						2014-01-08

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "../conv_str.hpp"
#include "../exceptions.hpp"
#include <string.h>


// ------------------------------------------------------------------------------------------------
int	convStr_urlToDomain(char *buff, const char *url, bool www)
{
    if (!buff) __exNull();

    *buff = 0;
    if (!url || !*url)
	return 0;

    const char *s;
    int l;

    if (!strncmp(url, "http://", 7))
	url+=7;
    else if (!strncmp(url, "https://", 8))
	url+=8;
    else if (!strncmp(url, "ftp://", 6))
	url+=6;

    if (!strncmp(url, "www.", 4)) {
	if (!strncmp(url+4, "www.", 4))
	    return 0;
	if (!www)
	    url+=4;
    }

    s = strpbrk(url, "/:? ");
    if (s)
	l = s-url;
    else
	l = strlen(url);

    strncpy(buff, url, l);
    buff[l] = 0;

    if (buff[l-1]=='.')
	buff[l-1]=0;

    s = strchr(buff, '.');
    if (!s) {
	*buff = 0;
	return 0;
    }

    convStr_toLow_UTF(buff, buff);
    return l;
}


// ------------------------------------------------------------------------------------------------
int	convStr_urlToDomainLevel2(char* buff, const char* url)
{
    if (!convStr_urlToDomain(buff, url, false))
        return 0;

    char* se, * sp, * s = buff;

    for (sp = NULL; (se = strchr(s, '.')) != NULL; s = se + 1)
        sp = s;

    if (sp > buff)
        strcpy(buff, sp);

    return strlen(buff);
}

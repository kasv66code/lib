/* punycode.cpp							2016-04-21

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "../tools.hpp"
#include "../conv_str.hpp"
#include "../exceptions.hpp"
#include "../types.hpp"

#include <stdlib.h>
#include <string.h>
#include <wctype.h>
#include <wchar.h>


#ifdef _WIN32
    enum punycode_status {
	punycode_success = 0,
	punycode_bad_input = 1,	// Input is invalid
	punycode_big_output = 2,	// Output would exceed the space provided
	punycode_overflow = 3	// Wider integers needed to process input
    };

    typedef enum {
	PUNYCODE_SUCCESS = punycode_success,
	PUNYCODE_BAD_INPUT = punycode_bad_input,
	PUNYCODE_BIG_OUTPUT = punycode_big_output,
	PUNYCODE_OVERFLOW = punycode_overflow
    } Punycode_status;

    typedef unsigned punycode_uint;

    extern int punycode_encode(size_t input_length, const punycode_uint input[], const unsigned char case_flags[], size_t * output_length, char output[]);
    extern int punycode_decode(size_t input_length, const char input[], size_t * output_length, punycode_uint output[], unsigned char case_flags[]);

    void* alloca(size_t size);
#else
    #include <punycode.h>
    #include <alloca.h>
    #include <unistd.h>
#endif


bool	punycodeDecode(const char *So, char *Sn)
{
    const char *se, *sb;

    *Sn = 0;
    for (sb = So; sb && *sb;) {
	se = strchr(sb, '.');
	int n = (se) ? se - sb : strlen(sb);
	if (!strncmp(sb, "xn--", 4)) {
	    size_t len = Domain_MaxSize;
	    punycode_uint *sr = (punycode_uint *)alloca((n + 1)*sizeof(punycode_uint));
	    memset(sr, 0, (n + 1)*sizeof(punycode_uint));
	    sb += 4;
	    n -= 4;
	    int Status = punycode_decode(n, sb, &len, sr, NULL);
	    if (Status) {
		return 0;
	    }

	    for (size_t i = 0; i < len; i++) {
		int z = wctomb(Sn, (wchar_t)(sr[i]));
		char *Sx = Sn;
		if (z <= 0) {
		    return 0;
		}
		Sn += z;
	    }
	} else {
	    strncpy(Sn, sb, n);
	    Sn += n;
	}
	sb = se;
	if (se) {
	    *Sn++ = '.';
	    sb++;
	}
	*Sn = 0;
    }
    *Sn = 0;
    return 1;
}

bool	punycodeEncode(const char *So, char *Sn)
{
    const char *se, *sb;

    *Sn = 0;
    for (sb = So; sb;) {
	se = strchr(sb, '.');
	int n = (se) ? se - sb : strlen(sb);
	bool r = 0;
	for (int i = 0; i < n; i++) {
	    if (sb[i] < 0) {
		r = 1;
		break;
	    }
	}
	if (r) {
	    punycode_uint *sr = (punycode_uint *)alloca((n+1)*sizeof(punycode_uint) + 1);
	    int m = 0;
	    for (int i = 0; n>0; i++) {
		int z = mbtowc((wchar_t *)(sr + i), sb, n);
		if (z <= 0) return 0;
		m++;
		sb += z;
		n -= z;
	    }
	    size_t len = Domain_MaxSize;
	    strcpy(Sn, "xn--");
	    Sn += 4;
	    int z = punycode_encode(m, sr, NULL, &len, Sn);
	    if (z<0) return 0;
	    Sn += len;
	} else {
	    strncpy(Sn, sb, n);
	    Sn += n;
	}
	sb = se;
	if (se) {
	    *Sn++ = '.';
	    sb++;
	}
	*Sn = 0;
    }
    *Sn = 0;
    return 1;
}

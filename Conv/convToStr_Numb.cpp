/* convToStr_i32.cpp							                2020-04-30

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "../exceptions.hpp"
#include "../conv_numb.hpp"
#include <stdio.h>


// ------------------------------------------------------------------------------------------------
int	convToStr_i32(char *buff, i32 d)
{
    if (!buff) __exNull();
    return sprintf(buff, "%d", d);
}


int	convToStr_u32(char* buff, u32 d)
{
    if (!buff) __exNull();
    return sprintf(buff, "%u", d);
}


int	convToStr_x32(char* buff, i32 d)
{
    if (!buff) __exNull();
    return sprintf(buff, "%x", d);
}


// ------------------------------------------------------------------------------------------------
int	convToStr_i64(char *buff, i64 d)
{
    if (!buff) __exNull();
    return sprintf(buff, "%lld", d);
}


int	convToStr_u64(char* buff, u64 d)
{
    if (!buff) __exNull();
    return sprintf(buff, "%llu", d);
}


int	convToStr_x64(char* buff, i64 d)
{
    if (!buff) __exNull();
    return sprintf(buff, "%llx", d);
}

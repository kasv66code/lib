/* convToStr_double.cpp							                2020-04-30

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "../exceptions.hpp"
#include "../conv_numb.hpp"
#include <stdio.h>
#include <string.h>


// ------------------------------------------------------------------------------------------------
int	convToStr_double(char *buff, double d)
{
    if (!buff) __exNull();
    if (d==0) return sprintf(buff, "0");
    int i = sprintf(buff, "%.15f", d);

    for(i--; i>0; i--) {
	if (buff[i] == '0') {
	    buff[i] = 0;
	    continue;
	}
	if (buff[i] == '.') {
	    buff[i] = 0;
	}
	break;
    }

    return strlen(buff);
}


// ------------------------------------------------------------------------------------------------
int	convToStr_double_AccuracyCount(char* buff, double d, int n)
{
    if (!buff) __exNull();
    if (n < 0 || n>12) __exAlgorithm();
    if (d == 0) return sprintf(buff, "0");

    char s[16];
    sprintf(s, "%%.%df", n);
    return sprintf(buff, s, d);
}


// ------------------------------------------------------------------------------------------------
int	convToStr_double_AccuracyAuto(char* buff, double d)
{
    if (!buff) __exNull();
    if (d == 0) return sprintf(buff, "0");

    double value = (d < 0) ? -d : d;
    int n = 0;

    if (value < .000001) {
        return (d > 0) ? sprintf(buff, "<0.000001") : sprintf(buff, ">-0.000001");
    }
    if (value < .00001)
        n = 8;
    else if (value < .0001)
        n = 7;
    else if (value < .001)
        n = 6;
    else if (value < .01)
        n = 5;
    else if (value < .1)
        n = 4;
    else if (value < 1)
        n = 3;
    else if (value < 10)
        n = 2;
    else if (value < 100)
        n = 1;

    char s[16];
    sprintf(s, "%%.%df", n);
    return sprintf(buff, s, d);
}

/* convTime_make.cpp							2020-04-30

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "../conv_time.hpp"
#include <string.h>

#ifdef _WIN32
    #define timegm	mktime
#endif



// ------------------------------------------------------------------------------------------------
time_t	convTime_make(int year, int month, int day, int hour, int minute, int second)
{
    struct tm tmc;

    memset(&tmc,0,sizeof(tmc));

    tmc.tm_year = year-1900;
    tmc.tm_mon = month-1;
    tmc.tm_mday = day;
    tmc.tm_hour = hour;
    tmc.tm_min = minute;
    tmc.tm_sec = second;

    return mktime(&tmc);
}


// ------------------------------------------------------------------------------------------------
time_t	convTime_makeGMT(int year, int month, int day, int hour, int minute, int second)
{
    struct tm tmc;

    memset(&tmc, 0, sizeof(tmc));

    tmc.tm_year = year - 1900;
    tmc.tm_mon = month - 1;
    tmc.tm_mday = day;
    tmc.tm_hour = hour;
    tmc.tm_min = minute;
    tmc.tm_sec = second;

    return timegm(&tmc);
}

/* convTimeToDay.cpp							                2020-04-30

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "../conv_time.hpp"

#ifdef _WIN32
    struct tm* localtime_r(const time_t* timep, struct tm* result);
#endif

// ------------------------------------------------------------------------------------------------
int	convTimeToDay(time_t t)
{
    struct tm tmc;
    localtime_r(&t, &tmc);
    return tmc.tm_mday;
}


// ------------------------------------------------------------------------------------------------
int	convTimeToDayOfWeek(time_t t)
{
    struct tm tmc;
    localtime_r(&t, &tmc);
    return tmc.tm_wday;
}


// ------------------------------------------------------------------------------------------------
int	convTimeToHour(time_t t)
{
    struct tm tmc;
    localtime_r(&t, &tmc);
    return tmc.tm_hour;
}


// ------------------------------------------------------------------------------------------------
int	convTimeToMin(time_t t)
{
    struct tm tmc;
    localtime_r(&t, &tmc);
    return tmc.tm_min;
}


// ------------------------------------------------------------------------------------------------
int	convTimeToSec(time_t t)
{
    struct tm tmc;
    localtime_r(&t, &tmc);
    return tmc.tm_sec;
}


// ------------------------------------------------------------------------------------------------
int	convTimeToMonth(time_t t)
{
    struct tm tmc;
    localtime_r(&t, &tmc);
    return tmc.tm_mon + 1;
}


// ------------------------------------------------------------------------------------------------
int	convTimeToYear(time_t t)
{
    struct tm tmc;
    localtime_r(&t, &tmc);
    return tmc.tm_year + 1900;
}


// ------------------------------------------------------------------------------------------------
void	convTimeToNumb(time_t t, int& year, int& month, int& day, int& hour, int& minute, int& second)
{
    struct tm tmc;
    localtime_r(&t, &tmc);

    year = tmc.tm_year + 1900;
    month = tmc.tm_mon + 1;
    day = tmc.tm_mday;

    hour = tmc.tm_hour;
    minute = tmc.tm_min;
    second = tmc.tm_sec;
}


// ------------------------------------------------------------------------------------------------
void	convTimeToDate(time_t t, int& year, int& month, int& day)
{
    struct tm tmc;
    localtime_r(&t, &tmc);

    year = tmc.tm_year + 1900;
    month = tmc.tm_mon + 1;
    day = tmc.tm_mday;
}

// ------------------------------------------------------------------------------------------------
void	convTimeToTime(time_t t, int& hour, int& minute, int& second)
{
    struct tm tmc;
    localtime_r(&t, &tmc);

    hour = tmc.tm_hour;
    minute = tmc.tm_min;
    second = tmc.tm_sec;
}

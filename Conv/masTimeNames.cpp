/* masName_month_En.cpp							2009-09-07

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "../conv_time.hpp"


// ------------------------------------------------------------------------------------------------
static const char *_masName_month_En[12]={
	"January",
	"February",
	"March",
	"April",
	"May",
	"June",
	"July",
	"August",
	"September",
	"October",
	"November",
	"December"
};
const char **masName_month_En = _masName_month_En;


// ------------------------------------------------------------------------------------------------
static const char* _masName_month_EnShort[12] = {
	"Jan",
	"Feb",
	"Mar",
	"Apr",
	"May",
	"Jun",
	"Jul",
	"Aug",
	"Sep",
	"Oct",
	"Nov",
	"Dec"
};
const char** masName_month_EnShort = _masName_month_EnShort;


// ------------------------------------------------------------------------------------------------
static const char* _masName_weekDay_EnShort[8] = {
	"Sun",
	"Mon",
	"Tue",
	"Wed",
	"Thu",
	"Fri",
	"Sat"
	"Sun",
};
const char** masName_weekDay_EnShort = _masName_weekDay_EnShort;


// ------------------------------------------------------------------------------------------------
static const char* _masName_month_RuUTFShort[12] = {
	"янв",
	"фев",
	"мар",
	"апр",
	"май",
	"июн",
	"июл",
	"авг",
	"сен",
	"окт",
	"ноя",
	"дек"
};
const char** masName_month_RuUTFShort = _masName_month_RuUTFShort;


// ------------------------------------------------------------------------------------------------
static const char* _masName_month_RuUTFGenitive[12] = {
	"января",
	"февраля",
	"марта",
	"апреля",
	"мая",
	"июня",
	"июля",
	"августа",
	"сентября",
	"октября",
	"ноября",
	"декабря"
};
const char** masName_month_RuUTFGenitive = _masName_month_RuUTFGenitive;


// ------------------------------------------------------------------------------------------------
static const char* _masName_month_RuUTF[12] = {
	"январь",
	"февраль",
	"март",
	"апрель",
	"май",
	"июнь",
	"июль",
	"август",
	"сентябрь",
	"октябрь",
	"ноябрь",
	"декабрь"
};
const char** masName_month_RuUTF = _masName_month_RuUTF;

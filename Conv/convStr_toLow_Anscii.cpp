/* convStr_toLow_Anscii.cpp						2014-01-08

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "../conv_str.hpp"
#include "../exceptions.hpp"
#include <stdlib.h>
#include <string.h>



// ------------------------------------------------------------------------------------------------
int	convStr_toLow_Anscii(char *StrDestination, const char *StrSource)
{
    if (!StrDestination) __exNull();

    int n = 0;
    if (StrSource) {
	char c;
	for(; *StrSource; StrSource++, StrDestination++) {
	    c = *StrSource;

	    if (c>='A' && c<='Z')
		c+=32;

	    *StrDestination = c;
	    n++;
	}
    }
    *StrDestination = 0;
    return n;
}

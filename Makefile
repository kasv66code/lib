CC = g++
AR = ar
CFLAGS = -O2 -s -std=c++0x

OS=$(shell uname -s)
ifeq ($(OS),FreeBSD)
CFLAGS += -I/usr/local/include
endif 

%.o: %.cpp
	$(CC) -c $(CFLAGS) $<

all:	myLib

myLib:	$(patsubst %.cpp,%.o,$(wildcard *.cpp))
	-rm -f *.a
	$(MAKE) -C Cache
	$(MAKE) -C CGI
	$(MAKE) -f MakefileFastCGI -C CGI
	$(MAKE) -C Conv
	$(MAKE) -C Exceptions
	$(MAKE) -C File
	$(MAKE) -C HTTP
	$(MAKE) -C JSON
	$(MAKE) -C List
	$(MAKE) -C Mem
	$(MAKE) -C Shablon
	$(MAKE) -C Tools
	$(MAKE) -C Tree
	ranlib libmy.a
	-rm -f *~

clean:
	-rm -f *.a *~

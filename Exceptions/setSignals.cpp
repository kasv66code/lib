/* setSignals.cpp									2020-04-12

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/

#include "../exceptions.hpp"
#include <stdlib.h> 
#include <string.h>


bool	enaWork=1;
extern bool     __enaLogs;



// ------------------------------------------------------------------------------------------------
#ifdef _WIN32
    typedef void(*sighandler_t)(int);
    sighandler_t signal(int signum, sighandler_t handler) { return handler; }
    #define SIG_IGN		NULL

    #define SIGTERM		15
    #define SIGSEGV		11
    #define SIGHUP		1
    #define SIGILL		4
    #define SIGFPE		8
    #define SIGBUS		7
    #define SIGABRT		6
    #define SIGINT		2
    #define SIGKILL		9

    #define SIGUSR2		2
    #define SIGPIPE		13
    #define SIGALRM		2
    #define SIGTSTP		2
    #define SIGTTIN		2
    #define SIGTTOU		2
    #define SIGURG		2
    #define SIGXCPU		2
    #define SIGXFSZ		2
    #define SIGVTALRM		2
    #define SIGPROF		2
    #define SIGIO		2
    #define SIGCHLD		2
    #define SIGQUIT		2
    #define SIGTRAP		2
    #define SIGIOT		2
    #define SIGEMT		2
    #define SIGSTKFLT		2
    #define SIGCONT		2
    #define SIGPWR		2
    #define SIGSYS		2

    int wait(int* status) { return 0; }
    int waitpid(int pid, int *status, int options) { return 0; }
    #define WNOHANG		1
    char* strsignal(int sig) { return (char *)""; }
#else
    #include <setjmp.h>
    #include <signal.h>
    #include <sys/time.h>
    #include <unistd.h>
    #include <sys/types.h> 
    #include <sys/wait.h>
#endif


static void	(*_funcSignalReapChild)(int);
static void	(*_funcSignalFatal)(int);
static void	(*_funcSignalQuit)(int);
static void	(*_funcSignalIgnore)(int);

static void	_onSignalFatal(int sig)
{
    signal(sig, SIG_IGN);
    enaWork = 0;
    if (__enaLogs) {
        __logDbg("_onSignalFatal> %d, %s", sig, strsignal(sig));
        if (_funcSignalFatal) _funcSignalFatal(sig);
        __dbgLog();
    }
    abort();
}

static void	_onSignalIgnore(int sig)
{
    signal(sig, SIG_IGN);
    if (__enaLogs) {
        __logDbg("_onSignalIgnore> %d, %s", sig, strsignal(sig));
        if (_funcSignalIgnore) _funcSignalIgnore(sig);
    }
    signal(sig, _onSignalIgnore);
}

static void	actionQuit(int sig)
{
    signal(sig, SIG_IGN);
    enaWork = 0;
    if (__enaLogs) {
        __logDbg("_onSignalQuit> %d, %s", sig, strsignal(sig));
        if (_funcSignalQuit) _funcSignalQuit(sig);
    }
    signal(sig, actionQuit);
}

void _onSignalReapChild(int sig)
{
    if (__enaLogs) {
        int wait_status;
        int pid;
        while ((pid = waitpid(-1, &wait_status, WNOHANG)) > 0) {
            if (_funcSignalReapChild) _funcSignalReapChild(pid);
        }
    }
    signal(sig, _onSignalReapChild);
}


// ------------------------------------------------------------------------------------------------
void	setSignals(bool hup, void (*fSignalReapChild)(int), void (*fSignalFatal)(int), void (*fSignalQuit)(int), void (*fSignalIgnore)(int))
{
    _funcSignalReapChild = fSignalReapChild;
    _funcSignalFatal = fSignalFatal;
    _funcSignalQuit = fSignalQuit;
    _funcSignalIgnore = fSignalIgnore;

    // signals ignore
    signal(SIGUSR2, _onSignalIgnore);
    signal(SIGALRM, _onSignalIgnore);
    signal(SIGTSTP, _onSignalIgnore);
    signal(SIGTTIN, _onSignalIgnore);
    signal(SIGTTOU, _onSignalIgnore);
    signal(SIGURG, _onSignalIgnore);
    signal(SIGXCPU, _onSignalIgnore);
    signal(SIGXFSZ, _onSignalIgnore);
    signal(SIGVTALRM, _onSignalIgnore);
    signal(SIGPROF, _onSignalIgnore);
    signal(SIGPIPE, _onSignalIgnore);
    signal(SIGIO, _onSignalIgnore);

    signal(SIGCHLD, _onSignalReapChild);

    // signals quit
    signal(SIGQUIT, actionQuit);
    signal(SIGINT, actionQuit);
    signal(SIGTERM, actionQuit);
    signal(SIGABRT, actionQuit);

    // signals hup
    signal(SIGHUP, (hup)?actionQuit:_onSignalIgnore);

    signal(SIGTRAP, _onSignalFatal);
    signal(SIGIOT, _onSignalFatal);
#ifdef SIGEMT		// this is not defined under Linux
    signal(SIGEMT, _onSignalFatal);
#endif
#ifdef SIGSTKFLT	// is not defined in BSD, OSX
    signal(SIGSTKFLT, _onSignalFatal);
#endif
    signal(SIGCONT, _onSignalFatal);
#ifdef SIGPWR		// is not defined in BSD, OSX
    signal(SIGPWR, _onSignalFatal);
#endif
    signal(SIGSYS, _onSignalFatal);

    // signals fatal
    signal(SIGILL, _onSignalFatal);
    signal(SIGBUS, _onSignalFatal);
    signal(SIGFPE, _onSignalFatal);
    signal(SIGSEGV, _onSignalFatal);
}

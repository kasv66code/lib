/* getCurrTime.cpp									2020-04-12

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/

#include "../exceptions.hpp"


#ifdef _WIN32
    struct timeval {
	long    tv_sec;
	long    tv_usec;
    };

    int gettimeofday(struct timeval *tv, struct timezone *tz);
#else
    #include <sys/time.h>
#endif


#include <mutex> 
using namespace std;

static mutex	lockGetCurrTime;
#define LockGetCurrTime()	lockGetCurrTime.lock()
#define unLockGetCurrTime()	lockGetCurrTime.unlock()   


// ------------------------------------------------------------------------------------------------
void	getCurrTime_s(time_t &t)
{
    LockGetCurrTime();
    time(&t);
    unLockGetCurrTime();
}

void	getCurrTime_ms(time_t &t, int &ms)
{
    struct timeval tv;
    LockGetCurrTime();
    gettimeofday(&tv, NULL);

    t = tv.tv_sec;
    ms = tv.tv_usec / 1000;
    unLockGetCurrTime();
}

void	getCurrTime_us(time_t &t, int &us)
{
    struct timeval tv;
    LockGetCurrTime();
    gettimeofday(&tv, NULL);

    t = tv.tv_sec;
    us = tv.tv_usec;
    unLockGetCurrTime();
}

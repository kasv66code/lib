/* log.cpp								                2020-04-12

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "../exceptions.hpp"
#include "../types.hpp"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h> 

#include <mutex> 
using namespace std;

static std::mutex	lockLogBase;
#define LockLogBase()	lockLogBase.lock()
#define unLockLogBase()	lockLogBase.unlock()   


#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>


#ifdef _WIN32
    #include <io.h> 
    
    int mkdir(const char *pathname, int mode);

    #ifndef S_ISDIR
    #define S_ISDIR(mode)  (((mode) & S_IFMT) == S_IFDIR)
    #endif

    #ifndef S_ISREG
    #define S_ISREG(mode)  (((mode) & S_IFMT) == S_IFREG)
    #endif

    struct tm *localtime_r(const time_t *timep, struct tm *result);
    #define strdup _strdup
    #define getpid _getpid
    #include <process.h>
#else
    #include <sys/file.h> 
    #include <unistd.h> 
#endif


// ------------------------------------------------------------------------------------------------
static char* __logDirName, * __logScriptName;
static const char* __logDir;
static int __runIndex;
bool __enaLogs;


void	__logInit(const char *dirName, const char *scriptName, int runIndex)
{
    if (dirName && *dirName) {
        if (__logDirName) free(__logDirName);
        __logDirName = strdup(dirName);
    }
    if (scriptName && *scriptName) {
        if (__logScriptName) free(__logScriptName);
        __logScriptName = strdup(scriptName);
    }
    __runIndex = runIndex;
    __enaLogs = 1;
}

void	__logClose(void)
{
    __enaLogs = 0;
}


// ------------------------------------------------------------------------------------------------
static const char	*__logPrepareDirOpen(void)
{
    if (__logDirName && *__logDirName) {
	struct stat st;

	if (!stat(__logDirName, &st)) {
	    if (S_ISDIR(st.st_mode))
		return __logDirName;
	} else if (!mkdir(__logDirName, 0755)) {
	    return __logDirName;
	}
    }
    return "./";
}

static const char	*__logPrepareDir(void)
{
    if (__logDir) return __logDir;

    const char *s = __logPrepareDirOpen();
    int l = strlen(s);
    if (s[l - 1] == '/') {
	__logDir = s;
	return __logDir;
    }

    char *dn = (char *)malloc(l+2);
    if (!dn) {
	__logDir = "./";
	return __logDir;
    }
    
    sprintf(dn, "%s/", s);
    __logDir = dn;
    return __logDir;
}


static char	*currFileName;
static FILE	*fo;


static FILE	*__logOpen(bool dbgLog=0)
{
    const char *dir = __logPrepareDir();

    time_t t;
    int ms;
    getCurrTime_ms(t, ms);

    struct tm T;
    localtime_r(&t, &T);

    char fn[FileName_MaxSize];
    char *sx = fn;
    sx += sprintf(sx, "%s%02d%02d%02d", dir, T.tm_year % 100, T.tm_mon + 1, T.tm_mday);
    if (dbgLog) {
	if (__logScriptName && *__logScriptName) sx += sprintf(sx, "-%s", __logScriptName);
	if (__runIndex > 0) sx += sprintf(sx, "-%d", __runIndex);
    }
    sprintf(sx, ".log");

    FILE *f;
    if (dbgLog) {
	if (fo && currFileName && !strcmp(currFileName, fn)) {
	    f = fo;
	} else {
	    if (currFileName) {
		free(currFileName);
		currFileName = NULL;
	    }
	    if (fo) fclose(fo);

	    currFileName = strdup(fn);
	    fo = fopen(fn, "a");
	    f = fo;
	}
    } else {
	f = fopen(fn, "a");
    }
    if (!f) return NULL;

    fprintf(f, "%02d:%02d:%02d.%03d;%d", T.tm_hour, T.tm_min, T.tm_sec, ms, getpid());
    if (!dbgLog) {
	fprintf(f, ";%s", (__logScriptName) ? __logScriptName : "");
	if (__runIndex > 0) fprintf(f, ";%d", __runIndex);
    }
    fprintf(f, "\t");
    return f;
}


// ------------------------------------------------------------------------------------------------
void	__log(const char *fmt, ...)
{
    if (!__enaLogs) return;

    LockLogBase();
    FILE *f = __logOpen();
    if (f) {
	if (fmt) {
	    va_list argp;
	    va_start(argp, fmt);
	    vfprintf(f, fmt, argp);
	    va_end(argp);
	}
	fprintf(f, "\n");
	fclose(f);
    }
    unLockLogBase();
}


// ------------------------------------------------------------------------------------------------
void	__logError(const char* File, int Line, const char* fmt, ...)
{
    if (!__enaLogs) return;

    LockLogBase();
    FILE *f = __logOpen();
    if (f) {
        fprintf(f, "Error! file=%s, line=%d\t", File, Line);
	if (fmt) {
	    va_list argp;
	    va_start(argp, fmt);
	    vfprintf(f, fmt, argp);
	    va_end(argp);
	}
	fprintf(f, "\n");
	fclose(f);
    }
    unLockLogBase();
}

// ------------------------------------------------------------------------------------------------
void	__logEnv(void)
{
    if (!__enaLogs) return;

    if (!environ) return;

    LockLogBase();
    FILE *f = __logOpen();
    if (f) {
	fprintf(f, "environ:");
	for (int i = 0; environ[i] != NULL; i++) {
	    fprintf(f, "\n\t%s", environ[i]);
	}
	fprintf(f, "\n");
	fclose(f);
    }
    unLockLogBase();
}


// ------------------------------------------------------------------------------------------------
void	__logDbg(const char *fmt, ...)
{
    if (!__enaLogs) return;

    if (!fmt) {
	LockLogBase();
	if (fo) {
	    fclose(fo);
	    fo = NULL;
	}
	if (currFileName) {
	    free(currFileName);
	    currFileName = NULL;
	}
	unLockLogBase();
	return;
    }

    LockLogBase();
    FILE *f = __logOpen(1);
    if (f) {
	if (fmt) {
	    va_list argp;
	    va_start(argp, fmt);
	    vfprintf(f, fmt, argp);
	    va_end(argp);
	}
	fprintf(f, "\n");
	fflush(f);
    }
    unLockLogBase();
}

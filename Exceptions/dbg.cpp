/* dbg.cpp										2020-04-12

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/

#include "../exceptions.hpp"
#include <string.h>
#include <mutex> 

using namespace std;

static mutex	lockDbg;
#define LockDbg()	lockDbg.lock()
#define unLockDbg()	lockDbg.unlock()   


class DbgPoint {
    public:
        DbgPoint(void);

	const char	*File;
	int		Line;

	int		idx;
	DbgPoint	*P;
	bool		ena;
};

DbgPoint::DbgPoint(void)
{
    memset(this, 0, sizeof(*this));
}

static DbgPoint	*masDbgPoint;
static DbgPoint	*freeDbgPoint;
static int	sizeDbgPoint;
static int	lastIdxDbgPoint;


// ------------------------------------------------------------------------------------------------
// задаёт количество одновременных процессов, всегда должна предшествовать другим __dbg
int	__dbgMaxThread(int n)
{
    if (n < 1) n = 1;
    if (masDbgPoint) {
	delete masDbgPoint;

	masDbgPoint = NULL;
	freeDbgPoint = NULL;
	sizeDbgPoint = 0;
	lastIdxDbgPoint = 0;
    }

    masDbgPoint = new DbgPoint[n];
    sizeDbgPoint = n;

    return __dbgNew();
}

// ------------------------------------------------------------------------------------------------
void	__dbg(int idx, const char *file, int line)
{
    if (!masDbgPoint || sizeDbgPoint <= 0) __exAlgorithm();
    if (idx < 0 || idx >= sizeDbgPoint) __exAlgorithm();
    DbgPoint *q = masDbgPoint + idx;
    if (!q->ena) __exAlgorithm();

    q->File = file;
    q->Line = line;
}

void	__dbgClear(int idx)
{
    __dbg(idx, NULL, 0);
}


// ------------------------------------------------------------------------------------------------
int	__dbgNew(void)
{
    if (lastIdxDbgPoint >= sizeDbgPoint && !freeDbgPoint) __exAlgorithm();

    DbgPoint *q;
    LockDbg();
    if (freeDbgPoint) {
	q = freeDbgPoint;
	freeDbgPoint = q->P;
    } else {
	q = masDbgPoint + lastIdxDbgPoint;
	q->idx = lastIdxDbgPoint;
	lastIdxDbgPoint++;
    }
    q->ena = 1;
    unLockDbg();
    return q->idx;
}

void	__dbgDel(int &idx)
{
    if (!masDbgPoint || sizeDbgPoint <= 0) __exAlgorithm();
    if (idx < 0 || idx >= sizeDbgPoint) __exAlgorithm();
    DbgPoint *q = masDbgPoint + idx;
    if (!q->ena) __exAlgorithm();

    LockDbg();
    q->P = freeDbgPoint;
    freeDbgPoint = q;
    q->File = NULL;
    q->Line = 0;
    q->ena = 0;
    unLockDbg();

    idx = -1;
}


// ------------------------------------------------------------------------------------------------

void	__dbgLog(void)
{
    static bool	__dbgDisWrite;

    if (__dbgDisWrite) return;
    __dbgDisWrite = 1;
    if (!masDbgPoint || sizeDbgPoint <= 0) return;
    if (lastIdxDbgPoint > sizeDbgPoint) lastIdxDbgPoint = sizeDbgPoint;

    for (int i = 0; i < lastIdxDbgPoint; i++) {
	DbgPoint *q = masDbgPoint + i;
	if (q->ena && q->File && q->Line>0)
	    __logDbg("__dbgLog[%d]> file=%s, line=%d", i, q->File, q->Line);
    }
    __logDbg(NULL);
}

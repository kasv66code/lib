/* loopCreate.cpp							2014-01-16

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_cache.hpp"


bool	Cache::loopCreate(void (*_rec_create_all)(FILE *f))
{
    if (_rec_create_all) __exNull();
    if (!filename || !*filename) __exNull();
    if (Cache::fw) __exAlgorithm();
    if (Cache::fr) __exAlgorithm();

    if (Cache::OpenCreate()) {
	_rec_create_all(Cache::fw);
	Cache::Close();
	return true;
    }
    return false;
}

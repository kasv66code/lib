/* loopUpdate.cpp							2014-01-15

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_cache.hpp"


bool	Cache::loopUpdate(bool (*_rec_update_one)(char *s, FILE *f), void (*_rec_append_all)(FILE *f))
{
    if (!_rec_update_one) __exNull();
    if (!filename || !*filename) __exNull();
    if (!buff) __exNull();
    if (!buff2) __exNull();
    if (!recordSizeMax) __exNull();
    if (Cache::fw) __exAlgorithm();
    if (Cache::fr) __exAlgorithm();

    if (Cache::OpenUpdate()) {
	if (!Cache::CheckEmpty()) {
	    for(; fgets(Cache::buff, Cache::recordSizeMax-1, Cache::fr); ) {
		Cache::buff[Cache::recordSizeMax-1]=0;
		if (!strchr(Cache::buff, '\n')) __exAlgorithm();
		strcpy(Cache::buff2, Cache::buff);
		if (_rec_update_one(Cache::buff2, Cache::fw))
		    fprintf(Cache::fw, "%s", Cache::buff);
	    }
	}
	if (_rec_append_all)
	    _rec_append_all(Cache::fw);
	Cache::Close();
	return true;
    }
    return false;
}

/* OpenCreate.cpp							2014-01-16

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_cache.hpp"


bool	Cache::OpenCreate(void)
{
    if (!filename || !*filename) __exNull();

    if (Cache::lockOpen(true)) {
	char fn_new[FileName_MaxSize];
	sprintf(fn_new, "%s~", filename);
	Cache::fw = fopen(fn_new, "w");
	if (Cache::fw) {
	    return true;
	}
	Cache::lockClose();
    }
    return false;
}

/* loopRead.cpp								2014-01-15

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_cache.hpp"


bool	Cache::loopRead(bool (*_rec_read_one)(char *s))
{
    if (!_rec_read_one) __exNull();
    if (!filename || !*filename) __exNull();
    if (!buff) __exNull();
    if (!recordSizeMax) __exNull();
    if (Cache::fw) __exAlgorithm();
    if (Cache::fr) __exAlgorithm();

    if (Cache::OpenRead()) {
	for(; fgets(Cache::buff, Cache::recordSizeMax-1, Cache::fr); ) {
	    if (!_rec_read_one(Cache::buff))
		break;
	}
	Cache::Close();
	return true;
    }
    return false;
}

/* Cache.cpp								2014-01-16

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/



#include "_cache.hpp"


void	Cache::CacheInit(const char *_filename, int _recordSizeMax)
{
    memset(this, 0, sizeof(*this));
    Cache::fd_lock = -1;

    Cache::recordSizeMax = (_recordSizeMax>kByte)?_recordSizeMax:CACHE_RECORD_SIZE_MAX_DEFAULT;

    Cache::filename = strdup(_filename);
    Cache::buff = (char *)malloc(Cache::recordSizeMax);
    Cache::buff2 = (char *)malloc(Cache::recordSizeMax);

    if (!filename) __exNull();
    if (!Cache::buff) __exNull();
    if (!Cache::buff2) __exNull();
}

Cache::Cache(const char *_filename, int _recordSizeMax)
{
    if (!_filename || !*_filename) __exNull();
    Cache::CacheInit(_filename, _recordSizeMax);
}

Cache::Cache(const char *_dirname, const char *_filename, int _recordSizeMax)
{
    if (!_dirname || !*_dirname) __exNull();
    if (!_filename || !*_filename) __exNull();

    char fn[FileName_MaxSize];
    _fileNameMake(fileNameMakeMode_dirCheck_or_Create, fn, _dirname, _filename, NULL);
    Cache::CacheInit(fn, _recordSizeMax);
}

Cache::~Cache(void)
{
    if (Cache::fr) fclose(Cache::fr);
    if (Cache::fw) fclose(Cache::fw);

    Cache::UnLock();

    if (Cache::filename) free(Cache::filename);
    if (Cache::buff) free(Cache::buff);
    if (Cache::buff2) free(Cache::buff2);
}

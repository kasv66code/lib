/* Delete.cpp									2014-01-16

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_cache.hpp"


void	Cache::Delete(void)
{
    if (!filename || !*filename) __exNull();
    if (!Cache::fr || Cache::fw) __exAlgorithm();

    if (Cache::lockOpen(true)) {
	char fn_new[FileName_MaxSize];
	sprintf(fn_new, "%s~", filename);
	if (_fileCheck(fn_new))
	    _fileDelete(fn_new);
	if (_fileCheck(filename))
	    _fileDelete(filename);

	Cache::lockClose();
    }
}

/* Lock.cpp									2014-11-05

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_cache.hpp"


void	Cache::Lock(void)
{
    Cache::group_lock = Cache::lockOpen(true);
}

void	Cache::UnLock(void)
{
    Cache::group_lock = false;
    Cache::lockClose();
}

// ------------------------------------------------------------------------------------------------

bool	Cache::lockOpen(bool mode_write)
{
    if (Cache::group_lock) return true;

    if (!filename || !*filename) __exNull();
    char fn_lock[FileName_MaxSize];
    sprintf(fn_lock, "%s.lock", Cache::filename);

    if (_fileOpen(&(Cache::fd_lock), fn_lock, fileOpenMode_update_or_create) && _fileLock(Cache::fd_lock, mode_write, false))
	return true;
    return false;
}

void	Cache::lockClose(void)
{
    if (Cache::group_lock)
	return;

    if (Cache::fd_lock>=0) {
	_fileClose(Cache::fd_lock);
	Cache::fd_lock = -1;
    }
}

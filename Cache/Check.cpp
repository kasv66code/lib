/* Check.cpp								2014-01-16

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_cache.hpp"


bool	Cache::Check(void)
{
    if (!filename || !*filename) __exNull();

    if (_fileCheck(Cache::filename) && !Cache::CheckEmpty()) {
	return true;
    }

    return false;
}

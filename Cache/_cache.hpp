/* _cache.hpp								2014-01-15

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "../exceptions.hpp"
#include "../cache.hpp"
#include "../file.hpp"


#ifdef _WIN32
    #define strdup	_strdup
#endif


#define _cache_empty_		"nul"

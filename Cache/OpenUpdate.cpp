/* OpenUpdate.cpp								2014-01-16

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_cache.hpp"


bool	Cache::OpenUpdate(void)
{
    if (!filename || !*filename) __exNull();

    if (Cache::lockOpen(false)) {
	if (_fileCheck(Cache::filename)) {
	    Cache::fr = fopen(filename, "r");
	    if (Cache::fr) {
		char fn_new[FileName_MaxSize];
		sprintf(fn_new, "%s~", filename);
		Cache::fw = fopen(fn_new, "w");
		if (Cache::fw) {
		    return true;
		}
		fclose(Cache::fr);
	    }
	}
	Cache::lockClose();
    }
    return false;
}

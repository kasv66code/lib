/* Close.cpp								2014-01-16

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_cache.hpp"


void	Cache::Close(void)
{
    if (!filename || !*filename) __exNull();
    if (!Cache::fr && !Cache::fw) __exAlgorithm();

    if (Cache::fr) {
	fclose(Cache::fr);
	Cache::fr = NULL;
    }
    if (Cache::fw) {
	fclose(Cache::fw);
	Cache::fw = NULL;

	char fn_new[FileName_MaxSize];
	sprintf(fn_new, "%s~", filename);
	i64 len=0;

	if (_fileGetStat(fn_new, &len, NULL)) {
	    if (len>0) {
		_fileRename(fn_new, filename);
	    } else {
		_fileDelete(fn_new);
		FILE *f = fopen(filename, "w");
		if (f) {
		    fprintf(f, "%s", _cache_empty_);
		    fclose(f);
		}
	    }
	}
    }

    if (Cache::fd_lock<0) __exAlgorithm();
    Cache::lockClose();
}

/* OpenRead.cpp								2014-01-16

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_cache.hpp"


bool	Cache::OpenRead(void)
{
    if (!filename || !*filename) __exNull();

    if (Cache::lockOpen(false)) {
	if (_fileCheck(Cache::filename) && !Cache::CheckEmpty()) {
	    Cache::fr = fopen(Cache::filename, "r");
	    if (Cache::fr) {
		return true;
	    }
	}
	Cache::lockClose();
    }
    return false;
}

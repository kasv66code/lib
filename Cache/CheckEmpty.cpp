/* CheckEmpty.cpp							2014-01-16

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_cache.hpp"


bool	Cache::CheckEmpty(void)
{
    i64 len=0;

    if (_fileGetStat(Cache::filename, &len, NULL) && len>0 && len== sizeof(_cache_empty_)-1) {
	return true;
    }

    return false;
}

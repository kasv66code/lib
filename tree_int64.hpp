/* tree_int64.hpp									2013-2020

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#ifndef	tree_int64_hpp
#define	tree_int64_hpp


#include "exceptions.hpp"
#include "mem.hpp"
#include "types.hpp"
#include <mutex> 


typedef struct nodeInt64_t {
    void* data;		// структура данных пользователя
    int			numb;		// переменная integer для пользователя

    struct nodeInt64_t* next;		// список для неуникальных записей
    i64			key;		// уникальный ключ
} *nodeInt64;


typedef int	(*__sort_compare_nodeInt64)(const nodeInt64*, const nodeInt64*);
typedef void	(*__freeData_nodeInt64_t)(void* Qx, nodeInt64 v);
typedef void	(*__newData_nodeInt64_t)(void* Qx, nodeInt64 v);


// --- класс дерева Int64 ------------------------------------------------------------------
class treeInt64 {
public:
    void* operator new(size_t size, class mem* m = NULL);
    void operator delete(void* p, mem* m);
    void operator delete(void* p);

    treeInt64(void* _Qx = NULL, __newData_nodeInt64_t _newData = NULL, __freeData_nodeInt64_t _freeData = NULL);
    ~treeInt64(void);

    void	    treeFree(void);
    void	    treeMasFree(void);

    void	    lockClose(void);		// закрыть mutex, если не предполагается вызывать десктруктор

    nodeInt64	    First(void);
    nodeInt64	    Last(void);

    nodeInt64	    Find(i64 key);			// поиск по совпадению - возвращает адрес узла
    nodeInt64	    Insert(i64 key);			// добавить узел - возвращает адрес узла
    void	    Delete(i64 key);			// удаляет из дерева ключ вместе с данными (если указана __free_nodeStringData)
    nodeInt64	    AppendDataInListLast(nodeInt64 q);	// добавляет элемент в конец списка (для неуникальных записей)
    nodeInt64	    AppendDataInListFirst(nodeInt64 q);	// добавляет элемент в начало списка (для неуникальных записей)

    // формирует массив из элементов дерева и возвращает элемент массива по индексу idx
    nodeInt64	operator [] (int idx);

    int	            count(void);

    // формирует и сортирует массив из элементов дерева
    void	    sort(__sort_compare_nodeInt64 fsort_compare);

    friend void*    _treeInt64KeyNew(class _Mem* m, void* qu, const void* key);
    friend void     _treeInt64KeyFree(class _Mem* m, void* qu, void* key);
private:
    tree* T;
    mem* M;
    std::mutex* lockT;

    void*       makeNodeKey(const void* key);
    void	makeNodeKey(nodeInt64 q, i64 key);

    void* Qx;		// структура пользователя
    __newData_nodeInt64_t	newData;	// функция для создания данных ключа
    __freeData_nodeInt64_t	freeData;	// функция для удаления данных ключа
};
// ------------------------------------------------------------------ класс дерева Int64 ---



#endif

/* tree_wstr.hpp									2013-2020

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#ifndef	tree_wstr_hpp
#define	tree_wstr_hpp


#include "exceptions.hpp"
#include "mem.hpp"
#include <mutex> 


typedef struct nodeWStr_t {
    void		*data;		// структура данных пользователя
    int			numb;		// переменная integer для пользователя

    struct nodeWStr_t	*next;		// список для неуникальных записей
    int			hashKey;	// hash для key для быстрого пропуску несовпадающих key
    int			keyLen;		// длина строки key
    wchar_t		key[2];		// уникальный ключ - string
} *nodeWStr;


typedef int	(*__sort_compare_nodeWStr)(const nodeWStr *, const nodeWStr *);
typedef void	(*__freeData_nodeWStr_t)(void *Qx, nodeWStr v);
typedef void	(*__newData_nodeWStr_t)(void *Qx, nodeWStr v);


// --- класс дерева Str ------------------------------------------------------------------
class treeWStr {
    public:
	void *operator new(size_t size, class mem *m=NULL);
	void operator delete(void* p, mem* m);
	void operator delete(void *p);

	enum class modeKeyCmp {
	    none=0,
	    lower
	};

	treeWStr(modeKeyCmp KeyCmp, void* _Qx = NULL, __newData_nodeWStr_t _newData = NULL, __freeData_nodeWStr_t _freeData = NULL);
	~treeWStr(void);

	void	lockClose(void);		// закрыть mutex, если не предполагается вызывать десктруктор

	void	treeFree(void);
	void	treeMasFree(void);

	nodeWStr	Find(const wchar_t*key);			// поиск по совпадению - возвращает адрес узла
	nodeWStr	Insert(const wchar_t* key);		// добавить узел - возвращает адрес узла
	void		Delete(const wchar_t* key);		// удаляет из дерева ключ вместе с данными (если указана __free_nodeWStringData)
	nodeWStr	AppendDataInListLast(nodeWStr q);	// добавляет элемент в конец списка (для неуникальных записей)
	nodeWStr	AppendDataInListFirst(nodeWStr q);	// добавляет элемент в начало списка (для неуникальных записей)

	// формирует массив из элементов дерева и возвращает элемент массива по индексу idx
	nodeWStr	operator [] (int idx);

	int		count(void);

	// формирует и сортирует массив из элементов дерева
	void		sort(__sort_compare_nodeWStr fsort_compare);

	friend void	*_treeWStrKeyNew(class _Mem* m, void* qu, const void* key);
	friend void     _treeWStrKeyFree(class _Mem* m, void* qu, void* key);
    private:
	tree		*T;
	mem		*M;
	std::mutex	*lockT;

	nodeWStr	makeNodeKey(const wchar_t* key);
	void		*copyNodeKey(const void*key);
	void		nodeFree(nodeWStr v);

	modeKeyCmp		ModeKeyCmp;
	void			*Qx;		// структура пользователя
	__newData_nodeWStr_t	newData;	// функция для создания данных ключа
	__freeData_nodeWStr_t	freeData;	// функция для удаления данных ключа
};
// ------------------------------------------------------------------ класс дерева Str ---



#endif

/*  cgi.hpp										2012-2020

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#ifndef	_cgi_hpp
#define	_cgi_hpp


#include "shablon.hpp"
#include "types.hpp"
#include <time.h>


enum class cgiContentType {
    cgiContentType_Unknow=0,
    cgiContentType_gif,
    cgiContentType_jpg,
    cgiContentType_png,
    cgiContentType_javascript,
    cgiContentType_plain,
    cgiContentType_html,
    cgiContentType_xml,
    cgiContentType_json
};


enum class cgiCharSet {
    cgiCharSet_Unknow =0,
    cgiCharSet_UTF8,
    cgiCharSet_Windows1251
};


enum class cgiStatus {
    cgiStatus_Unknow=0,
    cgiStatus_200,
    cgiStatus_301,
    cgiStatus_303,
    cgiStatus_304,
    cgiStatus_400,
    cgiStatus_401,
    cgiStatus_403,
    cgiStatus_404,
    cgiStatus_500,
    cgiStatus_501,
    cgiStatus_503
};


enum class cgiMethod {
    cgiMethod_Unknow=0,
    cgiMethod_GET,
    cgiMethod_HEAD,
    cgiMethod_POST,
    cgiMethod_POST_MULTIPART
};


typedef struct {
    char	*data;
    char	*contentType;
    char	*fileName;
    int		size;
} cgiFile;


typedef void   (*_header_lines_append_t)(char *Sout);


// --- класс обслуживания cgi-запросов ------------------------------------------------------------
class CGI {
    public:
	void* operator new(size_t size, mem* m=NULL);
	void operator delete(void* p, mem* m);
	void operator delete(void* p);

	CGI(cgiCharSet charSet=cgiCharSet::cgiCharSet_UTF8);
	~CGI(void);

	// in
	bool	Load(bool enaPost=1, int PostDataMaxLen=MByte, bool enaFiles=1, bool EnaAngleBrackets=1);

	const char	*getValue(const char *name);
	const char	*getValuePost(const char *name);
	const char	*setValue(const char *name, const char *value, bool post=0, bool replace=1);
	nodeStr		getValueNode(const char* name);

	const char	*getCookieIn(const char *name);
	const char	*setCookieIn(const char *name, const char *value, bool replace=1);

	const cgiFile	*getFile(const char *name);

	const char	*QueryString;
	const char	*CookieString;
	const char	*PostString;
	const char	*ContentType;
	int		PostLength;
	const char	*Method;
	cgiMethod	eMethod;
	bool		https;
	bool		www;
	const char	*Authorization;
	const char	*URL;

	const char	*DomainTwoLevel;
	const char	*ServerName;
	int		port;
	const char	*ScriptName;
	const char	*Referer;
	const char	*IP;
	const char	*UserAgent;
	const char	*Language;

	time_t		if_modified_since;

	// out
	_header_lines_append_t	funcHeaderLinesAppend;

	cgiCharSet	oCharSet;
	cgiContentType	oContentType;
	cgiStatus	Status;
	int		cacheLife;
	time_t		LastModified;
	const char	*RedirectUrl;

	bool		PutHeader(unsigned dataLen);
	void		PutData(unsigned dataLen, const char *dataMem);
	void		PutShablon(shablon *sh);
	void		PutRedirect(void);
	void		PutNotModified(void);
	bool		PutFile(const char *fileFullName, bool enaLastModified=1);

	const char	*setCookieOut(const char *name, const char *value, int timeLife=_year_, bool allSubDomain=1);
	const char	*getCookieOut(const char *name);

	void	ReadAllParams(const char *string, bool post=0);

    private:
	mem	*M;
	treeStr	*treeCookieIn, *treeCookieOut;
	time_t	currTime;
	treeStr	*treeQuery, *treeFile;
	bool	enaAngleBrackets;

	char	*bufferAddress;
	char	*bufferOutput;
	int	bufferSize;

	void	bufferOpen(int size);
	void	bufferFlush(void);

	int	hasVersion(unsigned major, unsigned minor);
	void	_putGMT(time_t t);
	void	putContentType(cgiContentType ContentType, cgiCharSet CharSet);
	void	_putCookie(nodeStr v);
	void	putCookie(void);

	void	_readAllParams(char *string, int slen, const char *spacer, class treeStr *tree, bool post);
	int	_readAllParamsMultipart(char *string, int slen, const char *boundary);
	void	_appendValue(char *name, char *value, class treeStr *tree, bool post);
	void	_appendFile(char *name, cgiFile *value);

	void	_setHeader(unsigned dataLen);

	const char* getAuthorization(void);
	cgiMethod   getMethod(void);
	const char* getServerName(bool& WWW);
	const char* getReferer(void);
	const char* getIPaddress(void);
	const char* getLanguage(void);
	const char* getDomainTwoLevel(const char* s);
	bool	    getHTTPS(void);
	int	    getServerPort(void);
	time_t	    getModifiedSince(void);
	const char* makeHTTP(void);

	bool	checkPostParam(const char* s);
};
// ------------------------------------------------------------------------------------------------

void	cgiEnvironToLog(void);

const char	*cgiCharSetName(cgiCharSet);
const char	*cgiMethodName(cgiMethod);
const char	*cgiContentTypeName(cgiContentType contentType, cgiCharSet charSet);
const char	*cgiStatusName(cgiStatus);
int		 cgiStatusCode(cgiStatus);

void	cgiErrorMsg(const char *Status, const char *txt);
void	cgiErrorMsg_400(void);
void	cgiErrorMsg_404(void);
void	cgiErrorMsg_500(void);
void	cgiErrorMsg_503(void);



#endif

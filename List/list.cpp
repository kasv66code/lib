/* list.сpp										2020-04-15
			Класс для построения списков

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_list.hpp"


// ------------------------------------------------------------------------------------------------
void* list::operator new(size_t size, class mem* m)
{
    if (m) {
        class list* q = (class list*)m->Calloc(size);
        q->M = m;
        return q;
    }
    class list* q = (class list*)calloc(1, size);
    return q;
}

void	list::operator delete(void* p)
{
    class list* q = (class list*)p;
    class mem* m = q->M;
    if (m)
        m->Free(p);
    else
        free(p);
}

void    list::operator delete(void* p, mem* m)
{
    if (m)
        m->Free(p);
    else
        free(p);
}

// ------------------------------------------------------------------------------------------------
list::list(void* qx, __newData_nodeList_t _newData, __freeData_nodeList_t _freeData)
{
    Qx = qx;
    newData = _newData;
    freeData = _freeData;

    lockL = new mutex;
}

void    list::lockClose(void)
{
    if (lockL) {
        delete lockL;
        lockL = NULL;
    }
}

list::~list(void)
{
    lockClose();

    nodeList q, qf;
    for (q = first; q; ) {
	if (freeData) freeData(Qx, q);
	qf = q;
        q = q->next;
        if (M) M->Free(qf); else free(qf);
    }

    if (mas) {
        if (M) M->Free(mas); else free(mas);
    }
}


// ------------------------------------------------------------------------------------------------
// возвращает элемент массива по индексу idx
nodeList	list::getIndex(int idx)
{
    if (Count > 0 && idx >= 0 && idx < Count) {
        LockList();
        MasMake();
        unLockList();
        return mas[idx];
    }
    return NULL;
}


int	list::count(void)
{
    return Count;
}

nodeList	list::operator [] (int idx)
{
    return getIndex(idx);
}


// ------------------------------------------------------------------------------------------------
// узел в начале списка
nodeList	list::getFirst(void)
{
    return first;
}

// узел в конеце списка
nodeList	list::getLast(void)
{
    return last;
}


// ------------------------------------------------------------------------------------------------
// сортировка массива
void	list::sort(__sort_compare_nodeList fsort_compare)
{
    if (Count > 1) {
        LockList();
        MasMake();
        qsort(mas, Count, sizeof(nodeList*), (__qsort_compare_t)fsort_compare);
        unLockList();
    }
}


// ------------------------------------------------------------------------------------------------
void	list::MasMake(void)
{
    if (!update) return;

    if (Count > 0) {
        int size = Count * sizeof(nodeList);

        if (!mas || sizeMasMem < size) {
            if (mas) { if (M) M->Free(mas); else free(mas); }
            mas = (nodeList *)((M) ? M->Calloc(size) : calloc(1,size));
            sizeMasMem = size;
        }

        int i=0;
        for (nodeList q = list::first; q; q = q->next)
            list::mas[i++] = q;
    }
    update = false;
}

/* listAdd.сpp							                        2020-04-15
			Класс для построения списков

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_list.hpp"


// добавить узел после элемента списка
nodeList	list::AddAfter(nodeList qp)
{
    if (!qp) __exNull();

    LockList();
    nodeList q = NewNode();

    nodeList qn = qp->next;
    if (!qn)
	last = q;
    else {
	q->next = qn;
	qn->prev = q;
    }

    qp->next = q;
    q->prev = qp;
    unLockList();

    return q;
}


// добавить узел перед элементом списка
nodeList	list::AddBefore(nodeList qn)
{
    if (!qn) __exNull();

    LockList();
    nodeList q = NewNode();

    nodeList qp = qn->prev;
    if (!qp)
        list::first = q;
    else {
        qp->next = q;
        q->prev = qp;
    }

    q->next = qn;
    qn->prev = q;
    unLockList();

    return q;
}


// добавить узел в начало списка
nodeList	list::AddFirst(void)
{
    LockList();
    nodeList q = NewNode();
    if (!first) {
        // добавляем первый элемент
        first = q;
        last = q;
    }
    else {
        nodeList qn = first;
        q->next = qn;
        first = q;
        qn->prev = q;
    }
    unLockList();
    return q;
}


// добавить узел в конец списка
nodeList	list::AddLast(void)
{
    LockList();
    nodeList q = NewNode();
    if (!first) {
        // добавляем первый элемент
        first = q;
        last = q;
    }
    else {
        nodeList qp = last;
        qp->next = q;
        q->prev = qp;
        last = q;
    }
    unLockList();
    return q;
}


nodeList	list::NewNode(void)
{
    nodeList q = (nodeList)((M)?M->Calloc(sizeof(struct nodeList_t)):calloc(1,sizeof(struct nodeList_t)));
    Count++;
    update = true;

    if (newData)
        newData(Qx, q);
    return q;
}

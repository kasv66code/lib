/* list.сpp										2020-04-15
			Класс для построения списков

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "../list.hpp"
#include "../types.hpp"
#include <string.h>


using namespace std;
#define LockList()	lockL->lock()
#define unLockList()	lockL->unlock()   

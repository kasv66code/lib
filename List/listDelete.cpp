/* listDelete.сpp							                2020-04-15
			Класс для построения списков

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_list.hpp"


// удалить узел, mode==1 - удалить вместе с данными (с помощью функции freeNodeData)
void	list::Delete(nodeList q)
{
    if (!q) __exNull();

    LockList();
    if (q->data) {
	if (freeData) freeData(Qx, q);
	q->data = NULL;
    }

    if (list::first == q && list::last == q) {
	    list::first = NULL;
	    list::last = NULL;
    } else {
	if (list::first == q)
	    list::first = q->next;
	if (list::last == q)
	    list::last = q->prev;
    }

    nodeList qn = q->next;
    nodeList qp = q->prev;

    if (qn)
	qn->prev = qp;
    if (qp)
	qp->next = qn;

    if (M) M->Free(q); else free(q);

    Count--;
    update=true;
    unLockList();
}

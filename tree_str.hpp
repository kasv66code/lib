/* tree_str.hpp										2013-2020

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#ifndef	tree_str_hpp
#define	tree_str_hpp


#include "exceptions.hpp"
#include "mem.hpp"
#include <mutex>


typedef struct nodeStr_t {
    void		*data;		// структура данных пользователя
    int			numb;		// переменная integer для пользователя

    struct nodeStr_t	*next;		// список для неуникальных записей
    int			hashKey;	// hash для key для быстрого пропуску несовпадающих key
    int			keyLen;		// длина строки key
    char		key[2];		// уникальный ключ - string
} *nodeStr;


typedef int	(*__sort_compare_nodeStr)(const nodeStr *, const nodeStr *);
typedef void	(*__freeData_nodeStr_t)(void *Qx, nodeStr v);
typedef void	(*__newData_nodeStr_t)(void *Qx, nodeStr v);


// --- класс дерева Str ------------------------------------------------------------------
class treeStr {
    public:
	void *operator new(size_t size, class mem *m=NULL);
	void operator delete(void* p, mem* m);
	void operator delete(void *p);

	enum class modeKeyCmp {
	    none=0,
	    ascii,
	    utf8
	};

	treeStr(modeKeyCmp KeyCmp, void* _Qx = NULL, __newData_nodeStr_t _newData = NULL, __freeData_nodeStr_t _freeData = NULL);
	~treeStr(void);

	void	lockClose(void);		// закрыть mutex, если не предполагается вызывать десктруктор

	void	treeFree(void);
	void	treeMasFree(void);

	nodeStr	Find(const char *key);			// поиск по совпадению - возвращает адрес узла
	nodeStr	Insert(const char* key);		// добавить узел - возвращает адрес узла
	void	Delete(const char* key);		// удаляет из дерева ключ вместе с данными (если указана __free_nodeStringData)
	nodeStr	AppendDataInListLast(nodeStr q);	// добавляет элемент в конец списка (для неуникальных записей)
	nodeStr	AppendDataInListFirst(nodeStr q);	// добавляет элемент в начало списка (для неуникальных записей)

	// формирует массив из элементов дерева и возвращает элемент массива по индексу idx
	nodeStr	operator [] (int idx);

	int	count(void);

	// формирует и сортирует массив из элементов дерева
	void	sort(__sort_compare_nodeStr fsort_compare);

	friend void	*_treeStrKeyNew(class _Mem* m, void* qu, const void* key);
	friend void     _treeStrKeyFree(class _Mem* m, void* qu, void* key);
    private:
	tree		*T;
	mem		*M;
	std::mutex	*lockT;

	nodeStr	makeNodeKey(const char* key);
	void	*copyNodeKey(const void*key);
	void	nodeFree(nodeStr v);

	modeKeyCmp		ModeKeyCmp;
	void			*Qx;		// структура пользователя
	__newData_nodeStr_t	newData;	// функция для создания данных ключа
	__freeData_nodeStr_t	freeData;	// функция для удаления данных ключа
};
// ------------------------------------------------------------------ класс дерева Str ---



#endif

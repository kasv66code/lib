/* base64.cpp								2013.11.04
*/


#include "../tools.hpp"


static const char basis_64[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

int Base64encode_len(int len)
{
    return ((len + 2) / 3 * 4) + 1;
}

int Base64encode(char *encoded, const char *string, int len)
{
    int i;
    char *p;

    p = encoded;
    for (i = 0; i < len - 2; i += 3) {
	*p++ = basis_64[(string[i] >> 2) & 0x3F];
	*p++ = basis_64[((string[i] & 0x3) << 4) | (((int)(string[i + 1] & 0xF0) >> 4) & 0x0F)];
	*p++ = basis_64[((string[i + 1] & 0xF) << 2) | (((int)(string[i + 2] & 0xC0) >> 6) & 0x03)];
	*p++ = basis_64[string[i + 2] & 0x3F];
    }

    if (i < len) {
	*p++ = basis_64[(string[i] >> 2) & 0x3F];
	if (i == (len - 1)) {
	    *p++ = basis_64[((string[i] & 0x3) << 4)];
	    *p++ = '=';
	} else {
	    *p++ = basis_64[((string[i] & 0x3) << 4) | (((int) (string[i + 1] & 0xF0) >> 4) & 0x0F)];
	    *p++ = basis_64[((string[i + 1] & 0xF) << 2)];
	}
	*p++ = '=';
    }
    *p++ = 0;

    return p - encoded;
}

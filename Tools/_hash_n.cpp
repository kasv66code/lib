/* _hash_n.cpp								2013-11-04

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "../tools.hpp"


unsigned	_hash_n(const char *s, int count)
{
    unsigned int h = 0;
    /* h = h * 65599 + *s; */
    while(*s && count>0) {
	h = (h << 16) + (h << 6) - h + (unsigned char) *s++;
	count--;
    }
    return h;
}


unsigned	_hash_n(const wchar_t *s, int count)
{
    unsigned int h = 0;
    /* h = h * 65599 + *s; */
    for (; *s && count > 0; count--,s++) {
    unsigned char c = (unsigned char)((*s >> 8) & 0xff);
    h = (h << 16) + (h << 6) - h + c;

    c = (unsigned char)(*s & 0xff);
    h = (h << 16) + (h << 6) - h + c;
    }
    return h;
}

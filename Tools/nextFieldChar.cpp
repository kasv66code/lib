/* nextFieldChar.cpp							2013-11-08

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "../tools.hpp"
#include "../exceptions.hpp"
#include <stdlib.h>
#include <string.h>


char	*nextFieldChar(char **s, const char *sx)
{
    if (!s || !*s) __exNull();
    if (!sx || !*sx) __exNull();

    char *sr = *s;
    if (!*sr)
	return sr;

    char *so = strpbrk(sr,sx);
    if (so) {
	if (*so==*sx) {
	    *so = 0;
	    *s = so+1;
	} else {
	    *so = 0;
	    for(; so[1]>0 && so[1]<=' '; so++);
	    *s = so;
	}
    } else {
	*s = sr+strlen(sr);
    }

    return sr;
}

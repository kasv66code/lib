/* isUrl.cpp								2013.11.04

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/



#include "../tools.hpp"
#include "../conv_numb.hpp"
#include "../exceptions.hpp"
#include <stdlib.h>
#include <string.h>
#include <wctype.h>
#include <wchar.h>

#ifdef _WIN32
    #include "../types.hpp"
    enum punycode_status {
	punycode_success = 0,
	punycode_bad_input = 1,	/* Input is invalid.                       */
	punycode_big_output = 2,	/* Output would exceed the space provided. */
	punycode_overflow = 3	/* Wider integers needed to process input. */
    };

    typedef enum {
	PUNYCODE_SUCCESS = punycode_success,
	PUNYCODE_BAD_INPUT = punycode_bad_input,
	PUNYCODE_BIG_OUTPUT = punycode_big_output,
	PUNYCODE_OVERFLOW = punycode_overflow
    } Punycode_status;

    typedef u32 punycode_uint;

    extern int punycode_encode (size_t input_length, const punycode_uint input[], const unsigned char case_flags[], size_t * output_length, char output[]);
    extern int punycode_decode (size_t input_length, const char input[], size_t * output_length, punycode_uint output[], unsigned char case_flags[]);
    #define alloca	malloc
#else
    #include <punycode.h>
    #include <alloca.h>
    #include <unistd.h>
#endif


// ------------------------------------------------------------------------------------------------
bool	isUrlWord(char *s)
{
    bool flagAscii=0, flagIdn=0;
    char *sx, c, pc=0;
    for(sx=s; (c=*sx)!=0; pc=c, sx++) {
	if (c >= '0' && c <= '9') {
	    continue;
	}
	if ((c>='a' && c<='z') || (c>='A' && c<='Z')) {
	    if (flagIdn) {
		__logDbg("isUrlWord> flagIdn+%c", c);
		return 0;
	    }
	    flagAscii=1;
	    continue;
	}
	if (c=='-')
	    continue;
	if (c<0) {
	    if (flagAscii) {
		__logDbg("isUrlWord> flagAscii+%c", c);
		return 0;
	    }
	    flagIdn = 1;

	    wchar_t cc, cLower, cUpper;
	    int n = mbtowc(&cc, sx, strlen(sx));
	    if (n <= 0) {
		__logDbg("isUrlWord> mbtowc<=0");
		return 0;
	    }
	    sx += n-1;

	    cLower = towlower(cc);
	    cUpper = towupper(cc);

	    if (cLower!=cUpper)
		continue;
	}
	__logDbg("isUrlWord> invalid char %c", c);
	return 0;
    }
    if (*s == '-' || pc == '-') {
	__logDbg("isUrlWord> double '-'");
	return 0;
    }
    int n=strlen(s);
    if (n < 2) {
	__logDbg("isUrlWord> n<2");
	return 0;
    }
    if (!strncmp(s,"xn--", 4)) {
	size_t len=n+1;
	punycode_uint *sr = (punycode_uint *)alloca((n+1)*sizeof(punycode_uint));
	if (punycode_decode(n - 4, s + 4, &len, sr, NULL)) {
	    __logDbg("isUrlWord> punycode_decode error: %s", s);
	    return 0;
	}
	if (len < 2) {
	    __logDbg("isUrlWord> decode len<2");
	    return 0;
	}
    } else if (n>4) {
	if (s[2] == '-' && s[3] == '-') {
	    __logDbg("isUrlWord> double '-' 2");
	    return 0;
	}
    }
    return 1;
}

bool	isUrl(const char *s)
{
    if (!s || !*s)
	return 0;

    if (!strncmp(s,"http://", 7))
	s+=7;
    else if (!strncmp(s,"https://", 8))
	s+=8;
    else if (!strncmp(s,"ftp://", 6))
	s+=6;

    if (!strncmp(s,"www.", 4)) {
	s+=4;
	if (!strncmp(s,"www.", 4))
	    return 0;
    }

    char c;
    const char *se = strpbrk(s, "/:");
    if (se) {
	if (*se==':') {
	    // прочитать номер порта
	    int i=1;
	    for(; c=se[i]; i++) {
		if (c=='/')
		    break;
		if (c>='0' && c<='9')
		    continue;
		return 0;
	    }
	    i=convStrTo_i32(se+1);
	    if (i<=0 || i>0xffff)
		return 0;
	}
    }

    int n=(se)?se-s:strlen(s);
    if (n<4 || n>150)
	return 0;

    char *sx, *So = (char *)alloca(n+1);
    if (!So) return 0;
    strncpy(So, s, n);
    So[n]=0;

    int idx=0;
    for(sx=So; sx;) {
	sx = strrchr(So, '.');
	if (sx) {
	    *sx++=0;
	    if (!isUrlWord(sx))
		return 0;
	    idx++;
	} else {
	    if (!isUrlWord(So))
		return 0;
	}
    }
    return (idx>0)?1:0;
}


// ------------------------------------------------------------------------------------------------
bool	isUrl(const wchar_t *s)
{
    if (!s || !*s)
	return 0;
    int l = wcslen(s)*sizeof(wchar_t)+3;
    char *Sa = (char *)alloca(l);
    if (!Sa) return 0;

    if (wcstombs(Sa, s, l) <= 0) return 0;
    return isUrl(Sa);
}

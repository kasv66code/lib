/* runLock.cpp								2013-11-04

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "../types.hpp"
#include "../tools.hpp"
#include "../file.hpp"
#include "../exceptions.hpp"
#include "../conv_numb.hpp"

#include <stdio.h>


// ------------------------------------------------------------------------------------------------
static int	_fileRunLock = -1;

bool	runLockOpen(const char *dir_name, const char *prefix_file_name, int count, int *lockIndex)
{
    char fn[FileName_MaxSize], *s;

    if (count < 1) count = 1;

    if (!_fileNameMake(fileNameMakeMode_makeOnly, fn, dir_name, prefix_file_name, NULL)) return false;

    s=fn+strlen(fn);
    for(int i=1; i<=count; i++) {
	if (i>1)
	    sprintf(s, "-%d.lock", i);
	else
	    sprintf(s, ".lock");
	if (_fileOpen(&_fileRunLock, fn, fileOpenMode_update_or_create)) {
	    if (_fileLock(_fileRunLock, true, true)) {
		if (lockIndex) (*lockIndex) = i;

		char Sb[20];
		int len = sprintf(Sb, "%d\n", getPID());
		_fileWrite(_fileRunLock, Sb, len);
		_fileSetLen(_fileRunLock, len);
		return true;
	    }
	    _fileRunLock = _fileClose(_fileRunLock);
	}
    }
    return false;
}


#define STR_UNLOCK  "unlock\n"

void	runLockClose(void)
{
    if (_fileRunLock >= 0) {
	_fileSetPos(_fileRunLock, 0);
	_fileWrite(_fileRunLock, STR_UNLOCK, sizeof(STR_UNLOCK)-1);
	_fileSetLen(_fileRunLock, sizeof(STR_UNLOCK) - 1);
	_fileRunLock = _fileClose(_fileRunLock);
    }
}


// ------------------------------------------------------------------------------------------------
int	runLockPID(const char* dir_name, const char* prefix_file_name, int lockIndex)
{
    char fn[FileName_MaxSize];

    int Pid = 0;
    if (!_fileNameMake(fileNameMakeMode_makeOnly, fn, dir_name, prefix_file_name, NULL)) return false;

    char *s=fn+strlen(fn);
    if (lockIndex > 1)
	sprintf(s, "-%d.lock", lockIndex);
    else
	sprintf(s, ".lock");

    FILE* f = fopen(fn, "r");
    if (f) {
	char Sb[20];
	if (fgets(Sb, sizeof(Sb) - 1, f)) {
	    Sb[sizeof(Sb) - 1] = 0;
	    int x = convStrTo_i32(Sb);
	    if (x > 0) {
		Pid = x;
	    }
	}
	fclose(f);
    }

    return Pid;
}

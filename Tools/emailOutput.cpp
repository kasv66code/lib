/* emailOutput.cpp							2013-11-18

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/



#include "../tools.hpp"
#include <string.h>
#include <stdio.h>


#ifdef _WIN32
    #define popen _popen
    #define pclose _pclose
#endif


#define	SEND_MAIL	"/usr/sbin/sendmail -t"


int	emailOutput(const char *str)
{
    if (!str)
	return 0;

    for(; *str>0 && *str<=' '; str++);

    if (*str) {
	FILE *output = popen( SEND_MAIL, "w");
	if (!output)
	    return -1;

	for(; *str; str++) {
	    if (*str=='\r')
		continue;
	    fputc(*str, output);
	}

	if (pclose(output) != 0)
	    return -1;

	return 1;
    }
    return 0;
}

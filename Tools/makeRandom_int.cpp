/* makeRandom_int.cpp							2013-11-18

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/



#include "../tools.hpp"
#include <stdlib.h>


#ifdef _WIN32
    struct timeval {
	time_t	 tv_sec;
	unsigned tv_usec;
    };
    int gettimeofday(struct timeval *tp, void *tzp);
#else
    #include <sys/time.h>
#endif



int	makeRandom_int(int mod)
{
    static bool init;

    if (!init) {
	struct timeval t; 
	gettimeofday(&t, NULL);
	srand( (unsigned)(t.tv_sec / (t.tv_usec+17)) );
	init = true;
    }
    return ((rand() & 0x7fffffff) / 99) % mod;
}

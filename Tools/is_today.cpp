/* is_today.cpp									2019-08-03

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "../tools.hpp"

#ifdef _WIN32
    struct tm* localtime_r(const time_t* timep, struct tm* result);
#endif


bool	is_today(time_t t, time_t *tc)
{
    if (!t) return 0;

    struct tm tmc;
    localtime_r(&t, &tmc);
    int Tx = tmc.tm_year * 10000 + (tmc.tm_mon + 1) * 100 + tmc.tm_mday;

    if (tc)
        time(tc);
    else
        time(&t);
    localtime_r(&t, &tmc);
    int Tc = tmc.tm_year * 10000 + (tmc.tm_mon + 1) * 100 + tmc.tm_mday;

    return (Tx == Tc) ? 1 : 0;
}

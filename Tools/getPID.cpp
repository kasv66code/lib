/* getPID.cpp								2014-01-07

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "../tools.hpp"



#ifdef _WIN32
    int	getpid(void);
#else
    #include <sys/types.h>
    #include <unistd.h>
#endif



int	 getPID(void)
{
    return getpid();
}

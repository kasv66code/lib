/* runTime.cpp								2013-11-04

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "../tools.hpp"
#include "../exceptions.hpp"



void    	runTimeStart(time_t& t, int& us)
{
    getCurrTime_us(t, us);
}

unsigned	runTimeStop(time_t ts, int us)
{
    time_t tc;
    int uc;
    getCurrTime_us(tc, uc);

    unsigned d = (unsigned)(tc - ts);
    d *= 1000000;
    d += uc - us;
    return d;
}

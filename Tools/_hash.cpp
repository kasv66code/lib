/* _hash.cpp								2013-11-04

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "../tools.hpp"


unsigned	_hash(const char *s)
{
    unsigned int h = 0;
    /* h = h * 65599 + *s; */
    while(*s)
	h = (h << 16) + (h << 6) - h + (unsigned char) *s++;
    return h;
}

unsigned	_hash(const wchar_t *s)
{
    unsigned int h = 0;
    /* h = h * 65599 + *s; */
    for (; *s; s++) {
	unsigned char c = (unsigned char)((*s >> 8) & 0xff);
        h = (h << 16) + (h << 6) - h + c;

	c = (unsigned char)(*s & 0xff);
        h = (h << 16) + (h << 6) - h + c;
    }
    return h;
}

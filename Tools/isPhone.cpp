/* isPhone.cpp								2013-11-18

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/



#include "../tools.hpp"
#include <string.h>
#include <wctype.h>
#include <wchar.h> 


// ------------------------------------------------------------------------------------------------
bool	isPhone(const char *s)
{
    unsigned char c;
    int i=0, nz=0, left_c=0, defis=0, n;

    if (!s || !*s)
	return 0;

    n = strlen(s);
    if (*s=='+') {
	defis=1;
	i=1;
    }

    for(; i<n; i++) {
	c = s[i];
	if (c==' ')
	    continue;
	if (c>='0' && c<='9') {
	    nz++;
	    defis=0;
	    continue;
	}
	if (c==')') {
	    if (left_c) {
		left_c=0;
		continue;
	    }
	    return 0;
	}
	if (left_c || defis)
	    return 0;
	if (c=='(') {
	    left_c=1;
	    continue;
	}
	if (c=='-') {
	    defis=1;
	    continue;
	}
	return 0;
    }

    if (nz>4)
	return 1;

    return 0;
}


// ------------------------------------------------------------------------------------------------
bool	isPhone(const wchar_t *s)
{
    wchar_t c;
    int i = 0, nz = 0, left_c = 0, defis = 0, n;

    if (!s || !*s)
	return 0;

    n = wcslen(s);
    if (*s == L'+') {
	defis = 1;
	i = 1;
    }

    for (; i<n; i++) {
	c = s[i];
	if (c == L' ')
	    continue;
	if (c >= L'0' && c <= L'9') {
	    nz++;
	    defis = 0;
	    continue;
	}
	if (c == L')') {
	    if (left_c) {
		left_c = 0;
		continue;
	    }
	    return 0;
	}
	if (left_c || defis)
	    return 0;
	if (c == L'(') {
	    left_c = 1;
	    continue;
	}
	if (c == L'-') {
	    defis = 1;
	    continue;
	}
	return 0;
    }

    if (nz>4)
	return 1;

    return 0;
}

/* isEmail.cpp							2013-11-18

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/



#include "../tools.hpp"
#include <stdlib.h>
#include <string.h>
#include <wctype.h>
#include <wchar.h>


// ------------------------------------------------------------------------------------------------
static bool	isEmailName(const char *s)
{
    if (*s=='-' || *s=='_' || *s=='.') return 0;
    bool flagAscii=0, flagIdn=0;
    const char *sx;
    char c;
    for(sx=s; (c=*sx)!=0; sx++) {
	if (c=='@')
	    break;
	if (c>='0' && c<='9')
	    continue;
	if ((c>='a' && c<='z') || (c>='A' && c<='Z')) {
	    if (flagIdn)
		return 0;
	    flagAscii=1;
	    continue;
	}
	if (c=='-' || c=='_')
	    continue;
	if (c=='.') {
	    if (*(sx-1)=='.')
		return 0;
	    flagAscii=0;
	    flagIdn=0;
	    continue;
	}
	if (c<0) {
	    if (flagAscii)
		return 0;
	    flagIdn = 1;

	    wchar_t cc, cLower, cUpper;
	    int n = mbtowc(&cc, sx, strlen(sx));
	    if (n <= 0) 
		return 0;
	    sx += n-1;

	    cLower = towlower(cc);
	    cUpper = towupper(cc);

	    if (cLower!=cUpper)
		continue;
	}
	return 0;
    }
    return 1;
}


bool	isEmail(const char *s)
{
    if (!s || !*s) {
	return 0;
    }

    const char *Sa = strchr(s, '@');
    if (!Sa || Sa==s)
	return 0;
    if (!isUrl(Sa+1))
	return 0;

    return isEmailName(s);
}

// ------------------------------------------------------------------------------------------------
static bool	isEmailName(const wchar_t *s)
{
    if (*s==L'-' || *s==L'_' || *s==L'.') return 0;
    bool flagAscii=0, flagIdn=0;
    const wchar_t *sx;
    wchar_t c;
    for(sx=s; (c=*sx)!=0; sx++) {
	if (c==L'@')
	    break;
	if (c>=L'0' && c<=L'9')
	    continue;
	if ((c>=L'a' && c<=L'z') || (c>=L'A' && c<=L'Z')) {
	    if (flagIdn)
		return 0;
	    flagAscii=1;
	    continue;
	}
	if (c==L'-' || c==L'_')
	    continue;
	if (c==L'.') {
	    if (*(sx-1)==L'.')
		return 0;
	    flagAscii=0;
	    flagIdn=0;
	    continue;
	}
	if (c<0) {
	    if (flagAscii)
		return 0;
	    flagIdn = 1;

	    wchar_t cLower, cUpper;

	    cLower = towlower(c);
	    cUpper = towupper(c);

	    if (cLower!=cUpper)
		continue;
	}
	return 0;
    }
    return 1;
}

bool	isEmail(const wchar_t *s)
{
    if (!s || !*s) {
	return 0;
    }

    const wchar_t *Sa = wcschr(s, L'@');
    if (!Sa || Sa==s)
	return 0;
    if (!isUrl(Sa+1))
	return 0;

    return isEmailName(s);
}

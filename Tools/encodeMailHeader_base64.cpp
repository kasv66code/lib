/* encode_base64.cpp							2013.11.04

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "../tools.hpp"
#include <string.h>
#include <stdio.h>


char	*encodeMailHeader_base64(char* newstring, const char* string, const char* charset)
{
    int n, len = strlen(string);

    n = sprintf(newstring, "=?%s?b?", charset);
    n += Base64encode(newstring + n, string, len);
    strcpy(newstring + n - 1, "?=");
    return newstring;
}

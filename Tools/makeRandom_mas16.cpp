/* makeRandom_mas16.cpp							2013-11-18

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/



#include "../tools.hpp"
#include <stdlib.h>


void	makeRandom_mas16(char *s, int len)
{
    const char *m="0123456789ABCDEF";
    makeRandom_int(99);

    char c=0;
    int i, j;
    for(i=0; i<len;) {
	j = (rand() / 99) & 0x0F;
	if (c == m[j])
	    continue;
	c = m[j];
	s[i++] = c;
    }
    s[i] = 0;
}

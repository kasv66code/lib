/* getUsedMemorySize.cpp					01.04.2010

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "../tools.hpp"
#include "../exceptions.hpp"
#include <string.h>
#include <stdio.h>
#include "../conv_str.hpp"


#ifdef _WIN32
    #define strcasecmp strcmp
#endif


static FILE* openProcessStatus(int pid)
{
    char s[256];
    sprintf(s, "/proc/%d/status", pid);
    return fopen(s, "r");
}

static bool	readSizeValue(char* s, int* v)
{
    if (!s || !*s) return 0;
    if (!v) return 1;

    char b[10];
    if (sscanf(s, "%d %s", v, b) == 2) {
	if (!strcasecmp(b,"kB")) return 1;
	if (!strcasecmp(b, "MB")) {
	    *v *= 1024;
	    return 1;
	}
	if (!strcasecmp(b, "GB")) {
	    *v *= 1024 * 1024;
	    return 1;
	}

	__logErr("Parser error %s", s);
	return 0;
    }
    __logErr("Parser error %s", s);
    return 0;
}

static bool	readNameValue(char* s, char* v)
{
    if (!s || !*s || !v) return 0;

    s = convStr_clearSpace(s);
    strcpy(v, s);
    return (s && *s) ? 1 : 0;
}


// ------------------------------------------------------------------------------------------------
bool	    getProcessUsedMemorySize(int pid, int* VmRSS_kB, int* VmHWM_kB)
{
    if (VmRSS_kB) *VmRSS_kB = 0;
    if (VmHWM_kB) *VmHWM_kB = 0;

    FILE *f = openProcessStatus(pid);
    if (!f) return 0;

    bool Rs = 0;
    bool Rm = 0;
    char s[255];
    for (; fgets(s, sizeof(s) - 1, f); ) {
	s[sizeof(s) - 1] = 0;

	char* sv = strchr(s, ':');
	if (!sv) continue;
	*sv++ = 0;

	if (!strcasecmp("VmRSS", s)) {
	    if (readSizeValue(sv, VmRSS_kB))
		Rs = 1;
	    continue;
	}

	if (!strcasecmp("VmHWM", s)) {
	    if (readSizeValue(sv, VmHWM_kB))
		Rm = 1;
	    continue;
	}
    }
    fclose(f);
    return (Rs && Rm) ? 1 : 0;

}

bool	    getThisProcessUsedMemorySize(int* VmRSS_kB, int* VmHWM_kB)
{
    return getProcessUsedMemorySize(getPID(), VmRSS_kB, VmHWM_kB);
}


// ------------------------------------------------------------------------------------------------
bool	    getProcessStatus(int pid, char* Name, char* State)
{
    FILE *f = openProcessStatus(pid);
    if (!f) return 0;

    bool Rn = 0;
    bool Rs = 0;
    char s[255];
    for (; fgets(s, sizeof(s) - 1, f); ) {
	s[sizeof(s) - 1] = 0;

	char* sv = strchr(s, ':');
	if (!sv) continue;
	*sv++ = 0;

	if (!strcasecmp("Name", s)) {
	    if (readNameValue(sv, Name))
		Rn = 1;
	    continue;
	}

	if (!strcasecmp("State", s)) {
	    if (readNameValue(sv, State))
		Rs = 1;
	    continue;
	}
    }
    fclose(f);
    return (Rn && Rs) ? 1 : 0;
}


// ------------------------------------------------------------------------------------------------
bool	    isProcessWork(int pid, const char* Name)
{
    if (!Name || !*Name || pid <= 0) {
	__logErr("Parameters error pid=%d; Name=%s", pid, Name);
	return 0;
    }

    char sName[255];
    char sState[32];

    if (!getProcessStatus(pid, sName, sState)) return 0;

    if (strcasecmp(Name, sName)) return 0;
    switch (*sState) {
	case 'D':
	case 'R':
	case 'S':
	    return 1;
    }
    return 0;
}



// ------------------------------------------------------------------------------------------------

#ifdef _WIN32
    #define popen _popen
    #define pclose _pclose
    #define strdup _strdup
#endif

#define	COMMAND_WHOAMI	"whoami"


const char* getThisProcessUsername(void)
{
    static char* strUsername = NULL;

    if (!strUsername) {
	char text[255];

	FILE* f;
	f = popen(COMMAND_WHOAMI, "r");
	if (!f) return NULL;

	if (fgets(text, sizeof(text) - 1, f)) {
	    text[sizeof(text) - 1] = 0;
	    char* s = convStr_clearSpace(text);
	    if (s && *s)
		strUsername = strdup(s);
	}

	pclose(f);
    }

    return strUsername;
}

/* newIndex.cpp									2014-02-22

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "../tools.hpp"
#include "../file.hpp"
#include "../conv_numb.hpp"


int	newIndex(const char *fn, bool (*fcheck)(int id), int firstNumber)
{
    int fd;
    if (_fileOpen(&fd, fn, fileOpenMode_update_or_create)) {
	int aid=0;
	if (_fileLock(fd, true, false)) {
	    i64 _len;
	    char s[16];
	    if (_fileGetStat(fd, &_len, NULL) && _len>0 && _len<10) {
		int l = (int)_len;
		if (_fileRead(fd, s, l)==l) {
		    s[l]=0;
		    int x = convStrTo_i32(s);
		    if (x>0)
			aid = x+1;
		}
	    }
	    if (!aid) 
		aid = firstNumber;
	    if (fcheck) {
		for(; fcheck(aid); aid++)
		    ;
	    }
	    _fileSetPos(fd,0);
	    _fileSetLen(fd,0);
	    convToStr_i32(s, aid);
	    _fileWrite(fd, s, strlen(s));
	}
	_fileClose(fd);
	return aid;
    }
    return 0;
}


int	getIndex(const char *fn)
{
    int fd;
    if (_fileOpen(&fd, fn, fileOpenMode_read)) {
	int aid = 0;
	if (_fileLock(fd, false, false)) {
	    i64 _len;
	    char s[16];
	    if (_fileGetStat(fd, &_len, NULL) && _len > 0 && _len < 10) {
		int l = (int)_len;
		if (_fileRead(fd, s, l) == l) {
		    s[l] = 0;
		    aid = convStrTo_i32(s);
		}
	    }
	}
	_fileClose(fd);
	return aid;
    }
    return 0;
}


int	setIndex(const char *fn, int idx)
{
    int fd;
    if (_fileOpen(&fd, fn, fileOpenMode_update_or_create)) {
	int aid = 0;
	if (_fileLock(fd, true, false)) {
	    i64 _len;
	    char s[16];
	    if (_fileGetStat(fd, &_len, NULL) && _len > 0 && _len < 10) {
		int l = (int)_len;
		if (_fileRead(fd, s, l) == l) {
		    s[l] = 0;
		    aid = convStrTo_i32(s);
		}
	    }
	    if (idx > aid) {
		_fileSetPos(fd, 0);
		_fileSetLen(fd, 0);
		convToStr_i32(s, aid);
		_fileWrite(fd, s, strlen(s));
	    }
	}
	_fileClose(fd);
	return aid;
    }
    return 0;
}

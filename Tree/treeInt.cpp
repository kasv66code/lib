/* treeint.cpp										2020-04-12

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "../tree_int.hpp"
#include "../Mem/_tree.hpp"


using namespace std;
#define LockTree()	lockT->lock()
#define unLockTree()	lockT->unlock()


void	*treeInt::operator new(size_t size, class mem *m)
{
    if (m) {
        class treeInt* q = (class treeInt*)m->Calloc(size);
        q->M = m;
        return q;
    }
    class treeInt* q = (class treeInt*)calloc(1,size);
    return q;
}

void	treeInt::operator delete(void* p)
{
    class treeInt* q = (class treeInt*)p;
    class mem* m = q->M;
    if (m)
        m->Free(p);
    else
        free(p);
}

void    treeInt::operator delete(void* p, mem* m)
{
    if (m)
        m->Free(p);
    else
        free(p);
}

// ------------------------------------------------------------------------------------------------
void    *_treeIntKeyNew(class _Mem* m, void* qu, const void* key)
{
    treeInt *Qu = (treeInt *)qu;
    if (!Qu) __exNull();

    return Qu->makeNodeKey(key);
}



int  _treeIntKeyCompare(const void* a, const void* b)
{
    const nodeInt Qa = (const nodeInt)a, Qb = (const nodeInt)b;
    return Qa->key - Qb->key;
}

void     _treeIntKeyFree(class _Mem* m, void* qu, void* key)
{
    treeInt *Qu = (treeInt *)qu;
    if (!Qu) __exNull();
    nodeInt qp, q = (nodeInt)key;

    for (; q; q = qp) {
        qp = q->next;
        if (Qu->freeData) Qu->freeData(Qu->Qx, q);
        if (Qu->M) Qu->M->Free(q);
        else free(q);
    }
}

treeInt::treeInt(void* qx, __newData_nodeInt_t _newData, __freeData_nodeInt_t _freeData)
{
    Qx = qx;
    newData = _newData;
    freeData = _freeData;

    lockT = new mutex;
    T = new tree((M) ? M->M : NULL, this, 1, _treeIntKeyCompare, _treeIntKeyNew, _treeIntKeyFree);
}

void    treeInt::lockClose(void)
{
    if (lockT) {
        delete lockT;
        lockT = NULL;
    }
}

treeInt::~treeInt(void)
{
    if (lockT) delete lockT;
    if (T) delete T;
}

// ------------------------------------------------------------------------------------------------
void	 treeInt::treeFree(void)
{
    LockTree();
    T->treeFree();
    unLockTree();
}

void	 treeInt::treeMasFree(void)
{
    LockTree();
    T->treeMasFree();
    unLockTree();
}


nodeInt	treeInt::First(void)
{
    nodeInt R;

    LockTree();
    R = (nodeInt)T->treeFirst();
    unLockTree();

    return R;
}

nodeInt	treeInt::Last(void)
{
    nodeInt R;

    LockTree();
    R = (nodeInt)T->treeLast();
    unLockTree();

    return R;
}


// поиск по совпадению - возвращает адрес узла
nodeInt	treeInt::Find(int key)
{
    nodeInt R;
    struct nodeInt_t q;
    makeNodeKey(&q, key);

    LockTree();
    R = (nodeInt)T->treeFind(&q);
    unLockTree();

    return R;
}

// добавить узел - возвращает адрес узла
nodeInt	treeInt::Insert(int key)
{
    nodeInt R=NULL;
    struct nodeInt_t q;
    makeNodeKey(&q, key);

    LockTree();
    R = (nodeInt)T->treeInsert(&q);
    unLockTree();

    if (!R) __exNull();
    return R;
}

// удаляет из дерева ключ вместе с данными (если указана __free_nodeStringData)
void	treeInt::Delete(int key)
{
    struct nodeInt_t q;
    makeNodeKey(&q, key);

    LockTree();
    T->treeDelete(&q);
    unLockTree();
}


// добавляет элемент в конец списка (для неуникальных записей)
nodeInt	treeInt::AppendDataInListLast(nodeInt q)
{
    if (!q) __exNull();

    LockTree();
    nodeInt ql = (nodeInt)M->Calloc(sizeof(struct nodeInt_t));
    if (ql) {
        for (; q->next; q = q->next);
        q->next = ql;
    }
    unLockTree();

    if (!ql) __exNull();
    return ql;
}

// добавляет элемент в начало списка (для неуникальных записей)
nodeInt	treeInt::AppendDataInListFirst(nodeInt q)
{
    if (!q) __exNull();
    LockTree();
    nodeInt ql = (nodeInt)M->Calloc(sizeof(struct nodeInt_t));
    if (ql) {
        ql->data = q->data;
        ql->numb = q->numb;
        ql->next = q->next;

        q->next = ql;
        q->data = NULL;
        q->numb = 0;
    }
    unLockTree();

    if (!ql) __exNull();
    return ql;
}

// формирует массив из элементов дерева и возвращает элемент массива по индексу idx
nodeInt	treeInt::operator [] (int idx) 
{
    nodeInt R = NULL;

    LockTree();
    R = (nodeInt)(*T)[idx];
    unLockTree();

    return R;
}

int	treeInt::count(void)
{
    int R;

    LockTree();
    R = T->treeCount();
    unLockTree();

    return R;
}

// формирует и сортирует массив из элементов дерева
void	treeInt::sort(__sort_compare_nodeInt fsort_compare)
{
    LockTree();
    T->treeSort((__tree_sort_compare_t)fsort_compare);
    unLockTree();
}

// ------------------------------------------------------------------------------------------------
void	*treeInt::makeNodeKey(const void *qKey)
{
    if (!qKey) __exNull();
    nodeInt qx = (nodeInt)qKey;

    nodeInt q;
    if (M)
        q = (nodeInt)treeInt::M->Calloc(sizeof(struct nodeInt_t));
    else
        q = (nodeInt)calloc(1, sizeof(struct nodeInt_t));
    if (!q) __exNull();

    q->key = qx->key;

    if (newData) newData(Qx, q);

    return q;
}

void	treeInt::makeNodeKey(nodeInt q, int key)
{
    memset(q, 0, sizeof(struct nodeInt_t));
    q->key = key;
}

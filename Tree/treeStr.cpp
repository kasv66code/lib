/* treeStr.cpp										2020-04-14

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "../tree_str.hpp"
#include "../Mem/_tree.hpp"
#include "../tools.hpp"
#include "../conv_str.hpp"


using namespace std;
#define LockTree()	lockT->lock()
#define unLockTree()	lockT->unlock()   


void	*treeStr::operator new(size_t size, class mem *m)
{
    if (m) {
        class treeStr* q = (class treeStr*)m->Calloc(size);
        q->M = m;
        return q;
    }
    class treeStr* q = (class treeStr*)calloc(1,size);
    return q;
}

void	treeStr::operator delete(void* p)
{
    class treeStr* q = (class treeStr*)p;
    class mem* m = q->M;
    if (m)
        m->Free(p);
    else
        free(p);
}

void    treeStr::operator delete(void* p, mem* m)
{
    if (m)
        m->Free(p);
    else
        free(p);
}


// ------------------------------------------------------------------------------------------------
void    *_treeStrKeyNew(class _Mem* m, void* qu, const void* key)
{
    treeStr *Qu = (treeStr *)qu;
    if (!Qu) __exNull();

    return Qu->copyNodeKey(key);
}

int  _treeStrKeyCompare(const void* a, const void* b)
{
    const nodeStr Qa = (const nodeStr)a, Qb = (const nodeStr)b;

    int r = Qa->hashKey - Qb->hashKey;
    if (r != 0) return r;

    r = Qa->keyLen - Qb->keyLen;
    if (r != 0) return r;

    return strcmp(Qa->key, Qb->key);
}

void     _treeStrKeyFree(class _Mem* m, void* qu, void* key)
{
    treeStr *Qu = (treeStr *)qu;
    if (!Qu) __exNull();
    nodeStr qp, q = (nodeStr)key;

    for (; q; q = qp) {
        qp = q->next;
        if (Qu->freeData) Qu->freeData(Qu->Qx, q);

        if (Qu->M) Qu->M->Free(q); else free(q);
    }
}

treeStr::treeStr(modeKeyCmp KeyCmp, void* qx, __newData_nodeStr_t _newData, __freeData_nodeStr_t _freeData)
{
    ModeKeyCmp = KeyCmp;
    Qx = qx;
    newData = _newData;
    freeData = _freeData;

    lockT = new mutex;
    T = new tree((M) ? M->M : NULL, this, 1, _treeStrKeyCompare, _treeStrKeyNew, _treeStrKeyFree);
}

void    treeStr::lockClose(void)
{
    if (lockT) {
        delete lockT;
        lockT = NULL;
    }
}

treeStr::~treeStr(void)
{
    if (lockT) delete lockT;
    if (T) delete T;
}


// ------------------------------------------------------------------------------------------------
void	 treeStr::treeFree(void)
{
    LockTree();
    T->treeFree();
    unLockTree();
}

void	 treeStr::treeMasFree(void)
{
    LockTree();
    T->treeMasFree();
    unLockTree();
}


// поиск по совпадению - возвращает адрес узла
nodeStr	treeStr::Find(const char *key)
{
    if (!key || !*key) __exValue();

    nodeStr R;
    struct nodeStr_t *q = makeNodeKey(key);

    LockTree();
    R = (nodeStr)T->treeFind(q);
    unLockTree();

    nodeFree(q);
    return R;
}

// добавить узел - возвращает адрес узла
nodeStr	treeStr::Insert(const char* key)
{
    if (!key || !*key) __exValue();

    nodeStr R=NULL;
    struct nodeStr_t *q = makeNodeKey(key);

    LockTree();
    R = (nodeStr)T->treeInsert(q);
    unLockTree();

    nodeFree(q);
    if (!R) __exNull();
    return R;
}

// удаляет из дерева ключ вместе с данными (если указана __free_nodeStringData)
void	treeStr::Delete(const char* key)
{
    if (!key || !*key) __exValue();

    struct nodeStr_t *q = makeNodeKey(key);

    LockTree();
    T->treeDelete(q);
    unLockTree();

    nodeFree(q);
}


// добавляет элемент в конец списка (для неуникальных записей)
nodeStr	treeStr::AppendDataInListLast(nodeStr q)
{
    if (!q) __exNull();

    LockTree();
    nodeStr ql = (nodeStr)M->Calloc(sizeof(struct nodeStr_t));
    if (ql) {
        for (; q->next; q = q->next);
        q->next = ql;
    }
    unLockTree();

    if (!ql) __exNull();
    return ql;
}

// добавляет элемент в начало списка (для неуникальных записей)
nodeStr	treeStr::AppendDataInListFirst(nodeStr q)
{
    if (!q) __exNull();
    LockTree();
    nodeStr ql = (nodeStr)M->Calloc(sizeof(struct nodeStr_t));
    if (ql) {
        ql->data = q->data;
        ql->numb = q->numb;
        ql->next = q->next;

        q->next = ql;
        q->data = NULL;
        q->numb = 0;
    }
    unLockTree();

    if (!ql) __exNull();
    return ql;
}

// формирует массив из элементов дерева и возвращает элемент массива по индексу idx
nodeStr	treeStr::operator [] (int idx) 
{
    nodeStr R = NULL;

    LockTree();
    R = (nodeStr)(*T)[idx];
    unLockTree();

    return R;
}

int	treeStr::count(void)
{
    int R;

    LockTree();
    R = T->treeCount();
    unLockTree();

    return R;
}

// формирует и сортирует массив из элементов дерева
void	treeStr::sort(__sort_compare_nodeStr fsort_compare)
{
    LockTree();
    T->treeSort((__tree_sort_compare_t)fsort_compare);
    unLockTree();
}


// ------------------------------------------------------------------------------------------------
void	treeStr::nodeFree(nodeStr v)
{
    if (!v) __exNull();

    if (M) M->Free(v); else free(v);
}

void    *treeStr::copyNodeKey(const void* key)
{
    if (!key) __exNull();
    const nodeStr v = (const nodeStr)key;
    if (v->keyLen <= 0) __exValue();
    if (v->hashKey <= 0) __exValue();

    size_t ls = sizeof(nodeStr_t) + v->keyLen;

    nodeStr vn = (nodeStr)((M) ? M->Calloc(ls+1) : calloc(1, ls+1));
    memcpy(vn, v, ls);
    return vn;
}

nodeStr	    treeStr::makeNodeKey(const char* key)
{
    int len = strlen(key);
    size_t ls = sizeof(nodeStr_t) + len;

    nodeStr v = (nodeStr)((M) ? M->Calloc(ls+1) : calloc(1, ls+1));

    v->hashKey = (_hash(key) & 0x1fffffff) + 1;
    v->keyLen = len;

    switch (ModeKeyCmp) {
        case modeKeyCmp::none:
            strcpy(v->key, key);
            break;
        case modeKeyCmp::ascii:
            convStr_toLow_Anscii(v->key, key);
            break;
        case modeKeyCmp::utf8:
            convStr_toLow_UTF(v->key, key);
            break;
        default:
            __exValue();
    }

    return v;
}

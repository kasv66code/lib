/* treeTime.cpp										2020-04-14

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "../tree_time.hpp"
#include "../Mem/_tree.hpp"


using namespace std;
#define LockTree()	lockT->lock()
#define unLockTree()	lockT->unlock()   


void	*treeTime::operator new(size_t size, class mem *m)
{
    if (m) {
        class treeTime* q = (class treeTime*)m->Calloc(size);
        q->M = m;
        return q;
    }
    class treeTime* q = (class treeTime*)calloc(1,size);
    return q;
}

void	treeTime::operator delete(void* p)
{
    class treeTime* q = (class treeTime*)p;
    class mem* m = q->M;
    if (m)
        m->Free(p);
    else
        free(p);
}

void    treeTime::operator delete(void* p, mem* m)
{
    if (m)
        m->Free(p);
    else
        free(p);
}

// ------------------------------------------------------------------------------------------------
void    *_treeTimeKeyNew(class _Mem* m, void* qu, const void* key)
{
    treeTime *Qu = (treeTime *)qu;
    if (!Qu) __exNull();

    return Qu->makeNodeKey(key);
}



int  _treeTimeKeyCompare(const void* a, const void* b)
{
    const nodeTime Qa = (const nodeTime)a, Qb = (const nodeTime)b;
    time_t r = Qa->key - Qb->key;
    if (r > 0) return 1;
    if (r < 0) return -1;
    return 0;
}

void     _treeTimeKeyFree(class _Mem* m, void* qu, void* key)
{
    treeTime *Qu = (treeTime *)qu;
    if (!Qu) __exNull();
    nodeTime qp, q = (nodeTime)key;

    for (; q; q = qp) {
        qp = q->next;
        if (Qu->freeData) Qu->freeData(Qu->Qx, q);
        if (Qu->M) Qu->M->Free(q);
        else free(q);
    }
}

treeTime::treeTime(void* qx, __newData_nodeTime_t _newData, __freeData_nodeTime_t _freeData)
{
    Qx = qx;
    newData = _newData;
    freeData = _freeData;

    lockT = new mutex;
    T = new tree((M) ? M->M : NULL, this, 1, _treeTimeKeyCompare, _treeTimeKeyNew, _treeTimeKeyFree);
}

void    treeTime::lockClose(void)
{
    if (lockT) {
        delete lockT;
        lockT = NULL;
    }
}

treeTime::~treeTime(void)
{
    if (lockT) delete lockT;
    if (T) delete T;
}

// ------------------------------------------------------------------------------------------------
void	 treeTime::treeFree(void)
{
    LockTree();
    T->treeFree();
    unLockTree();
}

void	 treeTime::treeMasFree(void)
{
    LockTree();
    T->treeMasFree();
    unLockTree();
}


nodeTime	treeTime::First(void)
{
    nodeTime R;

    LockTree();
    R = (nodeTime)T->treeFirst();
    unLockTree();

    return R;
}

nodeTime	treeTime::Last(void)
{
    nodeTime R;

    LockTree();
    R = (nodeTime)T->treeLast();
    unLockTree();

    return R;
}


// поиск по совпадению - возвращает адрес узла
nodeTime	treeTime::Find(time_t key)
{
    nodeTime R;
    struct nodeTime_t q;
    makeNodeKey(&q, key);

    LockTree();
    R = (nodeTime)T->treeFind(&q);
    unLockTree();

    return R;
}

// добавить узел - возвращает адрес узла
nodeTime	treeTime::Insert(time_t key)
{
    nodeTime R=NULL;
    struct nodeTime_t q;
    makeNodeKey(&q, key);

    LockTree();
    R = (nodeTime)T->treeInsert(&q);
    unLockTree();

    if (!R) __exNull();
    return R;
}

// удаляет из дерева ключ вместе с данными (если указана __free_nodeStringData)
void	treeTime::Delete(time_t key)
{
    struct nodeTime_t q;
    makeNodeKey(&q, key);

    LockTree();
    T->treeDelete(&q);
    unLockTree();
}


// добавляет элемент в конец списка (для неуникальных записей)
nodeTime	treeTime::AppendDataInListLast(nodeTime q)
{
    if (!q) __exNull();

    LockTree();
    nodeTime ql = (nodeTime)M->Calloc(sizeof(struct nodeTime_t));
    if (ql) {
        for (; q->next; q = q->next);
        q->next = ql;
    }
    unLockTree();

    if (!ql) __exNull();
    return ql;
}

// добавляет элемент в начало списка (для неуникальных записей)
nodeTime	treeTime::AppendDataInListFirst(nodeTime q)
{
    if (!q) __exNull();
    LockTree();
    nodeTime ql = (nodeTime)M->Calloc(sizeof(struct nodeTime_t));
    if (ql) {
        ql->data = q->data;
        ql->numb = q->numb;
        ql->next = q->next;

        q->next = ql;
        q->data = NULL;
        q->numb = 0;
    }
    unLockTree();

    if (!ql) __exNull();
    return ql;
}

// формирует массив из элементов дерева и возвращает элемент массива по индексу idx
nodeTime	treeTime::operator [] (int idx) 
{
    nodeTime R = NULL;

    LockTree();
    R = (nodeTime)(*T)[idx];
    unLockTree();

    return R;
}

int	treeTime::count(void)
{
    int R;

    LockTree();
    R = T->treeCount();
    unLockTree();

    return R;
}

// формирует и сортирует массив из элементов дерева
void	treeTime::sort(__sort_compare_nodeTime fsort_compare)
{
    LockTree();
    T->treeSort((__tree_sort_compare_t)fsort_compare);
    unLockTree();
}

// ------------------------------------------------------------------------------------------------
void	*treeTime::makeNodeKey(const void *qKey)
{
    if (!qKey) __exNull();
    nodeTime qx = (nodeTime)qKey;

    nodeTime q;
    if (M)
        q = (nodeTime)treeTime::M->Calloc(sizeof(struct nodeTime_t));
    else
        q = (nodeTime)calloc(1, sizeof(struct nodeTime_t));
    if (!q) __exNull();

    q->key = qx->key;

    if (newData) newData(Qx, q);

    return q;
}

void	treeTime::makeNodeKey(nodeTime q, time_t key)
{
    memset(q, 0, sizeof(struct nodeTime_t));
    q->key = key;
}

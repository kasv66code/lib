/* treeWStr.cpp										2020-04-14

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "../tree_wstr.hpp"
#include "../Mem/_tree.hpp"
#include "../tools.hpp"
#include "../conv_str.hpp"


using namespace std;
#define LockTree()	lockT->lock()
#define unLockTree()	lockT->unlock()   


void	*treeWStr::operator new(size_t size, class mem *m)
{
    if (m) {
        class treeWStr* q = (class treeWStr*)m->Calloc(size);
        q->M = m;
        return q;
    }
    class treeWStr* q = (class treeWStr*)calloc(1,size);
    return q;
}

void	treeWStr::operator delete(void* p)
{
    class treeWStr* q = (class treeWStr*)p;
    class mem* m = q->M;
    if (m)
        m->Free(p);
    else
        free(p);
}

void    treeWStr::operator delete(void* p, mem* m)
{
    if (m)
        m->Free(p);
    else
        free(p);
}


// ------------------------------------------------------------------------------------------------
void    *_treeWStrKeyNew(class _Mem* m, void* qu, const void* key)
{
    treeWStr *Qu = (treeWStr *)qu;
    if (!Qu) __exNull();

    return Qu->copyNodeKey(key);
}

int  _treeWStrKeyCompare(const void* a, const void* b)
{
    const nodeWStr Qa = (const nodeWStr)a, Qb = (const nodeWStr)b;

    int r = Qa->hashKey - Qb->hashKey;
    if (r != 0) return r;

    r = Qa->keyLen - Qb->keyLen;
    if (r != 0) return r;

    return wcscmp(Qa->key, Qb->key);
}

void     _treeWStrKeyFree(class _Mem* m, void* qu, void* key)
{
    treeWStr *Qu = (treeWStr *)qu;
    if (!Qu) __exNull();
    nodeWStr qp, q = (nodeWStr)key;

    for (; q; q = qp) {
        qp = q->next;
        if (Qu->freeData) Qu->freeData(Qu->Qx, q);

        if (Qu->M) Qu->M->Free(q); else free(q);
    }
}

treeWStr::treeWStr(modeKeyCmp KeyCmp, void* qx, __newData_nodeWStr_t _newData, __freeData_nodeWStr_t _freeData)
{
    ModeKeyCmp = KeyCmp;
    Qx = qx;
    newData = _newData;
    freeData = _freeData;

    lockT = new mutex;
    T = new tree((M) ? M->M : NULL, this, 1, _treeWStrKeyCompare, _treeWStrKeyNew, _treeWStrKeyFree);
}

void    treeWStr::lockClose(void)
{
    if (lockT) {
        delete lockT;
        lockT = NULL;
    }
}

treeWStr::~treeWStr(void)
{
    if (lockT) delete lockT;
    if (T) delete T;
}


// ------------------------------------------------------------------------------------------------
void	 treeWStr::treeFree(void)
{
    LockTree();
    T->treeFree();
    unLockTree();
}

void	 treeWStr::treeMasFree(void)
{
    LockTree();
    T->treeMasFree();
    unLockTree();
}


// поиск по совпадению - возвращает адрес узла
nodeWStr	treeWStr::Find(const wchar_t *key)
{
    if (!key || !*key) __exValue();

    nodeWStr R;
    struct nodeWStr_t *q = makeNodeKey(key);

    LockTree();
    R = (nodeWStr)T->treeFind(q);
    unLockTree();

    nodeFree(q);
    return R;
}

// добавить узел - возвращает адрес узла
nodeWStr	treeWStr::Insert(const wchar_t* key)
{
    if (!key || !*key) __exValue();

    nodeWStr R=NULL;
    struct nodeWStr_t *q = makeNodeKey(key);

    LockTree();
    R = (nodeWStr)T->treeInsert(q);
    unLockTree();

    nodeFree(q);
    if (!R) __exNull();
    return R;
}

// удаляет из дерева ключ вместе с данными (если указана __free_nodeWStringData)
void	treeWStr::Delete(const wchar_t* key)
{
    if (!key || !*key) __exValue();

    struct nodeWStr_t *q = makeNodeKey(key);

    LockTree();
    T->treeDelete(q);
    unLockTree();

    nodeFree(q);
}


// добавляет элемент в конец списка (для неуникальных записей)
nodeWStr	treeWStr::AppendDataInListLast(nodeWStr q)
{
    if (!q) __exNull();

    LockTree();
    nodeWStr ql = (nodeWStr)M->Calloc(sizeof(struct nodeWStr_t));
    if (ql) {
        for (; q->next; q = q->next);
        q->next = ql;
    }
    unLockTree();

    if (!ql) __exNull();
    return ql;
}

// добавляет элемент в начало списка (для неуникальных записей)
nodeWStr	treeWStr::AppendDataInListFirst(nodeWStr q)
{
    if (!q) __exNull();
    LockTree();
    nodeWStr ql = (nodeWStr)M->Calloc(sizeof(struct nodeWStr_t));
    if (ql) {
        ql->data = q->data;
        ql->numb = q->numb;
        ql->next = q->next;

        q->next = ql;
        q->data = NULL;
        q->numb = 0;
    }
    unLockTree();

    if (!ql) __exNull();
    return ql;
}

// формирует массив из элементов дерева и возвращает элемент массива по индексу idx
nodeWStr	treeWStr::operator [] (int idx) 
{
    nodeWStr R = NULL;

    LockTree();
    R = (nodeWStr)(*T)[idx];
    unLockTree();

    return R;
}

int	treeWStr::count(void)
{
    int R;

    LockTree();
    R = T->treeCount();
    unLockTree();

    return R;
}

// формирует и сортирует массив из элементов дерева
void	treeWStr::sort(__sort_compare_nodeWStr fsort_compare)
{
    LockTree();
    T->treeSort((__tree_sort_compare_t)fsort_compare);
    unLockTree();
}


// ------------------------------------------------------------------------------------------------
void	treeWStr::nodeFree(nodeWStr v)
{
    if (!v) __exNull();

    if (M) M->Free(v); else free(v);
}


void    *treeWStr::copyNodeKey(const void* key)
{
    if (!key) __exNull();
    const nodeWStr v = (const nodeWStr)key;
    if (v->keyLen <= 0) __exValue();
    if (v->hashKey <= 0) __exValue();

    size_t ls = sizeof(nodeWStr_t) + (v->keyLen * sizeof(wchar_t));

    nodeWStr vn = (nodeWStr)((M) ? M->Calloc(ls) : calloc(1, ls));
    memcpy(vn, v, ls);
    return vn;
}


nodeWStr	    treeWStr::makeNodeKey(const wchar_t* key)
{
    int len = wcslen(key);
    size_t ls = sizeof(nodeWStr_t) + (len * sizeof(wchar_t));

    nodeWStr v = (nodeWStr)((M) ? M->Calloc(ls) : calloc(1, ls));

    v->hashKey = (_hash(key) & 0x1fffffff) + 1;
    v->keyLen = len;

    switch (ModeKeyCmp) {
        case modeKeyCmp::none:
            wcscpy(v->key, key);
            break;
        case modeKeyCmp::lower:
            convWStr_toLow(v->key, key);
            break;
        default:
            __exValue();
    }

    return v;
}

/* treeInt64.cpp										2020-04-14

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "../tree_int64.hpp"
#include "../Mem/_tree.hpp"


using namespace std;
#define LockTree()	lockT->lock()
#define unLockTree()	lockT->unlock()   


void	*treeInt64::operator new(size_t size, class mem *m)
{
    if (m) {
        class treeInt64* q = (class treeInt64*)m->Calloc(size);
        q->M = m;
        return q;
    }
    class treeInt64* q = (class treeInt64*)calloc(1,size);
    return q;
}

void	treeInt64::operator delete(void* p)
{
    class treeInt64* q = (class treeInt64*)p;
    class mem* m = q->M;
    if (m)
        m->Free(p);
    else
        free(p);
}

void    treeInt64::operator delete(void* p, mem* m)
{
    if (m)
        m->Free(p);
    else
        free(p);
}

// ------------------------------------------------------------------------------------------------
void    *_treeInt64KeyNew(class _Mem* m, void* qu, const void* key)
{
    treeInt64 *Qu = (treeInt64 *)qu;
    if (!Qu) __exNull();

    return Qu->makeNodeKey(key);
}



int  _treeInt64KeyCompare(const void* a, const void* b)
{
    const nodeInt64 Qa = (const nodeInt64)a, Qb = (const nodeInt64)b;
    i64 r = Qa->key - Qb->key;
    if (r > 0) return 1;
    if (r < 0) return -1;
    return 0;
}

void     _treeInt64KeyFree(class _Mem* m, void* qu, void* key)
{
    treeInt64 *Qu = (treeInt64 *)qu;
    if (!Qu) __exNull();
    nodeInt64 qp, q = (nodeInt64)key;

    for (; q; q = qp) {
        qp = q->next;
        if (Qu->freeData) Qu->freeData(Qu->Qx, q);
        if (Qu->M) Qu->M->Free(q);
        else free(q);
    }
}

treeInt64::treeInt64(void* qx, __newData_nodeInt64_t _newData, __freeData_nodeInt64_t _freeData)
{
    Qx = qx;
    newData = _newData;
    freeData = _freeData;

    lockT = new mutex;
    T = new tree((M) ? M->M : NULL, this, 1, _treeInt64KeyCompare, _treeInt64KeyNew, _treeInt64KeyFree);
}

void    treeInt64::lockClose(void)
{
    if (lockT) {
        delete lockT;
        lockT = NULL;
    }
}

treeInt64::~treeInt64(void)
{
    if (lockT) delete lockT;
    if (T) delete T;
}

// ------------------------------------------------------------------------------------------------
void	 treeInt64::treeFree(void)
{
    LockTree();
    T->treeFree();
    unLockTree();
}

void	 treeInt64::treeMasFree(void)
{
    LockTree();
    T->treeMasFree();
    unLockTree();
}


nodeInt64	treeInt64::First(void)
{
    nodeInt64 R;

    LockTree();
    R = (nodeInt64)T->treeFirst();
    unLockTree();

    return R;
}

nodeInt64	treeInt64::Last(void)
{
    nodeInt64 R;

    LockTree();
    R = (nodeInt64)T->treeLast();
    unLockTree();

    return R;
}


// поиск по совпадению - возвращает адрес узла
nodeInt64	treeInt64::Find(i64 key)
{
    nodeInt64 R;
    struct nodeInt64_t q;
    makeNodeKey(&q, key);

    LockTree();
    R = (nodeInt64)T->treeFind(&q);
    unLockTree();

    return R;
}

// добавить узел - возвращает адрес узла
nodeInt64	treeInt64::Insert(i64 key)
{
    nodeInt64 R=NULL;
    struct nodeInt64_t q;
    makeNodeKey(&q, key);

    LockTree();
    R = (nodeInt64)T->treeInsert(&q);
    unLockTree();

    if (!R) __exNull();
    return R;
}

// удаляет из дерева ключ вместе с данными (если указана __free_nodeStringData)
void	treeInt64::Delete(i64 key)
{
    struct nodeInt64_t q;
    makeNodeKey(&q, key);

    LockTree();
    T->treeDelete(&q);
    unLockTree();
}


// добавляет элемент в конец списка (для неуникальных записей)
nodeInt64	treeInt64::AppendDataInListLast(nodeInt64 q)
{
    if (!q) __exNull();

    LockTree();
    nodeInt64 ql = (nodeInt64)M->Calloc(sizeof(struct nodeInt64_t));
    if (ql) {
        for (; q->next; q = q->next);
        q->next = ql;
    }
    unLockTree();

    if (!ql) __exNull();
    return ql;
}

// добавляет элемент в начало списка (для неуникальных записей)
nodeInt64	treeInt64::AppendDataInListFirst(nodeInt64 q)
{
    if (!q) __exNull();
    LockTree();
    nodeInt64 ql = (nodeInt64)M->Calloc(sizeof(struct nodeInt64_t));
    if (ql) {
        ql->data = q->data;
        ql->numb = q->numb;
        ql->next = q->next;

        q->next = ql;
        q->data = NULL;
        q->numb = 0;
    }
    unLockTree();

    if (!ql) __exNull();
    return ql;
}

// формирует массив из элементов дерева и возвращает элемент массива по индексу idx
nodeInt64	treeInt64::operator [] (int idx) 
{
    nodeInt64 R = NULL;

    LockTree();
    R = (nodeInt64)(*T)[idx];
    unLockTree();

    return R;
}

int	treeInt64::count(void)
{
    int R;

    LockTree();
    R = T->treeCount();
    unLockTree();

    return R;
}

// формирует и сортирует массив из элементов дерева
void	treeInt64::sort(__sort_compare_nodeInt64 fsort_compare)
{
    LockTree();
    T->treeSort((__tree_sort_compare_t)fsort_compare);
    unLockTree();
}

// ------------------------------------------------------------------------------------------------
void	*treeInt64::makeNodeKey(const void *qKey)
{
    if (!qKey) __exNull();
    nodeInt64 qx = (nodeInt64)qKey;

    nodeInt64 q;
    if (M)
        q = (nodeInt64)treeInt64::M->Calloc(sizeof(struct nodeInt64_t));
    else
        q = (nodeInt64)calloc(1, sizeof(struct nodeInt64_t));
    if (!q) __exNull();

    q->key = qx->key;

    if (newData) newData(Qx, q);

    return q;
}

void	treeInt64::makeNodeKey(nodeInt64 q, i64 key)
{
    memset(q, 0, sizeof(struct nodeInt64_t));
    q->key = key;
}

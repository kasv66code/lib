/* Double.cpp										2020-04-14

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "../tree_double.hpp"
#include "../Mem/_tree.hpp"


using namespace std;
#define LockTree()	lockT->lock()
#define unLockTree()	lockT->unlock()   


void	*treeDouble::operator new(size_t size, class mem *m)
{
    if (m) {
        class treeDouble* q = (class treeDouble*)m->Calloc(size);
        q->M = m;
        return q;
    }
    class treeDouble* q = (class treeDouble*)calloc(1,size);
    return q;
}

void	treeDouble::operator delete(void* p)
{
    class treeDouble* q = (class treeDouble*)p;
    class mem* m = q->M;
    if (m)
        m->Free(p);
    else
        free(p);
}

void    treeDouble::operator delete(void* p, mem* m)
{
    if (m)
        m->Free(p);
    else
        free(p);
}

// ------------------------------------------------------------------------------------------------
void    *_treeDoubleKeyNew(class _Mem* m, void* qu, const void* key)
{
    treeDouble *Qu = (treeDouble *)qu;
    if (!Qu) __exNull();

    return Qu->makeNodeKey(key);
}



int  _treeDoubleKeyCompare(const void* a, const void* b)
{
    const nodeDouble Qa = (const nodeDouble)a, Qb = (const nodeDouble)b;
    double r = Qa->key - Qb->key;
    if (r > 0) return 1;
    if (r < 0) return -1;
    return 0;
}

void     _treeDoubleKeyFree(class _Mem* m, void* qu, void* key)
{
    treeDouble *Qu = (treeDouble *)qu;
    if (!Qu) __exNull();
    nodeDouble qp, q = (nodeDouble)key;

    for (; q; q = qp) {
        qp = q->next;
        if (Qu->freeData) Qu->freeData(Qu->Qx, q);
        if (Qu->M) Qu->M->Free(q);
        else free(q);
    }
}

treeDouble::treeDouble(void* qx, __newData_nodeDouble_t _newData, __freeData_nodeDouble_t _freeData)
{
    Qx = qx;
    newData = _newData;
    freeData = _freeData;

    lockT = new mutex;
    T = new tree((M) ? M->M : NULL, this, 1, _treeDoubleKeyCompare, _treeDoubleKeyNew, _treeDoubleKeyFree);
}

void    treeDouble::lockClose(void)
{
    if (lockT) {
        delete lockT;
        lockT = NULL;
    }
}

treeDouble::~treeDouble(void)
{
    if (lockT) delete lockT;
    if (T) delete T;
}

// ------------------------------------------------------------------------------------------------
void	 treeDouble::treeFree(void)
{
    LockTree();
    T->treeFree();
    unLockTree();
}

void	 treeDouble::treeMasFree(void)
{
    LockTree();
    T->treeMasFree();
    unLockTree();
}


nodeDouble	treeDouble::First(void)
{
    nodeDouble R;

    LockTree();
    R = (nodeDouble)T->treeFirst();
    unLockTree();

    return R;
}

nodeDouble	treeDouble::Last(void)
{
    nodeDouble R;

    LockTree();
    R = (nodeDouble)T->treeLast();
    unLockTree();

    return R;
}


// поиск по совпадению - возвращает адрес узла
nodeDouble	treeDouble::Find(double key)
{
    nodeDouble R;
    struct nodeDouble_t q;
    makeNodeKey(&q, key);

    LockTree();
    R = (nodeDouble)T->treeFind(&q);
    unLockTree();

    return R;
}

// добавить узел - возвращает адрес узла
nodeDouble	treeDouble::Insert(double key)
{
    nodeDouble R=NULL;
    struct nodeDouble_t q;
    makeNodeKey(&q, key);

    LockTree();
    R = (nodeDouble)T->treeInsert(&q);
    unLockTree();

    if (!R) __exNull();
    return R;
}

// удаляет из дерева ключ вместе с данными (если указана __free_nodeStringData)
void	treeDouble::Delete(double key)
{
    struct nodeDouble_t q;
    makeNodeKey(&q, key);

    LockTree();
    T->treeDelete(&q);
    unLockTree();
}


// добавляет элемент в конец списка (для неуникальных записей)
nodeDouble	treeDouble::AppendDataInListLast(nodeDouble q)
{
    if (!q) __exNull();

    LockTree();
    nodeDouble ql = (nodeDouble)M->Calloc(sizeof(struct nodeDouble_t));
    if (ql) {
        for (; q->next; q = q->next);
        q->next = ql;
    }
    unLockTree();

    if (!ql) __exNull();
    return ql;
}

// добавляет элемент в начало списка (для неуникальных записей)
nodeDouble	treeDouble::AppendDataInListFirst(nodeDouble q)
{
    if (!q) __exNull();
    LockTree();
    nodeDouble ql = (nodeDouble)M->Calloc(sizeof(struct nodeDouble_t));
    if (ql) {
        ql->data = q->data;
        ql->numb = q->numb;
        ql->next = q->next;

        q->next = ql;
        q->data = NULL;
        q->numb = 0;
    }
    unLockTree();

    if (!ql) __exNull();
    return ql;
}

// формирует массив из элементов дерева и возвращает элемент массива по индексу idx
nodeDouble	treeDouble::operator [] (int idx) 
{
    nodeDouble R = NULL;

    LockTree();
    R = (nodeDouble)(*T)[idx];
    unLockTree();

    return R;
}

int	treeDouble::count(void)
{
    int R;

    LockTree();
    R = T->treeCount();
    unLockTree();

    return R;
}

// формирует и сортирует массив из элементов дерева
void	treeDouble::sort(__sort_compare_nodeDouble fsort_compare)
{
    LockTree();
    T->treeSort((__tree_sort_compare_t)fsort_compare);
    unLockTree();
}

// ------------------------------------------------------------------------------------------------
void	*treeDouble::makeNodeKey(const void *qKey)
{
    if (!qKey) __exNull();
    nodeDouble qx = (nodeDouble)qKey;

    nodeDouble q;
    if (M)
        q = (nodeDouble)treeDouble::M->Calloc(sizeof(struct nodeDouble_t));
    else
        q = (nodeDouble)calloc(1, sizeof(struct nodeDouble_t));
    if (!q) __exNull();

    q->key = qx->key;

    if (newData) newData(Qx, q);

    return q;
}

void	treeDouble::makeNodeKey(nodeDouble q, double key)
{
    memset(q, 0, sizeof(struct nodeDouble_t));
    q->key = key;
}

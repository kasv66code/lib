/*  shablon.hpp										2012-2020

			Шаблонизатор

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#ifndef _shablon_hpp
#define _shablon_hpp


#include "tree_str.hpp"
#include <time.h>


class shablon;

typedef bool	(*__sh_proc_tag_t)(shablon*sh, const char *tag);
typedef bool	(*__sh_proc_special_t)(shablon*sh, const char *tag, const char *str);
typedef void	(*__sh_proc_run_t)(shablon*sh);
typedef void	(*__sh_proc_aftereffect_t)(shablon*sh);


// --- класс для кэширования файлов ---------------------------------------------------------------
#define CACHE_LIFE_SECOND	3

typedef struct {
    char    *f_data;	// содержимое файла
    int	    f_length;	// длина файла
    time_t  f_mtime;	// время последнего изменения файла
    time_t  t_check_tm;	// время последней проверки времени изменения файла или его загрузки
    bool    f_NotFound;	// признак, что файл отсутствует 
} cacheFileData;

class cacheFiles {
    public:
	void* operator new(size_t size, mem* m = NULL);
	void operator delete(void* p, mem* m);
	void operator delete(void *p);

	cacheFiles(void);
	~cacheFiles(void);

	cacheFileData	*getFile(const char *dir, const char *name);

    private:
	class mem	*M;
	treeStr		*tree;

	char	*makeFileNameFull(const char *dn, const char *fn);
};
// ------------------------------------------------------------------------------------------------


// shablon format string:
//	'j' - shablonFORMAT_JS
//	'h' - shablonFORMAT_HTML
//	'u' - shablonFORMAT_URL
//	'b' - shablonFORMAT_NL_to_BR
//	't' - shablonFORMAT_HTML_TEXTAREA
//	's' - shablonFORMAT_JSON

// --- класс формирования строки на основе шаблонов -----------------------------------------------
class shablon {
    public:
	void *operator new(size_t size, mem *m = NULL);
	void operator delete(void* p, mem* m);
	void operator delete(void *p);

	shablon(void);
	~shablon(void);

	bool	tagAdd(const char *tag, __sh_proc_tag_t func);
	bool	tagExec(const char *tag);
	char	*tagExecToMem(const char *tag);

	int	run(void);		// выполнить шаблон

	__sh_proc_run_t	runFunction;
	const char* runString;		// строка-шаблон для выполнения
	const char* runFileName;	// название файла шаблона

	const char* dirName;		// путь к папке файлов шаблонов
	const char* dirNameAlt;		// альтернативный путь к папке файлов шаблонов
	cacheFiles* files;

	bool	spaceNotUpdate;		// если true, то не прореживать пробелы, \t и \n

	__sh_proc_special_t	func_percent;
	__sh_proc_special_t	func_at;
	__sh_proc_aftereffect_t func_aftereffect;

	void	putChar(char c);
	void	putStr(const char *str, int len=0);				// выдает строку c преобразованием по формату в тэге (если преобразование указано, если не указано, то без преобразования)
	void	putStrF(const char *format, const char *str, int len=0);	// выдает строку c преобразованием по формату в тэге или, если оно не указано, то по формату в параметре
	void	putPrintf(const char *pFormat, ...);				// выдает строку как printf

	void	putChar(wchar_t c);
	void	putStr(const wchar_t *str, int len = 0);			// выдает строку c преобразованием по формату в тэге (если преобразование указано, если не указано, то без преобразования)
	void	putStrF(const char *format, const wchar_t *str, int len = 0);	// выдает строку c преобразованием по формату в тэге или, если оно не указано, то по формату в параметре
	void	putPrintf(const wchar_t *pFormat, ...);				// выдает строку как printf

	void	putNumb(int d);					// выдает десятичное число
	void	putNumb(long long int d);			// выдает десятичное число
	void	putNumbHex(int d);				// выдает шестнадцатиричное число
	void	putNumbHex(long long int d);			// выдает шестнадцатиричное число
	void	putTime(time_t t);							// выдает время, как десятичное число
	void	putTime(time_t t, const char *formatTime, const char **month = NULL);	// выдает время по формату 
	void	putDouble(double d);			// выдает десятичное число, количество знаков после запятой макс (нули справа убираются)
	void	putDouble(double d, int n);		// выдает десятичное число, n-количество знаков после запятой
	void	putDoubleAuto(double d);		// выдает десятичное число, количество знаков после запятой определяется в зависимости от значения
	void	putNumbDelimited(int d);		// выдает десятичное число с разделителями по три разряда "1 000"
	void	putDoubleDelimited(double d, int n);	// выдает десятичное число с разделителями по три разряда "1 000", n-количество знаков после запятой

	void	putJsonBool(const char* tagName, bool v);
	void	putJsonInt(const char* tagName, int v);
	void	putJsonInt(const char* tagName, long long int v);
	void	putJsonTime(const char* tagName, time_t v);
	void	putJsonDouble(const char* tagName, double v);
	void	putJsonDouble(const char* tagName, double v, int n);
	void	putJsonStr(const char* tagName, const char* v);
	void	putJsonStr(const char* tagName, const wchar_t* v);

//	void	putJSON(json *v);

	void	buffCopy(char *s);
	int	buffLength(void);
	void	buffClear(void);


    private:
	mem		*M;
	class treeStr	*tags;

	int	formatCurrent;

	typedef struct shablonBuffOutput_t {
	    char *s;
	    int size, len;

	    struct shablonBuffOutput_t *next;
	} shablonBuffOutput;

	shablonBuffOutput	*buffOutputFirst, *buffOutputCurr;
	int	lengthFull;
	int	spaceMode;
	char	lastSymbol;
	bool	enaInclude;

	bool	buffAltActive;
	shablonBuffOutput	*buffOutputFirsAlt;

	void	newBuffOutput(void);

	void	loopFile(const char *shablon_file_name, int recurse_level);
	int	loopStr(char *str, int recurse_level);
	void	loopLocal(char *str, char *name);
	int	loopFor(char *str, int recurse_level);
	char	*loopIF(char *So, char c, int recurse_level);

	void		formatGet(const char *tag_name);
	const char	*formatCurrSet(const char *tag_name);
	bool		__tagExec(const char *tag, const char *s, int recurse_level);
	bool		_tagExec(const char *tag, const char *s, int recurse_level);

	void	_putCharInBuff(char c);
	void	_putStrInBuff(const char *str, int len=0);
	void	_putStrInBuff(const wchar_t *str, int len);

	void	*add_fragment(const char* s, int type);
};
// ------------------------------------------------------------------------------------------------

inline bool	shablonPutTrue(shablon* sh, const char *tag) {return true;}
inline bool	shablonPutFalse(shablon* sh, const char *tag) {return false;}



#endif

/*  free_HTTP_reply.cpp								2018-05-16

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "../http.hpp"


void	*HTTP::operator new(size_t size, mem *m)
{
    HTTP* q = NULL;
    if (m) {
        q = (HTTP*)m->Calloc(size);
        q->M = m;
    } else {
        q = (HTTP*)calloc(1,size);
    }
    if (!q) {
        __exNull();
    }

    return q;
}

void	HTTP::operator delete(void *p)
{
    HTTP *q = (class HTTP *)p;
    mem *m = q->M;
    if (m)
        m->Free(p);
    else
        free(p);
}

void    HTTP::operator delete(void* p, mem* m)
{
    if (m)
        m->Free(p);
    else
        free(p);
}

// Конструктор ------------------------------------------------------------------------------------
HTTP::HTTP(void)
{
}
// ------------------------------------------------------------------------------------------------


// Деструктор -------------------------------------------------------------------------------------
HTTP::~HTTP(void)
{
    HTTP::Clear();
}
// ------------------------------------------------------------------------------------------------

void	HTTP::Clear(void)
{
    if (M) {
        if (header) M->Free(header);
        if (content) M->Free(content);
        if (status) M->Free(status);
    } else {
        if (header) free(header);
        if (content) free(content);
        if (status) free(status);
    }

    if (Header) delete Header;
    Header = NULL;

    if (Content) delete Content;
    Content = NULL;

    code = 0;
    content_length = 0;

    header = NULL;
    content = NULL;
    status = NULL;

    cUrlClear();
}

void	HTTP::answerClear(void)
{
    if (Header) delete Header;
    Header = NULL;

    if (Content) delete Content;
    Content = NULL;

    cUrlClear();
}

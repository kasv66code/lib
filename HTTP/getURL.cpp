/* getURL.cpp									2018-05-19

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "../http.hpp"
#include "../exceptions.hpp"
#include "../conv_str.hpp"
#include "../conv_numb.hpp"
#include "../conv_time.hpp"
#include <curl/curl.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <time.h>

#define UserAgent_Default	"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36"

#ifdef WIN32
    #define	strncasecmp	strncmp
    #define	strdup		_strdup
#endif

// ------------------------------------------------------------------------------------------------
void	HTTP::cUrlClear(void)
{
    if (cUrl) {
	// закрываем дескриптор curl

	CURL *curl_handle = (CURL *)cUrl;
	curl_easy_cleanup(curl_handle);
	cUrl = NULL;
    }
}


// ------------------------------------------------------------------------------------------------
size_t	HTTP::AppendHeader(char* ptr, size_t size, size_t nmemb)
{
    nodeList q = Header->AddLast();
    q->data = (M) ? M->Calloc(size * nmemb + 1) : calloc(1, size * nmemb + 1);
    q->numb = size * nmemb;
    memcpy(q->data, ptr, q->numb);

    if (!strncasecmp(ptr, "HTTP", 4)) {
	char *s = strchr(ptr + 4, ' ');
	if (s) {
	    s++;
	    code = convStrTo_i32(s);
	    s = strchr(s, ' ');
	    if (s) {
		s++;
		if (status) {
		    if (M) M->Free(status); else free(status);
		}

		status = (M) ? M->StrDup(s) : strdup(s);

		convStr_clearSpace(status);
	    }
	}
    }

    return size * nmemb;
}

static size_t	_HTTP_WriteHeader(char *ptr, size_t size, size_t nmemb, void *data)
{
    HTTP *reply = (HTTP *)data;
    return reply->AppendHeader(ptr, size, nmemb);
}



size_t	HTTP::AppendContent(char* ptr, size_t size, size_t nmemb)
{
    nodeList q = Content->AddLast();
    q->data = (M) ? M->Calloc(size * nmemb + 1) : calloc(1, size * nmemb + 1);
    q->numb = size * nmemb;
    memcpy(q->data, ptr, q->numb);

    return size * nmemb;
}

static size_t	_HTTP_WriteContent(char *ptr, size_t size, size_t nmemb, void *data)
{
    HTTP *reply = (HTTP *)data;
    return reply->AppendContent(ptr, size, nmemb);
}


#define CURL_EASY_SETOPT(n,v)    if (curl_easy_setopt(curl_handle,n,v)) {__logDbg("%d=%s", n, v); Err=1;}
#define CURL_easy_setopt(n,v)    curl_easy_setopt(curl_handle,n,v)

static void	__listFreeData(void* m, nodeList v)
{
    if (v && v->data) {
	if (m) {
	    mem *M = (mem *)m;
	    M->Free(v->data);
	} else {
	    free(v->data);
	}
    }
}

// ------------------------------------------------------------------------------------------------
bool	HTTP::getURL(const char *url, ...)
{
    Header = new(M) list(M, NULL, __listFreeData);
    Content = new(M) list(M, NULL, __listFreeData);

    bool R = 0;
    bool Err = 0;
    bool flagUserAgent = 0;
    bool flagReferer = 0;
    bool flagTimeout = 0;

    CURL *curl_handle;
    curl_handle = curl_easy_init();
    if (!curl_handle) __exNull();
    HTTP::cUrl = curl_handle;

    // задаем  url адрес
    CURL_EASY_SETOPT(CURLOPT_URL, url);

    // включаем перенаправление и ограничиваем их число
    CURL_easy_setopt(CURLOPT_FOLLOWLOCATION, 1);
    CURL_easy_setopt(CURLOPT_MAXREDIRS, 3);

    // отключить проверку сертификата
    CURL_easy_setopt(CURLOPT_SSL_VERIFYPEER, 0);
    CURL_easy_setopt(CURLOPT_SSL_VERIFYHOST, 0);

    // сохраняем html код cтраницы
    CURL_easy_setopt(CURLOPT_WRITEFUNCTION, _HTTP_WriteContent);
    CURL_easy_setopt(CURLOPT_WRITEDATA, this);

    // сохраняем загловок ответа сервера
    CURL_easy_setopt(CURLOPT_HEADERFUNCTION, _HTTP_WriteHeader);
    CURL_easy_setopt(CURLOPT_WRITEHEADER, this);

    struct curl_slist *hs = NULL;

    char *option;
    va_list ap;
    va_start(ap, url);
    while ((option = va_arg(ap, char*))) {
	if (strcmp(option, "UserAgent") == 0) {
	    char *value = va_arg(ap, char*);
	    if (value && *value) {
		CURL_EASY_SETOPT(CURLOPT_USERAGENT, value);
		flagUserAgent = 1;
	    }
	} else if (strcmp(option, "Referer") == 0) {
	    char *value = va_arg(ap, char*);
	    if (value && *value) {
		CURL_EASY_SETOPT(CURLOPT_REFERER, value);
		flagReferer = 1;
	    }
	} else if (strcmp(option, "Cookie") == 0) {
	    char *value = va_arg(ap, char*);
	    if (value && *value) {
		CURL_EASY_SETOPT(CURLOPT_COOKIE, value);
	    }
	} else if (strcmp(option, "Post") == 0) {
	    char *value = va_arg(ap, char*);
	    if (value && *value) {
		CURL_easy_setopt(CURLOPT_POST, 1);
		CURL_EASY_SETOPT(CURLOPT_POSTFIELDS, value);
		CURL_easy_setopt(CURLOPT_POSTFIELDSIZE, strlen(value));
	    }
	} else if (strcmp(option, "Timeout") == 0) {
	    CURL_easy_setopt(CURLOPT_TIMEOUT, va_arg(ap, int));
	    flagTimeout = 1;
	} else if (strcmp(option, "Auth") == 0) {
	    char *value = va_arg(ap, char*);
	    if (value && *value) {
		CURL_easy_setopt(CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		CURL_EASY_SETOPT(CURLOPT_USERPWD, value);
	    }
	} else if (strcmp(option, "IfModified") == 0) {
	    char So[50];
	    char *sx = So;
	    sx += sprintf(sx, "If-Modified-Since: ");
	    convToStr_timeGMT(sx, va_arg(ap, time_t));
	    hs = curl_slist_append(hs, So);
	} else if (strcmp(option, "Proxy") == 0) {
	    char *value = va_arg(ap, char*);
	    if (value && *value) {
		CURL_EASY_SETOPT(CURLOPT_PROXY, value);
	    }
	} else if (strcmp(option, "Bearer") == 0) {
	    char *value = va_arg(ap, char*);

	    if (value && *value) {
		CURL_EASY_SETOPT( CURLOPT_XOAUTH2_BEARER, value);
	    }
	} else if (strcmp(option, "Header") == 0) {
	    char *value = va_arg(ap, char*);
	    if (value && *value) {
		hs = curl_slist_append(hs, value);
	    }
	} 
	else if (strcmp(option, "Header2") == 0) {
	    char *value = va_arg(ap, char*);
	    char *value2 = va_arg(ap, char*);
	    if (value && *value && value2 && *value2) {
		char* V = NULL;
		if (M) V = (char*)M->Alloc(strlen(value) + strlen(value2) + 1);
		else V = (char*)malloc(strlen(value) + strlen(value2) + 1);
		if (!V) __exNull();

		sprintf(V, "%s%s", value, value2);
		hs = curl_slist_append(hs, V);

		if (M) M->Free(V); else free(V);
	    }
	} else {
	    __exValue();
	}

	if (hs) CURL_EASY_SETOPT(CURLOPT_HTTPHEADER, hs);
    }
    va_end(ap);

    if (!Err) {
	if (!flagUserAgent)	CURL_EASY_SETOPT(CURLOPT_USERAGENT, UserAgent_Default);
	if (!flagReferer)	CURL_easy_setopt(CURLOPT_AUTOREFERER, 1);
	if (!flagTimeout)	CURL_easy_setopt(CURLOPT_TIMEOUT, 30);

	// выполняем запрос
	CURLcode res = curl_easy_perform(curl_handle);
	if (res == CURLE_OK) {
	    // занести результат
	    if (answerLoad()) {
		R = 1;
	    }
	} else {
	    __logDbg("curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
	}
    }

    return R;
}

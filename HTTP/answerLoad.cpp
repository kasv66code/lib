/* answerLoad.cpp								2018-05-19

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "../http.hpp"
#include "../exceptions.hpp"
#include <stdlib.h>
#include <string.h>


char	*HTTP::ListToStr(list *L, int *Size)
{
    if (!L || L->count() <= 0) return NULL;

    int len = 0;
    nodeList q;
    for (q = L->getFirst(); q; q = q->next) {
	len += q->numb;
    }
    if (!len) return NULL;

    char* So = NULL;
    if (M)
        So = (char*)M->Calloc(len + 1);
    else
        So = (char*)calloc(1, len + 1);
    if (!So) __exNull();

    char *sx = So;
    for (q = L->getFirst(); q; q = q->next) {
	if (q->data) {
	    memcpy(sx, q->data, q->numb);
	    sx += q->numb;
	}
    }

    if (Size) *Size = len;

    return So;
}


// ------------------------------------------------------------------------------------------------
bool	HTTP::answerLoad(void)
{
    header = ListToStr(Header);
    content = ListToStr(Content, &content_length);

    if (Header) delete Header;
    Header = NULL;

    if (Content) delete Content;
    Content = NULL;

    return (!header || !content) ? 0 : 1;
}

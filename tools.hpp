/*  tools.hpp								                2012-2020

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#ifndef	_tools_hpp
#define	_tools_hpp

#include <time.h>


unsigned	_hash(const char *s);
unsigned	_hash_n(const char *s, int n);

unsigned	_hash(const wchar_t *s);
unsigned	_hash_n(const wchar_t *s, int n);


int	 getPID(void);

bool	runLockOpen(const char* dir_name, const char* prefix_file_name, int count=0, int* lockIndex=NULL);
int	runLockPID(const char* dir_name, const char* prefix_file_name, int lockIndex=0);
void	runLockClose(void);

void    	runTimeStart(time_t& t, int& us);
unsigned	runTimeStop(time_t t, int us);

bool	isUrl(const char *url);
bool	isEmail(const char *email);
bool	isPhone(const char *phone);

bool	isUrl(const wchar_t *url);
bool	isEmail(const wchar_t *email);
bool	isPhone(const wchar_t *phone);

bool	is_today(time_t t, time_t* tc);

int Base64decode_len(const char *bufcoded);
int Base64decode(char *bufplain, const char *bufcoded);
int Base64encode_len(int len);
int Base64encode(char *encoded, const char *string, int len);

char	*encodeMailHeader_base64(char* newstring, const char* string, const char* charset);

int	makeRandom_int(int mod);
void	makeRandom_mas64(char *s, int len);
void	makeRandom_mas32(char *s, int len);
void	makeRandom_mas16(char *s, int len);

int	emailOutput(const char *str); 

char	*MD5(const char *d, int n, char *md);

char	*nextFieldChar(char **s, const char *x);

#define nextField_line(s)	nextFieldChar(s, "\n\r")
#define nextField_tab(s)	nextFieldChar(s, "\t\n\r")
#define nextField_comma(s)	nextFieldChar(s, ",\t\n\r")
#define nextField_semicolon(s)	nextFieldChar(s, ";\t\n\r")

int	newIndex(const char *fn, bool (*fcheck)(int id), int firstNumber);
int	getIndex(const char *fn);
int	setIndex(const char *fn, int idx);


bool	    isProcessWork(int pid, const char* Name);

bool	    getProcessStatus(int pid, char* Name, char* State);
bool	    getProcessUsedMemorySize(int pid, int* VmRSS_kB, int* VmHWM_kB=NULL);

bool	    getThisProcessUsedMemorySize(int* VmRSS_kB, int* VmHWM_kB=NULL);
const char* getThisProcessUsername(void);


#endif

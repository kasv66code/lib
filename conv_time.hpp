/* conv_time.hpp									2012-2020

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/



#ifndef	_conv_time_hpp
#define	_conv_time_hpp


#include "types.hpp"
#include <time.h>


// Преобразования времени
int	convToStr_timeGMT(char* buff, time_t t);
time_t	convStrTo_timeGMT(const char* str);

int	convToStr_timeNumb(char *buff, time_t t);	// преобразует время в числовую строку
time_t	convStrTo_timeNumb(const char *str);

int	convTimeToYear(time_t t);	// возвращает год
int	convTimeToMonth(time_t t);	// возвращает месяц
int	convTimeToDay(time_t t);	// возвращает день месяца
int	convTimeToHour(time_t t);	// возвращает час
int	convTimeToMin(time_t t);	// возвращает минуты
int	convTimeToSec(time_t t);	// возвращает секунды
int	convTimeToDayOfWeek(time_t t);	// возвращает день недели

void	convTimeToNumb(time_t t, int &year, int &month, int &day, int &hour, int &minute, int &second);
void	convTimeToDate(time_t t, int &year, int &month, int &day);
void	convTimeToTime(time_t t, int &hour, int &minute, int &second);

time_t	convStrTo_timeCompile(const char *Sdate, const char *Stime);

time_t	convTime_make(int year, int month, int day, int hour, int minute, int second);
time_t	convTime_makeGMT(int year, int month, int day, int hour, int minute, int second);

extern const char **masName_month_En;			// January, ..
extern const char **masName_month_EnShort;		// Jan, ..
extern const char **masName_weekDay_EnShort;		// Sun, ..

extern const char **masName_month_RuUTF;		// январь, ..
extern const char **masName_month_RuUTFShort;		// янв, ..
extern const char **masName_month_RuUTFGenitive;	// января, ..

int	convToStr_timeFormat(char *buff, time_t t, const char *strFormat=NULL, const char **month=NULL);	// формирует строковое представление времени
// формат строки:
//	%y - полный год, %yy - две последние цифры года
//	%m - месяц [1..12], %mm - две цифры месяца [01..12], %ms - подставить наименование месяца из массива строк month[]
//	%d - день месяца [1..31], %dd - две цифры дня месяца [01..31]
//	%h - час [0..23], %hh - две цифры часа [00..23]
//	%n - минута [0..59], %nn - две цифры минут [00..59]
//	%s - секунда [0..59], %ss - две цифры секунд [00..59]
//	%% - подставляет символ '%'

#define	StringTimeFormat_Default	"%dd.%mm.%y %hh:%nn:%ss"



#endif

/*  cache.hpp										2012-2020

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#ifndef	_cache_hpp
#define	_cache_hpp


// --- класс обслуживания cahce-файлов ------------------------------------------------------------
class Cache {
    public:
	Cache(const char *_filename, int _recordSizeMax);
	Cache(const char *_dirname, const char *_filename, int _recordSizeMax);
	~Cache(void);

	bool	Check(void);
	void	Delete(void);
	void	Lock(void);
	void	UnLock(void);

	bool	loopRead(bool (*_rec_read_one)(char *s));
	bool	loopUpdate(bool (*_rec_update_one)(char *s, FILE *f), void (*_rec_append_all)(FILE *f));
	bool	loopCreate(void (*_rec_create_all)(FILE *f));

    private:
	FILE	*fr, *fw;
	char	*filename;
	int	recordSizeMax;
	char	*buff, *buff2;
	int	fd_lock;
	bool	group_lock;

	void	CacheInit(const char *_filename, int _recordSizeMax);

	bool	CheckEmpty(void);

	bool	lockOpen(bool mode_write);
	void	lockClose(void);

	bool	OpenCreate(void);
	bool	OpenRead(void);
	bool	OpenUpdate(void);

	void	Close(void);
};
// ------------------------------------------------------------------------------------------------

#define CACHE_RECORD_SIZE_MAX_DEFAULT	64*1024



#endif

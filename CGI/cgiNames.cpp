/*  getMethodName.cpp							23.10.2013

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_cgi.hpp"


// ------------------------------------------------------------------------------------------------
const char* cgiMethodName(cgiMethod method)
{
    switch (method) {
	case cgiMethod::cgiMethod_GET:
	    return cgi_Method_GET;
	case cgiMethod::cgiMethod_HEAD:
	    return cgi_Method_HEAD;
	case cgiMethod::cgiMethod_POST:
	    return cgi_Method_POST;
	case cgiMethod::cgiMethod_POST_MULTIPART:
	    return cgi_Method_MULTIPART;
    }
    return "";
}


// ------------------------------------------------------------------------------------------------
const char* cgiCharSetName(cgiCharSet charSet)
{
    switch (charSet) {
	case cgiCharSet::cgiCharSet_UTF8:
	    return _CGI_CHARSET_UTF;
	    break;
	case cgiCharSet::cgiCharSet_Windows1251:
	    return _CGI_CHARSET_WIN;
	    break;
    }
    return "";
}


// ------------------------------------------------------------------------------------------------
const char* cgiContentTypeName(cgiContentType contentType, cgiCharSet charSet)
{
    switch (contentType) {
	case cgiContentType::cgiContentType_gif:
	   return _CGI_CONTENT__gif;
	case cgiContentType::cgiContentType_jpg:
	    return _CGI_CONTENT__jpg;
	case cgiContentType::cgiContentType_png:
	    return _CGI_CONTENT__png;
	case cgiContentType::cgiContentType_json:
	    return _CGI_CONTENT__json;
	case cgiContentType::cgiContentType_javascript:
	    switch (charSet) {
		case cgiCharSet::cgiCharSet_Windows1251:
		    return _CGI_CONTENT__javascript "; charset=" _CGI_CHARSET_WIN;
		default:
		case cgiCharSet::cgiCharSet_UTF8:
		    return _CGI_CONTENT__javascript "; charset=" _CGI_CHARSET_UTF;
	    }
	    break;
	case cgiContentType::cgiContentType_plain:
	    switch (charSet) {
		case cgiCharSet::cgiCharSet_Windows1251:
		    return _CGI_CONTENT__plain "; charset=" _CGI_CHARSET_WIN;
		default:
		case cgiCharSet::cgiCharSet_UTF8:
		    return _CGI_CONTENT__plain "; charset=" _CGI_CHARSET_UTF;
	    }
	    break;
	case cgiContentType::cgiContentType_xml:
	    switch (charSet) {
		case cgiCharSet::cgiCharSet_Windows1251:
		    return _CGI_CONTENT__xml "; charset=" _CGI_CHARSET_WIN;
		default:
		case cgiCharSet::cgiCharSet_UTF8:
		    return _CGI_CONTENT__xml "; charset=" _CGI_CHARSET_UTF;
	    }
	    break;
	default:
	case cgiContentType::cgiContentType_html:
	    switch (charSet) {
		case cgiCharSet::cgiCharSet_Windows1251:
		    return _CGI_CONTENT__html "; charset=" _CGI_CHARSET_WIN;
		default:
		case cgiCharSet::cgiCharSet_UTF8:
		    return _CGI_CONTENT__html "; charset=" _CGI_CHARSET_UTF;
	    }
	    break;
    }

    return "";
}


// ------------------------------------------------------------------------------------------------
const char* cgiStatusName(cgiStatus status)
{
    switch (status) {
	case cgiStatus::cgiStatus_200:
	    return cgi_Status_200;
	case cgiStatus::cgiStatus_303:
	    return cgi_Status_303;
	case cgiStatus::cgiStatus_304:
	    return cgi_Status_304;
	case cgiStatus::cgiStatus_400:
	    return cgi_Status_400;
	case cgiStatus::cgiStatus_401:
	    return cgi_Status_401;
	case cgiStatus::cgiStatus_403:
	    return cgi_Status_403;
	case cgiStatus::cgiStatus_404:
	    return cgi_Status_404;
	case cgiStatus::cgiStatus_500:
	    return cgi_Status_500;
	case cgiStatus::cgiStatus_501:
	    return cgi_Status_501;
	case cgiStatus::cgiStatus_503:
	    return cgi_Status_503;
    }
    return "";
}


// ------------------------------------------------------------------------------------------------
int	    cgiStatusCode(cgiStatus status)
{
    switch (status) {
	default:
	case cgiStatus::cgiStatus_200:
	    return 200;
	case cgiStatus::cgiStatus_303:
	    return 303;
	case cgiStatus::cgiStatus_304:
	    return 304;
	case cgiStatus::cgiStatus_400:
	    return 400;
	case cgiStatus::cgiStatus_401:
	    return 401;
	case cgiStatus::cgiStatus_403:
	    return 403;
	case cgiStatus::cgiStatus_404:
	    return 404;
	case cgiStatus::cgiStatus_500:
	    return 500;
	case cgiStatus::cgiStatus_501:
	    return 501;
	case cgiStatus::cgiStatus_503:
	    return 503;
    }
    return 0;
}

/*  PutRedirect.cpp							23.10.2013

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_cgi.hpp"


void	CGI::PutRedirect(void)
{
    CGI::bufferOpen(0);
    const char *url = CGI::RedirectUrl;

    if (!url || strlen(url)<5) __exValue();

    char *http = (char *)"";

    if (CGI::hasVersion(1,1)) {
	if (CGI::Status == cgiStatus::cgiStatus_301)
	    CGI::bufferOutput += sprintf(CGI::bufferOutput, "%s", cgiStatusName(cgiStatus::cgiStatus_301));
	else
	    CGI::bufferOutput += sprintf(CGI::bufferOutput, "%s", cgiStatusName(cgiStatus::cgiStatus_303));
    }

    CGI::putCookie();

    if (strncmp(url,"http://",7) && strncmp(url,"https://",8) && strncmp(url,"ftp://",6))
	http = (char *)"http://";

    CGI::bufferOutput += sprintf(CGI::bufferOutput, cgi_Location "%s%s\n\n\n", http, url);

    CGI::bufferOutput += sprintf(CGI::bufferOutput, cgi_Cache_Control cgi_Pragma cgi_URI "%s%s\n\n", http, url);

    CGI::bufferFlush();
}

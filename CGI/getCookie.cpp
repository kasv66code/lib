/*  getCookie.cpp								23.10.2013

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_cgi.hpp"


// ------------------------------------------------------------------------------------------------
const char	*CGI::getCookieIn(const char *name)
{
    nodeStr q = CGI::treeCookieIn->Find(name);
    if (q)
	return (const char *)q->data;

    return NULL;
}

const char	*CGI::setCookieIn(const char* name, const char* value, bool replace)
{
    nodeStr q = CGI::treeCookieIn->Insert(name);
    if (q) {
        if (q->data) {
            if (!replace) return (const char*)q->data;
        }
        q->data = ((M)?M->StrDup(value):strdup(value));
        return (const char*)q->data;
    }

    return NULL;
}


// ------------------------------------------------------------------------------------------------
const char	*CGI::getCookieOut(const char *name)
{
    nodeStr q = CGI::treeCookieOut->Find(name);
    if (q && q->data) {
	_cookieDataSet* v = (_cookieDataSet*)q->data;
	return v->Value;
    }

    return NULL;
}


const char* CGI::setCookieOut(const char* name, const char* value, int timeLife, bool allSubDomain)
{
    if (!name || !*name) __exNull();
    if (!value || !*value) __exNull();

    nodeStr q = CGI::treeCookieOut->Insert(name);
    if (q) {
        _cookieDataSet* v = (_cookieDataSet*)((M)?M->Calloc(sizeof(_cookieDataSet)):calloc(1,sizeof(_cookieDataSet)));
	v->Name = (M)?M->StrDup(name):strdup(name);
	v->Value = (M) ? M->StrDup(value) : strdup(value);
	v->timeLife = timeLife;
	v->allSubDomain = allSubDomain;
	q->data = v;
	return v->Value;
    }
    return NULL;
}

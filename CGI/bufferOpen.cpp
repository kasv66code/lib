/*  bufferOpen.cpp								25.10.2013

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_cgi.hpp"


void	CGI::bufferOpen(int size)
{
    CGI::bufferSize = size + HEADER_SIZE;
    CGI::bufferAddress = (char*)((M) ? M->Alloc(bufferSize) : malloc(bufferSize));
    CGI::bufferOutput = CGI::bufferAddress;
}

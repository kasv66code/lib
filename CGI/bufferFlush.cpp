/*  bufferFlush.cpp								25.10.2013

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_cgi.hpp"
#include "_cgi_io.hpp"


void	CGI::bufferFlush(void)
{
    *(bufferOutput)=0;
    fwrite(bufferAddress, 1, bufferOutput - bufferAddress, FILE_output);
    fflush(FILE_output);
    bufferOutput = bufferAddress;
}

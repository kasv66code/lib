/* cgi.cpp							23.10.2013

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_cgi.hpp"


//-------------------------------------------------------------------------------------------------
void	*CGI::operator new(size_t size, mem *m)
{
    CGI* q = (CGI*)((m) ? m->Calloc(size) : calloc(1, size));
    q->M = m;
    return q;
}


void	CGI::operator delete(void* p)
{
    CGI* q = (CGI*)p;
    mem* m = q->M;
    if (m)
        m->Free(p);
    else
        free(p);
}

void    CGI::operator delete(void* p, mem* m)
{
    if (m)
        m->Free(p);
    else
        free(p);
}


//-------------------------------------------------------------------------------------------------
CGI::CGI(cgiCharSet charSet)
{
    oCharSet = charSet;

    treeQuery = new(M) treeStr(treeStr::modeKeyCmp::ascii, NULL, NULL);
    treeFile = new(M) treeStr(treeStr::modeKeyCmp::ascii, NULL, NULL);
    treeCookieIn = new(M) treeStr(treeStr::modeKeyCmp::ascii, NULL, NULL);
    treeCookieOut = new(M) treeStr(treeStr::modeKeyCmp::ascii, NULL, NULL);
}

CGI::~CGI(void)
{
}
enum class modeKeyCmp {
    none = 0,
    ascii,
    utf8
};

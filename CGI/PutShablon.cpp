/*  PutShablon.c							2013-10-29

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_cgi.hpp"


using namespace std;

static mutex	lockCGI;
#define LockCGI()	lockCGI.lock()
#define unLockCGI()	lockCGI.unlock()  


static CGI *_cgi;

static bool	_put_percent(shablon *_sh, const char *tag, const char *str)
{
    if (_cgi && _sh && tag && *tag) {
	const char *s = _cgi->getValue(tag);
	if (s && *s) {
	    if (str)
		_sh->putStr(s); 

	    return true;
	}
    }
    return false;
}


void	CGI::PutShablon(class shablon *sh)
{
    bool func_percent_update = 0;

    if (!sh) __exNull();

    LockCGI();
    if (!sh->func_percent) {
	_cgi = this;
	sh->func_percent = _put_percent;
        func_percent_update = 1;
    }
    sh->run();

    int dataLen = sh->buffLength();
    CGI::bufferOpen( dataLen );
    CGI::_setHeader(dataLen);

    if (eMethod != cgiMethod::cgiMethod_HEAD && dataLen>0) {
	sh->buffCopy(CGI::bufferOutput);
	CGI::bufferOutput += dataLen;
    }
    CGI::bufferFlush();

    if (func_percent_update) {
	_cgi = NULL;
	sh->func_percent = NULL;
    }
    unLockCGI();
}

/*  _CGI_PutErrorMsg.cpp						2013-11-02

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_cgi.hpp"
#include "_cgi_io.hpp"


// ------------------------------------------------------------------------------------------------
void	cgiErrorMsg(const char* Status, const char* txt)
{
    fprintf(FILE_output, "%s", "Content-Language: " HTTP11_DEFAULTLANG "\n");
    fprintf(FILE_output, "%s", Status);
    fprintf(FILE_output, "%s", cgi_Cache_Control cgi_Pragma);
    fprintf(FILE_output, "%s", "Vary: Accept-Language\nContent-Type: " _CGI_CONTENT__html "\n\n");
    fprintf(FILE_output, "<html><body><b>%s</b></body></html>\n", txt);
    fflush(FILE_output);
}


// ------------------------------------------------------------------------------------------------
void	cgiErrorMsg_400(void)
{
    cgiErrorMsg(cgi_Status_400, "400. Bad Request");
}


// ------------------------------------------------------------------------------------------------
void	cgiErrorMsg_404(void)
{
    cgiErrorMsg(cgi_Status_404, "404. Page not found");
}


// ------------------------------------------------------------------------------------------------
void	cgiErrorMsg_500(void)
{
    cgiErrorMsg(cgi_Status_500, "500. Internal Server Error");
}


// ------------------------------------------------------------------------------------------------
void	cgiErrorMsg_503(void)
{
    cgiErrorMsg(cgi_Status_503 "Retry-After: 30\n", "503. Service Unavailable");
}


// ------------------------------------------------------------------------------------------------
extern char** environ;

void	cgiEnvironToLog(void)
{
    for (int i = 0; environ[i] != NULL; i++) {
        __logDbg("Environ %d> %s", i+1, environ[i]);
    }
}

/*  getValue.cpp								23.10.2013

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_cgi.hpp"


// ------------------------------------------------------------------------------------------------
const char	*CGI::getValue(const char *name)
{
    nodeStr q = CGI::treeQuery->Find(name);
    if (q)
	return (const char *)q->data;

    return NULL;
}


// ------------------------------------------------------------------------------------------------
const char* CGI::getValuePost(const char* name)
{
    nodeStr q = CGI::treeQuery->Find(name);
    if (q && q->numb>0)
	return (const char *)q->data;

    return NULL;
}


// ------------------------------------------------------------------------------------------------
const char* CGI::setValue(const char* name, const char* value, bool post, bool replace)
{
    nodeStr q = CGI::treeQuery->Insert(name);
    if (q) {
        if (q->data) {
            if (!replace) return (const char*)q->data;
        }
        q->data = ((M) ? M->StrDup(value) : strdup(value));
        if (post) q->numb = 1;

        return (const char*)q->data;
    }
    return NULL;
}


// ------------------------------------------------------------------------------------------------
nodeStr CGI::getValueNode(const char *name)
{
    return treeQuery->Find(name);
}

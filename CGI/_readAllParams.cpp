/*  _readAllParams.cpp							24.10.2013

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_cgi.hpp"


static void	replaceAngleBrackets(char *s)
{
    if (!s || !*s) return;
    for (; *s; s++) {
	if (*s == '<' || *s == '>')
	    *s = ' ';
    }
}


void	CGI::_appendValue(char *name, char *value, class treeStr *tree, bool post)
{
    if (!name)
	return;

    convStr_urlDecode(name, name);
    name = convStr_clearSpace(name);

    if (!*name)
	return;

    if (!value) {
	value = (char *)"";
    } else if (*value) {
	convStr_urlDecode(value, value);
	value = convStr_clearSpace(value);
    }

    if (!CGI::enaAngleBrackets) {
	replaceAngleBrackets(name);
	replaceAngleBrackets(value);
    }

    nodeStr q = tree->Insert(name);
    if (!q)
	return;

    if (q->data)
	q = tree->AppendDataInListLast(q);

    q->data = value;
    q->numb = (int)post;
}


void	CGI::_readAllParams(char *q, int slen, const char *c, class treeStr *tree, bool post)
{
    while (q && *q) {
	if (*q == '?' || *q == '&' || *q == '&') {
	    q++;
	    continue;
	}

	char *name = q;
	char *value;

	if ((q=strpbrk(name, c))!=NULL) {
	    *q = 0;
	    q++;
	}

	value = strchr(name, '=');
	if (value) {
	    *value = 0;
	    value++;
	}

	CGI::_appendValue(name, value, tree, post);
    }
}


void	CGI::ReadAllParams(const char* s, bool post)
{
    if (!s || !*s) return;

    char* So = M->StrDup(s);
    _readAllParams(So, strlen(So), "&;", treeQuery, post);
}

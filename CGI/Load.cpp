/* Load.cpp										2020-04-28

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/



#include "_cgi.hpp"
#include "_cgi_io.hpp"


// ------------------------------------------------------------------------------------------------
bool	CGI::Load(bool enaPost, int PostDataMaxLen, bool enaFiles, bool EnaAngleBrackets)
{
    time(&currTime);
    enaAngleBrackets = EnaAngleBrackets;

    eMethod = getMethod();
    if (eMethod == cgiMethod::cgiMethod_Unknow || ((eMethod == cgiMethod::cgiMethod_POST || eMethod == cgiMethod::cgiMethod_POST_MULTIPART) && !enaPost)) {
	__logDbg("CGI::Load> eMethod=%d; enaPost=%d", (int)eMethod, (int)enaPost);
	return 0;
    }
    if (eMethod == cgiMethod::cgiMethod_POST || eMethod == cgiMethod::cgiMethod_POST_MULTIPART) {
	PostLength = convStrTo_i32(getenv(ENV__CONTENT_LENGTH));

	if (PostLength > 0) {
	    if (PostLength > PostDataMaxLen) {
		__logDbg("CGI::Load> PostLength > PostDataMaxLen (%d > %d)", PostLength, PostDataMaxLen);
		return 0;
	    }

	    char* s = (char*)((M) ? M->Calloc(PostLength + 1) : calloc(1, PostLength + 1));
	    if (!s) __exNull();

	    int freadSize = fread((void*)s, 1, PostLength, FILE_input);
	    if (freadSize == PostLength) {

		if (eMethod == cgiMethod::cgiMethod_POST_MULTIPART) {
		    if (!ContentType || !*ContentType) {
			__logDbg("CGI::Load> ContentType='%s'", ContentType);
			return 0;
		    }

		    const char* boundary = strstr(ContentType, "boundary=");
		    if (boundary && boundary[9]) {
			_readAllParamsMultipart(s, PostLength, boundary + 9);
		    } else {
			__logDbg("CGI::Load> boundary='%s'", boundary);
			return 0;
		    }
		} else {
		    if (checkPostParam(s)) {
			PostString = (M) ? M->StrDup(s) : strdup(s);
			CGI::_readAllParams(s, PostLength, "&;", treeQuery, 1);
		    } else {
			PostString = s;
		    }
		}
	    } else {
		__logDbg("CGI::Load> fread error! freadSize=%d != PostLength=%d", freadSize, PostLength);
		return 0;
	    }
	}
    }

    QueryString = getenv(ENV__QUERY_STRING);
    if (QueryString && *QueryString) {
	char* s = (M) ? M->StrDup(QueryString) : strdup(QueryString);
	_readAllParams( s, strlen(s), "&;", treeQuery, 0);
    }

    CookieString = getenv(ENV__HTTP_COOKIE);
    if (CookieString && *CookieString) {
	char* s = (M) ? M->StrDup(CookieString) : strdup(CookieString);
	_readAllParams( s, strlen(s), ";", treeCookieIn, 0);
    }

    ScriptName = getenv(ENV__SCRIPT_NAME);
    UserAgent = getenv(ENV__HTTP_USER_AGENT);
    Language = getLanguage();
    Authorization = getAuthorization();

    ServerName = getServerName(www);
    Referer = getReferer();
    IP = getIPaddress();

    DomainTwoLevel = getDomainTwoLevel(ServerName);

    https = getHTTPS();
    port = getServerPort();

    if_modified_since = getModifiedSince();

    URL = makeHTTP();

    setvbuf(FILE_output, NULL, _IONBF, 16*kByte);
    return 1;
}


// ------------------------------------------------------------------------------------------------
const char*	CGI::getAuthorization(void)
{
    Authorization = getenv(ENV__Authorization);
    if (Authorization && *Authorization) {
	const char* Sc = strchr(Authorization, ' ');
	if (Sc)
	    Authorization = Sc + 1;
    }
    return Authorization;
}



cgiMethod	CGI::getMethod(void)
{
    ContentType = getenv(ENV__CONTENT_TYPE);
    Method = getenv(ENV__REQUEST_METHOD);
    if (Method && *Method) {
	if (!strcasecmp(Method, ENV__GET))
	    return cgiMethod::cgiMethod_GET;

	if (!strcasecmp(Method, ENV__HEAD))
	    return cgiMethod::cgiMethod_HEAD;

	if (!strcasecmp(Method, ENV__POST)) {
	    if (ContentType && *ContentType) {
		if (!strncasecmp(ContentType, ENV__APPLICATION, sizeof(ENV__APPLICATION)-1))
		    return cgiMethod::cgiMethod_POST;
		if (!strncasecmp(ContentType, ENV__MULTIPART, sizeof(ENV__MULTIPART)-1))
		    return cgiMethod::cgiMethod_POST_MULTIPART;
	    }
	    return cgiMethod::cgiMethod_POST;
	}
    }
//    __logDbg("CGI::getMethod> ContentType=%s; Method=%s", ContentType, Method);
    return cgiMethod::cgiMethod_GET;
//    return cgiMethod::cgiMethod_Unknow;
}


// ------------------------------------------------------------------------------------------------
const char* CGI::getServerName(bool &WWW)
{
    char* s = getenv(ENV__HTTP_HOST);
    if (!s)
	s = getenv(ENV__SERVER_NAME);

    if (s && *s && !strncasecmp(s, "www.", 4))
	WWW = true;

    return s;
}


// ------------------------------------------------------------------------------------------------
const char* CGI::getReferer(void)
{
    const char* s = getenv(ENV__HTTP_REFERER);
    if (s && *s && (!strncasecmp(s, "http://", 7) || !strncasecmp(s, "https://", 8) || !strncasecmp(s, "ftp://", 6)))
	return s;
    return NULL;
}


// ------------------------------------------------------------------------------------------------
const char* CGI::getIPaddress(void)
{
    const char *ip = getenv(ENV__HTTP_X_REAL_IP);
    if (!ip)
	ip = getenv(ENV__REMOTE_ADDR);
    return ip;
}


// ------------------------------------------------------------------------------------------------
const char* CGI::getLanguage(void)
{
    const char *L = getenv(ENV__HTTP_ACCEPT_LANGUAGE);
    if (!L)
	L = getenv(ENV__LANG);
    return L;
}


// ------------------------------------------------------------------------------------------------
bool	CGI::getHTTPS(void)
{
    const char *on = getenv(ENV__HTTPS_X);
    if (on && !strcasecmp(on,"on"))
	return true;
    on = getenv(ENV__HTTPS);
    if (on && !strcasecmp(on,"on"))
	return true;
    return false;
}


// ------------------------------------------------------------------------------------------------
int	CGI::getServerPort(void)
{
    return convStrTo_i32(getenv(ENV__SERVER_PORT));
}


// ------------------------------------------------------------------------------------------------
time_t	CGI::getModifiedSince(void)
{
    return convStrTo_timeGMT(getenv(ENV__HTTP_IF_MODIFIED_SINCE));
}


// ------------------------------------------------------------------------------------------------
const char	*CGI::makeHTTP(void)
{
    if (ServerName && *ServerName) {
	int len = strlen(ServerName)+30;
	if (ScriptName && *ScriptName) len += strlen(ScriptName);
	char* s = (char*)((M) ? M->Calloc(len) : calloc(1, len));
	if (s) {
	    char* sx = s;
	    sx += sprintf(sx, "http%s://%s%s", (https)?"s":"", (www)?"www.":"", ServerName);
	    if (port>0 && port!=80)
		sx += sprintf(sx, ":%d", port);
	    if (ScriptName) {
		if (*ScriptName != '/') sx += sprintf(sx, "/");
		sx += sprintf(sx, "%s", ScriptName);
	    } else {
		sx += sprintf(sx, "/");
	    }
	    return s;
	}
    }
    return NULL;
}


// ------------------------------------------------------------------------------------------------
static bool	isIPaddress(const char* s)
{
    if (strchr(s, ':')) return 1;

    int d[4];
    if (sscanf(s, "%d.%d.%d.%d", d, d + 1, d + 2, d + 3) == 4) return 1;

    return 0;
}

const char	*CGI::getDomainTwoLevel(const char *s)
{
    if (s && *s && !isIPaddress(s)) {
	const char *se, *sp;

	for(sp=NULL; (se=strchr(s, '.'))!=NULL; s=se+1)
	    sp = s;

	if (sp && s) {
	    bool r=false;
	    for(s++; *s; s++) {
		if (*s>='0' && *s<='9')
		    continue;
		r=true;
		break;
	    }
	    if (r)
		return sp;
	}
    }
    return NULL;
}


// ------------------------------------------------------------------------------------------------
bool	CGI::checkPostParam(const char* s)
{
    if (!s || !*s) return 0;

//__log("checkPostParam> 2: ContentType=%s", ContentType);
    if (ContentType && *ContentType) {
	if (!strncasecmp(ContentType, _CGI_CONTENT__gif, sizeof(_CGI_CONTENT__gif)-1)) return 0;
	if (!strncasecmp(ContentType, _CGI_CONTENT__jpg, sizeof(_CGI_CONTENT__jpg)-1)) return 0;
	if (!strncasecmp(ContentType, _CGI_CONTENT__png, sizeof(_CGI_CONTENT__png)-1)) return 0;
	if (!strncasecmp(ContentType, _CGI_CONTENT__javascript, sizeof(_CGI_CONTENT__javascript)-1)) return 0;
	if (!strncasecmp(ContentType, _CGI_CONTENT__json, sizeof(_CGI_CONTENT__json)-1)) return 0;
	if (!strncasecmp(ContentType, _CGI_CONTENT__plain, sizeof(_CGI_CONTENT__plain)-1)) return 0;
	if (!strncasecmp(ContentType, _CGI_CONTENT__html, sizeof(_CGI_CONTENT__html)-1)) return 0;
	if (!strncasecmp(ContentType, _CGI_CONTENT__xml, sizeof(_CGI_CONTENT__xml)-1)) return 0;
    }

    int n = 0;
    for (char c; (c=*s)!=0; s++, n++) {
	if (c == '=' && n > 0 && s[1]) {
	    return 1;
	}
	if (c >= '0' && c <= '9') continue;
	if (c >= 'a' && c <= 'z') continue;
	if (c >= 'A' && c <= 'Z') continue;
	if (c == '_') continue;
	if (c == '-') continue;
	if (c == '.') continue;
	break;
    }
    return 0;
}

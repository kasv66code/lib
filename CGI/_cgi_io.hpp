/*  _cgi_io.hpp						                                2020-04-29

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#ifdef mFCGI
    #include <fcgi_stdio.h> 
#else
    #include <stdio.h>
#endif 

#define FILE_input	stdin
#define FILE_output     stdout

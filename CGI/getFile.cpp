/*  getFile.cpp								23.10.2013

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_cgi.hpp"


const cgiFile	*CGI::getFile(const char *name)
{
    nodeStr q = CGI::treeFile->Find(name);
    if (q)
	return (const cgiFile *)q->data;

    return NULL;
}

/*  PutNotModified.cpp								23.10.2013

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_cgi.hpp"



void	CGI::PutNotModified(void)
{
    if (!CGI::LastModified) __exValue();

    CGI::bufferOpen(0);

    CGI::bufferOutput += sprintf(CGI::bufferOutput, "%sLast-Modified: ", cgi_Status_304);
    CGI::Status = cgiStatus::cgiStatus_304;

    CGI::_putGMT(CGI::LastModified);

    sprintf(CGI::bufferOutput, "\n\n");
    CGI::bufferOutput += strlen(CGI::bufferOutput);

    CGI::bufferFlush();
}

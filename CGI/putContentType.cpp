/*  putContentType.cpp						23.10.2013

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/



#include "_cgi.hpp"



void	CGI::putContentType(cgiContentType ContentType, cgiCharSet CharSet)
{
    const char *content_type;

    switch (ContentType) {
	case cgiContentType::cgiContentType_gif:
	    content_type = _CGI_CONTENT__gif;
	    break;
	case cgiContentType::cgiContentType_jpg:
	    content_type = _CGI_CONTENT__jpg;
	    break;
	case cgiContentType::cgiContentType_png:
	    content_type = _CGI_CONTENT__png;
	    break;
	case cgiContentType::cgiContentType_javascript:
	    content_type = _CGI_CONTENT__javascript;
	    break;
	case cgiContentType::cgiContentType_plain:
	    content_type = _CGI_CONTENT__plain;
	    break;
	case cgiContentType::cgiContentType_xml:
	    content_type = _CGI_CONTENT__xml;
	    break;
	case cgiContentType::cgiContentType_json:
	    content_type = _CGI_CONTENT__json;
	    break;
	default:
	case cgiContentType::cgiContentType_html:
	    content_type = _CGI_CONTENT__html;
	    break;
    }
    CGI::bufferOutput += sprintf(CGI::bufferOutput, "%s", content_type);

    switch (ContentType) {
	case cgiContentType::cgiContentType_javascript:
	case cgiContentType::cgiContentType_plain:
	case cgiContentType::cgiContentType_xml:
	default:
	case cgiContentType::cgiContentType_html:
	    {
		const char* char_set = cgiCharSetName(CharSet);
		if (char_set && *char_set)
		    CGI::bufferOutput += sprintf(CGI::bufferOutput, "; charset=%s", char_set);
	    }
	    break;
    }
}

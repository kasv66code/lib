/*  PutHeader.c								2013-10-29

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_cgi.hpp"


bool	CGI::PutHeader(unsigned dataLen)
{
    CGI::bufferOpen( 0 );
    CGI::_setHeader(dataLen);
    CGI::bufferFlush();

    return (eMethod == cgiMethod::cgiMethod_HEAD)?false:true;
}

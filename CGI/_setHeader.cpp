/*  _setHeader.cpp							2013-10-29

    Author: Kachkovsky Sergey V.
    kasv@list.ru

    cacheLife:
	-1 - кеш по-умолчанию
	 0 - не использовать кеш
	>0 - время в кеше в секундах
*/


#include "_cgi.hpp"


void	CGI::_setHeader(unsigned dataLen)
{
    CGI::bufferOutput += sprintf(CGI::bufferOutput, "%s", "Content-Language: " HTTP11_DEFAULTLANG "\n");

    if (CGI::Status == cgiStatus::cgiStatus_Unknow)
	CGI::Status = cgiStatus::cgiStatus_200;
    CGI::bufferOutput += sprintf(CGI::bufferOutput, "%s", cgiStatusName(CGI::Status));

    if (!CGI::cacheLife) {
	CGI::bufferOutput += sprintf(CGI::bufferOutput, "%s", cgi_Cache_Control cgi_Pragma);
    } else if (CGI::cacheLife>0) {
	CGI::bufferOutput += sprintf(CGI::bufferOutput, "Cache-Control: max-age=%d\n", CGI::cacheLife);

	CGI::bufferOutput += sprintf(CGI::bufferOutput, "Expires: ");

	CGI::_putGMT( currTime+CGI::cacheLife );
	CGI::bufferOutput += sprintf(CGI::bufferOutput, "\n");
    }

    if (CGI::LastModified>0) {
	CGI::bufferOutput += sprintf(CGI::bufferOutput, "Last-Modified: ");

	CGI::_putGMT( CGI::LastModified );
	CGI::bufferOutput += sprintf(CGI::bufferOutput, "\n");
    }

    if (dataLen>0) {
	CGI::bufferOutput += sprintf(CGI::bufferOutput, "Content-Length: %d\n", dataLen);
    }

    if (oContentType == cgiContentType::cgiContentType_Unknow)
	oContentType = cgiContentType::cgiContentType_html;

    CGI::bufferOutput += sprintf(CGI::bufferOutput, "Vary: Accept-Language\nContent-Type: ");

    putContentType(oContentType, oCharSet);
    CGI::bufferOutput += sprintf(CGI::bufferOutput, "\n");

    if (CGI::funcHeaderLinesAppend) {
	CGI::funcHeaderLinesAppend(CGI::bufferOutput);
	if (*CGI::bufferOutput)
	    CGI::bufferOutput += strlen(CGI::bufferOutput);
    }

    CGI::putCookie();
    CGI::bufferOutput += sprintf(CGI::bufferOutput, "\n");

    if (CGI::bufferOutput - CGI::bufferAddress > HEADER_SIZE) __exAlgorithm();
}

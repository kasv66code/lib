/*  PutFile.c								24.11.2014

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_cgi.hpp"
#include "_cgi_io.hpp"


bool	CGI::PutFile(const char *fileFullName, bool enaLastModified)
{
    int fd = -1;
    if (!_fileOpen(&fd, fileFullName, fileOpenMode_read)) {
	// ���� �� ������
	return false;
    }

    i64 dataLen;
    time_t mTime;
    FILE *f;
    if (!_fileGetStat(fd, &dataLen, &mTime) || (f = fdopen(fd, "r"))==NULL) {
	// ���-�� ����� �� ���
	_fileClose(fd);
	return false;
    }

    if (enaLastModified) CGI::LastModified = mTime;

    int size = 0;
    if (dataLen>0)
	size = (dataLen>BufferOutputMax)?BufferOutputMax/2:((int)dataLen);
    CGI::bufferOpen( size );

    CGI::_setHeader((int)dataLen);

    if (eMethod != cgiMethod::cgiMethod_HEAD && dataLen>0) {
	size = bufferSize - (bufferOutput - bufferAddress) - 3;

	if (size < dataLen) {
	    int len = fread(CGI::bufferOutput, 1, size, f);
	    if (len>0) {
	    	CGI::bufferOutput += len;
	    	dataLen -= len;
	    }
	} else {
	    int len = fread(CGI::bufferOutput, 1, (size_t)dataLen, f);
	    if (len>0) {
	    	CGI::bufferOutput += len;
	    	dataLen -= len;
	    }
	}
	CGI::bufferFlush();

	if (dataLen>0) {
	    for(; dataLen>0; ) {
		int len = (dataLen>size)?size:((int)dataLen);
		int L = fread(CGI::bufferOutput, 1, len, f);
		if (L>0) {
		    fwrite(CGI::bufferOutput, 1, L, FILE_output);
		    dataLen -= L;
		}
	    }
	    fflush(FILE_output);
	}
    } else
	CGI::bufferFlush();

    fclose(f);
    return true;
}

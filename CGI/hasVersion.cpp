/*  hasVersion.cpp							23.10.2013

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_cgi.hpp"
#include <ctype.h>


static void	__get_cgiversion(unsigned *major, unsigned *minor)
{
    const char *p=getenv(ENV__SERVER_PROTOCOL);

    *major=0;
    *minor=0;
    if (!p)	return;
    if ( toupper(*p++) != 'H' ||
	 toupper(*p++) != 'T' ||
	 toupper(*p++) != 'T' ||
	 toupper(*p++) != 'P' || *p++ != '/')
	 return;

    while (isdigit(*p))
	*major= *major * 10 + (*p++ - '0');
    if (*p++ == '.') {
	while (isdigit(*p))
	    *minor= *minor * 10 + (*p++ - '0');
    }
}

int	CGI::hasVersion(unsigned major, unsigned minor)
{
    unsigned vmajor, vminor;

    __get_cgiversion(&vmajor, &vminor);
    return (vmajor > major || (vmajor == major && vminor >= minor));
}

/*  _cgi_env.hpp									2020-04-28

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#define	ENV__QUERY_STRING		"QUERY_STRING"
#define	ENV__REQUEST_METHOD		"REQUEST_METHOD"
#define	ENV__GET			"GET"
#define	ENV__HEAD			"HEAD"
#define	ENV__POST			"POST"
#define	ENV__CONTENT_LENGTH		"CONTENT_LENGTH"
#define	ENV__CONTENT_TYPE		"CONTENT_TYPE"
#define	ENV__HTTP_COOKIE		"HTTP_COOKIE"
#define	ENV__APPLICATION		"application/x-www-form-urlencoded"
#define	ENV__MULTIPART			"multipart/form-data"
#define	ENV__REMOTE_ADDR		"REMOTE_ADDR"
#define	ENV__SERVER_PROTOCOL		"SERVER_PROTOCOL"
#define	ENV__SCRIPT_NAME		"SCRIPT_NAME"
#define	ENV__SERVER_PORT		"SERVER_PORT"
#define	ENV__SERVER_NAME		"SERVER_NAME"
#define	ENV__HTTP_HOST			"HTTP_HOST"
#define	ENV__HTTP_REFERER		"HTTP_REFERER"
#define	ENV__HTTP_ACCEPT		"HTTP_ACCEPT"
#define	ENV__HTTP_ACCEPT_LANGUAGE	"HTTP_ACCEPT_LANGUAGE"
#define	ENV__LANG			"LANG"
#define	ENV__HTTP_USER_AGENT		"HTTP_USER_AGENT"
#define	ENV__HTTP_X_REAL_IP		"HTTP_X_REAL_IP"
#define	ENV__HTTP_IF_MODIFIED_SINCE	"HTTP_IF_MODIFIED_SINCE"
#define	ENV__HTTPS			"HTTPS"
#define	ENV__HTTPS_X			"HTTP_X_HTTPS"
#define	ENV__Authorization		"HTTP_AUTHORIZATION"

/*  PutData.c								24.10.2013

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_cgi.hpp"
#include "_cgi_io.hpp"


void	CGI::PutData(unsigned dataLen, const char *dataStr)
{
    int size = 0;
    if (dataStr && dataLen>0)
	size = (dataLen>BufferOutputMax)?BufferOutputMax/2:dataLen;
    CGI::bufferOpen( size );

    CGI::_setHeader(dataLen);

    if (eMethod != cgiMethod::cgiMethod_HEAD && dataStr && dataLen>0) {
	size = bufferSize - (bufferOutput - bufferAddress) - 3;

	if ((unsigned)size < dataLen) {
	    memcpy(CGI::bufferOutput, dataStr, size);
	    CGI::bufferOutput += size;

	    dataStr += size;
	    dataLen -= size;
	} else {
	    memcpy(CGI::bufferOutput, dataStr, dataLen);
	    CGI::bufferOutput += dataLen;
	    dataLen = 0;
	}
	CGI::bufferFlush();

	if (dataLen) {
	    fwrite((void *)dataStr, 1, dataLen, FILE_output);
	    fflush(FILE_output);
	}
    } else
	CGI::bufferFlush();
}

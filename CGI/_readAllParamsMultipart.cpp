/*  _readAllParamsMultipart.c						24.10.2013
*/


#include "_cgi.hpp"


int	CGI::_readAllParamsMultipart(char *q, int length, const char *boundary)
{
    int bndLen;
    char *name = NULL;
    char *fileName = NULL;
    char *contentType = NULL;
    char *value = NULL;
    char *valueEnd = NULL;

    char *p = (char *)q;
    char *begin = p;
    char *bnd = (char *)((M)?M->Alloc(strlen(boundary)+5):malloc(strlen(boundary)+5));
    int ok = 0;
    int isFile = 0;
    if (!bnd)
	return 0;
    
    sprintf(bnd, "--%s", boundary);
    bndLen = strlen(bnd);

    if (strncmp(p, bnd, bndLen))
	return 0;
    if (p[bndLen] != '\r' || p[bndLen+1] != '\n')
	return 0;
    p = p + bndLen + 2;

    while (p && *p){
	ok = isFile = 0;
	if (!strncmp(p, "Content-Disposition: form-data; ", 32)){
	    p = p + 32;
	    if (!strncmp(p, "name=\"", 6)){
		name = p + 6;
		p = strchr(name, '"');
		if (p){
		    *p = 0;
		    p++;
		    if (!strncmp(p, "\r\n\r\n", 4)){
			ok = 1;
			p += 4;
		    }
		    else if (*p == ';' && !strncmp(p+2, "filename=\"", 10)){
			fileName = p + 12;
			p = strchr(fileName, '"');
			if (p && p[1] == '\r' && p[2] == '\n'){
			    *p = 0;
			    p += 3;

			    if (!strncmp(p, "Content-Type: ", 14)){
				contentType = p + 14;
				p = strchr(contentType, '\r');
				if (p && !strncmp(p, "\r\n\r\n", 4)){
				    *p = 0;
				    p += 4;
				    ok = 1;
				    isFile = 1;
				}
			    }
			}
		    }
		}
	    }
	}

	value = p;
	p = strstr(p, bnd);

	if (!p && isFile){
	    char *_p = value + strlen(value) + 1;
	    while (!p && (_p - begin < length)){
		p = strstr(_p, bnd);
		if (!p){
		    _p += strlen(_p) + 1;
		}
	    }
	}

	if (p) {
	    valueEnd = p-2;
	    *valueEnd = 0;
	    if (p[bndLen] == '-' && p[bndLen+1] == '-')
		p = NULL;
	    else
		p += bndLen + 2;

	    if (ok) {
		if (!isFile) {
		    CGI::_appendValue(name, value, CGI::treeQuery, 1);
		} else {
		    cgiFile *fl = (cgiFile *)((M)?M->Calloc(sizeof(cgiFile)):calloc(1,sizeof(cgiFile)));
		    if (fl) {
			fl->size = valueEnd-value;
			fl->fileName = fileName;
			fl->data     = value;
			fl->contentType = contentType;

			CGI::_appendFile(name, fl);
		    } else
			return 0;
		}
	    }
	}
    }
    return 1;
}

void	CGI::_appendFile(char *name, cgiFile *value)
{
    if (!name)
	return;

    convStr_urlDecode(name, name);
    name = convStr_clearSpace(name);

    if (!*name)
	return;

    nodeStr q = CGI::treeFile->Insert(name);
    if (!q)
	return;

    if (q->data)
	q = CGI::treeFile->AppendDataInListLast(q);

    q->data = value;
}

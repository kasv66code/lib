/*  _cgi.hpp								                2020-04-28

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#ifndef	__cgi_hpp
#define	__cgi_hpp


#include "../conv_numb.hpp"
#include "../conv_str.hpp"
#include "../conv_time.hpp"
#include "../file.hpp"
#include "../exceptions.hpp"
#include "../cgi.hpp"
#include <stdlib.h>
#include <string.h>
#include "_cgi_env.hpp"


#ifdef _WIN32
    int strcasecmp(const char *s1, const char *s2);
    int strncasecmp(const char *s1, const char *s2, size_t n);
    #define strdup  _strdup
    #define fdopen  _fdopen
#endif


#define BufferOutputMax		(500*kByte)
#define HEADER_SIZE		(32*kByte)

#define HTTP11_DEFAULTLANG	"ru"

#define	cgi_Status_200	        "Status: 200 OK\n"
#define	cgi_Status_301	        "Status: 301 Moved Permanently\n"
#define	cgi_Status_303	        "Status: 303 Moved\n"
#define	cgi_Status_304	        "Status: 304 Not Modified\n"
#define	cgi_Status_400	        "Status: 400 Bad Request\n"
#define	cgi_Status_401	        "Status: 401 Unauthorized\n"
#define	cgi_Status_403	        "Status: 403 Forbidden\n"
#define	cgi_Status_404	        "Status: 404 Not Found\n"
#define	cgi_Status_500	        "Status: 500 Internal Server Error\n"
#define	cgi_Status_501	        "Status: 501 Not Implemented\n"
#define	cgi_Status_503	        "Status: 503 Service Unavailable\n"

#define cgi_Method_GET		"GET"
#define cgi_Method_HEAD		"HEAD"
#define cgi_Method_POST		"POST"
#define cgi_Method_MULTIPART	"MULTIPART"

#define	_CGI_CONTENT__gif		"image/gif"
#define	_CGI_CONTENT__jpg		"image/jpg"
#define	_CGI_CONTENT__png		"image/png"
#define	_CGI_CONTENT__javascript	"application/javascript"
#define	_CGI_CONTENT__json		"application/json"
#define	_CGI_CONTENT__plain		"text/plain"
#define	_CGI_CONTENT__html		"text/html"
#define	_CGI_CONTENT__xml		"text/xml"

#define	_CGI_CHARSET_UTF	"utf-8"
#define	_CGI_CHARSET_WIN	"windows-1251"


#define	cgi_Cache_Control	"Cache-Control: no-store\n"
#define	cgi_Pragma		"Pragma: no-cache\n"

#define	cgi_Location		"Location: "
#define	cgi_URI			"URI: "


typedef struct {
    char	*Name;
    char	*Value;
    time_t	timeLife;
    bool	allSubDomain;
} _cookieDataSet;




#endif

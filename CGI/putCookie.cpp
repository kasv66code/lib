/*  putCookie.cpp								23.10.2013

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_cgi.hpp"



void	CGI::putCookie(void)
{
    class treeStr *t = CGI::treeCookieOut;
    int i=0, n=t->count();

    for(; i<n; i++) {
	nodeStr q = (*t)[i];
	if (q && q->data) {
	    CGI::_putCookie(q);
	}
    }
}


static const char *_month_mas[12]={
	"Jan",
	"Feb",
	"Mar",
	"Apr",
	"May",
	"Jun",
	"Jul",
	"Aug",
	"Sep",
	"Oct",
	"Nov",
	"Dec"
};

static const char *_wday_mas[8]={
	"Sun",
	"Mon",
	"Tue",
	"Wed",
	"Thu",
	"Fri",
	"Sat"
	"Sun",
};

void	CGI::_putGMT(time_t t)
{
    struct tm *tmc;
    int day,month,year,hours,minutes,seconds,wday;

    tmc = gmtime(&t);

    day	=tmc->tm_mday;
    month   =tmc->tm_mon;
    year    =tmc->tm_year+1900;
    hours   =tmc->tm_hour;
    minutes =tmc->tm_min;
    seconds =tmc->tm_sec;
    wday    =tmc->tm_wday;

    CGI::bufferOutput += sprintf(CGI::bufferOutput, "%s, %02d %s %d %02d:%02d:%02d GMT", _wday_mas[wday], day, _month_mas[month], year, hours, minutes, seconds);
}


void	CGI::_putCookie(nodeStr v)
{
    _cookieDataSet *q = (_cookieDataSet *)v->data;
    CGI::bufferOutput += sprintf(CGI::bufferOutput, "Set-Cookie: %s=%s;", q->Name, q->Value);
    if (q->timeLife) {
	CGI::bufferOutput += sprintf(CGI::bufferOutput, " expires=");
	CGI::_putGMT((q->timeLife>0)?currTime+q->timeLife:currTime-1);
	CGI::bufferOutput += sprintf(CGI::bufferOutput, ";");
    }
    if (q->allSubDomain) {
	if ((CGI::DomainTwoLevel && *CGI::DomainTwoLevel) || (CGI::ServerName && *CGI::ServerName))
	    CGI::bufferOutput += sprintf(CGI::bufferOutput, " domain=.%s;", (CGI::DomainTwoLevel) ? CGI::DomainTwoLevel : CGI::ServerName);
    }

    CGI::bufferOutput += sprintf(CGI::bufferOutput, " path=/;\n"); 
}

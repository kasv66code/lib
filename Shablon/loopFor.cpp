/* loopFor.c									2018-05-15

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_shablon.hpp"


int	shablon::loopFor(char *Sn, int recurse_level)
{
    int i, n=_shablon_MAX_LOOP_N;
    char *sw = strchr(Sn,' ');

    if (sw) {
	sw++;
	if (!*sw) {
	    shablon::putStr(_shablon_ExecuteError "error shablon syntactic FOR = ");
	    shablon::putStr(Sn, 20);
	    shablon::putStr("...\n");
	    return 0;
	}

	n=atoi(Sn);
	if (n<=0 || n>_shablon_MAX_LOOP_N) {
	    shablon::putStr(_shablon_ExecuteError "error shablon FOR ");
	    shablon::putStr(Sn);
	    shablon::putStr(" (count=");
	    shablon::putNumb(n);
	    shablon::putStr("), min=1, max=");
	    shablon::putNumb(_shablon_MAX_LOOP_N);
	    shablon::putStr(".\n");
	    return 0;
	}
	Sn = sw;
    }

    for(i=0; i<n && shablon::_tagExec(Sn, NULL, recurse_level); i++)
	shablon::_tagExec(Sn, "", recurse_level);

    return 1;
}

/* _shablon.hpp									        2020-04-25

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#ifndef __shablon_hpp
#define __shablon_hpp


#include "../conv_numb.hpp"
#include "../conv_str.hpp"
#include "../conv_time.hpp"
#include "../shablon.hpp"
#include "../exceptions.hpp"
#include <stdlib.h>
#include <string.h>


#ifdef _WIN32
    #define strdup  _strdup
#endif


#define	shablonFORMAT_JS		1
#define	shablonFORMAT_HTML		2
#define	shablonFORMAT_URL		4
#define	shablonFORMAT_NL_to_BR		8
#define	shablonFORMAT_HTML_TEXTAREA	16
#define	shablonFORMAT_JSON		32

#define	_shablon_MAX_INCLUDE_RECURSE_LEVEL	10
#define	_shablon_MAX_PROC_RECURSE_LEVEL		30
#define	_shablon_MAX_LOOP_N			100
#define	_shablon_MAX_LEN_TAG_NAME		127
#define	_shablon_MEM_BUF_SIZE			64*kByte

#define	_shablon_ExecuteError		"\nShablon execute error: "


typedef enum {
    shablonType_String=0,	// строка-часть шаблона
    shablonType_Global,		// глобальный тег
    shablonType_Local,		// локальный тег
    shablonType_Procent,	// тег вида %
    shablonType_At		// тег вида @
} shablonType;

typedef enum {
    shablonSpaceMode_Empty=0,	// не разделитель
    shablonSpaceMode_Space,	// пробел
    shablonSpaceMode_Tab,	// \t
    shablonSpaceMode_NL		// \n
} shablonSpaceMode;


// структура для описания локального фрагмента шаблона (тега вида [#* )
typedef struct _shablonFragment_t {
    shablonType	type;
    const char	*str;
    struct _shablonFragment_t *next;
} _shablonFragment;


// структура для описания прикрепленных к тегам функций
typedef struct {
    __sh_proc_tag_t	_procedure;	// функция обслуживания тэга
    _shablonFragment	*fragment;	// список фрагментов шаблона, вызовы локальных и глобальных тегов (формируется при обслуживании)  - список ( __sh_fragment_t )
} _shablonProcedure; 



#define	__get_tag_name(a,b)	{Sn = So+a;				\
				Se = strstr(Sn,b);			\
				if (!Se) {				\
				    shablon::putStr(_shablon_ExecuteError "error shablon tag = ");	\
				    shablon::putStr(Sn, 20);		\
				    shablon::putStr("...\n");		\
				    return 0;				\
				}					\
				*Se = 0;				\
				if (!*Sn) {				\
				    shablon::putStr(_shablon_ExecuteError "error tag name empty\n");	\
				    return 0;				\
				}					\
				Sc = Se+sizeof(b)-1;}

#define	format_Push()	int formatCurrSave = shablon::formatCurrent
#define	format_Pop()	shablon::formatCurrent = formatCurrSave



#endif

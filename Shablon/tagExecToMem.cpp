/* tagExecToMem.cpp								2018-05-15

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_shablon.hpp"


char	*shablon::tagExecToMem(const char *tag)
{
    char *s = (char *)"";
    // pushBuffer
    if (shablon::buffAltActive) __exAlgorithm();
    shablon::buffAltActive = true;
    shablonBuffOutput *save_buffOutputFirst = shablon::buffOutputFirst;
    shablonBuffOutput *save_buffOutputCurr = shablon::buffOutputCurr;
    int save_lengthFull = shablon::lengthFull;
    int save_formatCurrent = shablon::formatCurrent;
    int save_spaceMode = shablon::spaceMode;
    char save_lastSymbol = shablon::lastSymbol;

    shablon::buffOutputFirst = shablon::buffOutputFirsAlt;
    shablon::buffClear();
    if (!shablon::buffOutputFirst)
	shablon::newBuffOutput();

    if (shablon::_tagExec(tag, "", 0) && shablon::lengthFull>0) {
	s = (char *)((M)?M->Alloc(shablon::lengthFull+1):malloc(shablon::lengthFull+1));
	shablon::buffCopy(s);
    }

    // popBuffer
    shablon::buffOutputFirsAlt = shablon::buffOutputFirst;
    shablon::buffAltActive = false;
    shablon::buffOutputFirst = save_buffOutputFirst;
    shablon::buffOutputCurr = save_buffOutputCurr;
    shablon::lengthFull = save_lengthFull;
    shablon::formatCurrent = save_formatCurrent;
    shablon::spaceMode = save_spaceMode;
    shablon::lastSymbol = save_lastSymbol;
    return s;
}

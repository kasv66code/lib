/* loopLocal.c									2018-05-15

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_shablon.hpp"


void	*shablon::add_fragment(const char *s, int type)
{
    if (s && *s) {
	_shablonFragment* q = NULL;
	if (M) q=(_shablonFragment*)M->Calloc(sizeof(_shablonFragment));
	else q=(_shablonFragment*)calloc(1,sizeof(_shablonFragment));
	q->str = s;
	q->type = (shablonType)type;
	return q;
    }
    return NULL;
}


#define	__add_fragment(a,b)	{qn = (_shablonFragment*)add_fragment(a,b);	\
				if (qn) {			\
				    if (q) {			\
					q->next = qn;		\
				    }				\
				    if (!d->fragment) {		\
					d->fragment = qn;	\
				    }				\
				    q = qn;			\
				}}


void	shablon::loopLocal(char *s, char *name)
{
    _shablonProcedure *d;
    _shablonFragment *q=NULL, *qn;
    nodeStr v = shablon::tags->Find(name);

    if (!v) {
	if (shablon::tagAdd(name, shablonPutTrue)==false)
	    return;
	v = shablon::tags->Find(name);
	if (!v)
	    return;
    }

    d = (_shablonProcedure *)v->data;

    char *So, *Se, *Sn, *Sc;
    for( Sc=s; (So=strstr(Sc,"[#"))!=NULL; ) {
	*So=0;
	if (*Sc)
	    __add_fragment(Sc,shablonType_String);

	So+=2;
	if (*So == '#') {	// локальная переменная
	    Sn = So+1;
	    Se = strstr(Sn,"##]");
	    if (!Se) {
		__add_fragment("error syntactic local tag",shablonType_String);
		return;
	    }
	    *Se = 0;
	    Sc = Se + 3;

	    __add_fragment(Sn,shablonType_Local);
	    continue;
	}

	Sn = So;
	Se = strstr(Sn,"#]");
	if (!Se) {
	    __add_fragment("error syntactic global tag",shablonType_String);
	    return;
	}
	*Se = 0;
	Sc = Se + 2;

	switch (*Sn) {
	    case '%':
		__add_fragment(Sn,shablonType_Procent);
		break;
	    case '@':
		__add_fragment(Sn,shablonType_At);
		break;
	    default:
		__add_fragment(Sn,shablonType_Global);
	}
    }

    if (*Sc)
	__add_fragment(Sc,shablonType_String);
}

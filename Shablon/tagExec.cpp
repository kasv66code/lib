/* tagExec.cpp								2018-05-15

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_shablon.hpp"


// ------------------------------------------------------------------------------------------------
void	shablon::formatGet(const char *tag_name)
{
    int format_str_curr = 0;
    if (tag_name) {
	char c;
	for(; (c=*tag_name)!=0; tag_name++) {
	    if (c==':' || c=='|')
		break;
	    switch (c) {
		case 'j':
		    format_str_curr |= shablonFORMAT_JS;
		    continue;
		case 'h':
		    format_str_curr |= shablonFORMAT_HTML;
		    continue;
		case 'u':
		    format_str_curr |= shablonFORMAT_URL;
		    continue;
		case 'b':
		    format_str_curr |= shablonFORMAT_NL_to_BR;
		    continue;
		case 't':
		    format_str_curr |= shablonFORMAT_HTML_TEXTAREA;
		    continue;
		case 's':
		    format_str_curr |= shablonFORMAT_JSON;
		    continue;
	    }
	    break;
	}
    }
    shablon::formatCurrent = format_str_curr;
}

const char	*shablon::formatCurrSet(const char *tag_name)
{
    if (tag_name && *tag_name) {
	const char *se = strpbrk(tag_name, ":|");
	if (se) {
	    formatGet(tag_name);
	    return se+1;
	}
    }
    return tag_name;
}

bool	shablon::__tagExec(const char *tag_name, const char *s, int recurse_level)
{
    if (!tag_name || !*tag_name) {
empty:	;
	shablon::putStr(_shablon_ExecuteError "error tag name empty\n");
	return 0;
    }

    if (*tag_name == '%' || *tag_name == '@') {
	if (!tag_name[1])
	    return 0;

	if (*tag_name == '%') {
	    if (shablon::func_percent)
		return (shablon::func_percent)(this, tag_name+1, s);
	} else if (*tag_name == '@') {
	    if (shablon::func_at)
		return (shablon::func_at)(this, tag_name+1, s);
	}
	return true;
    } else {
	char Sbuff[_shablon_MAX_LEN_TAG_NAME+1];
	const char *local=NULL;

	const char *se = strchr(tag_name, '.');
	if (se) {
	    int l = se-tag_name;
	    if (l>0 && l<_shablon_MAX_LEN_TAG_NAME) {
		strncpy(Sbuff, tag_name, sizeof(Sbuff)-1);
		Sbuff[l]=0;
		local = se+1;
		tag_name = Sbuff;
	    } else
		goto empty;
	}

	_shablonProcedure *d;
	nodeStr v = shablon::tags->Find((char *)tag_name);

	if (!v)
	    return 0;
	
	d = (_shablonProcedure *)v->data;
	if (d->_procedure) {

	    if (local && *local) {
		(d->_procedure)(this, local);
		return 1;
	    }

	    if (!s)		// проверка условия
		return (d->_procedure)(this, NULL);

	    if (!*s) {	// выполнение функции
		if ((d->_procedure)(this, "")==false)
		    return 0;

		if (d->fragment) {	// цикл отображения списка
		    _shablonFragment *q = d->fragment;

		    for(; q; q=q->next) {
			switch (q->type) {
			    case shablonType_String:
				shablon::putStr(q->str);
				break;
			    case shablonType_Global:
			    case shablonType_Procent:
			    case shablonType_At:
				shablon::_tagExec(q->str, "", recurse_level);
				break;
			    case shablonType_Local: 
				if (d->_procedure) {
				    format_Push();
				    (d->_procedure)(this, formatCurrSet(q->str));
				    format_Pop();
				    break;
				}
			}
		    }
		}

		return 1;
	    }
	}
    }
    return 0;
}
// ------------------------------------------------------------------------------------------------

bool	shablon::_tagExec(const char *tag_name, const char *s, int recurse_level)
{
    bool ret = 0;
    if (tag_name && *tag_name && recurse_level<=_shablon_MAX_PROC_RECURSE_LEVEL) {
	format_Push();
	ret = shablon::__tagExec(shablon::formatCurrSet(tag_name), s, recurse_level+1);
	format_Pop();
    }
    return ret;
}

bool	shablon::tagExec(const char *tag_name)
{
    return shablon::_tagExec(tag_name, "", 0);
}

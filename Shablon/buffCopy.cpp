/* buffCopy.cpp									2018-05-15

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_shablon.hpp"


void	shablon::buffCopy(char *s)
{
    shablonBuffOutput *q = shablon::buffOutputFirst;
    for(; q; q = q->next) {
	memcpy(s, q->s, q->len);
	s += q->len;
    }
    *s = 0;
}

int	shablon::buffLength(void)
{
    return shablon::lengthFull;
}

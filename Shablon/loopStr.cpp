/* loopStr.cpp									2018-05-15

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_shablon.hpp"


int	shablon::loopStr(char *s, int recurse_level)
{
    char *So, *Se, *Sn, *Sc;

    if (!s || !*s)
	return 1;

    for( Sc=s; (So=strstr(Sc,"[#"))!=NULL; ) {
	*So=0;
	shablon::putStr(Sc);
	So+=2;

	switch (*So) {
	    case '!':		// комментарий - пропустить до "!#]"
		__get_tag_name(1,"!#]");
		continue;
	    case '{':		// комментарий - пропустить до "}#]"
		__get_tag_name(1,"}#]");
		continue;
	    case '*':		// описание шаблона (+ локальные теги)
		__get_tag_name(1,"#]");

		Se = strstr(Sc,"[#/#]");
		if (!Se) {
		    shablon::putStr(_shablon_ExecuteError "error shablon tag = *");
		    shablon::putStr(Sn, 20);
		    shablon::putStr("...\n");
		    return 0;
		}
		*Se = 0;
		shablon::loopLocal(Sc, Sn);
		for(Sc=Se+5; *Sc>0 && *Sc<=' '; Sc++)
		    ;
		continue;
	    case '"':		// не обрабатывать фрагмент html-кода (выдать без анализа)
		if (strncmp(So+1,"#]",2)) {
quote_err:	    ;
		    shablon::putStr(_shablon_ExecuteError "error shablon tag \"\"");
		    return 0;
		}
		Sc = So+3;

		Se = strstr(Sc,"[#/\"#]");
		if (!Se)
		    goto quote_err;

		*Se = 0;
		shablon::putStr(Sc);
		for(Sc=Se+6; *Sc>0 && *Sc<=' '; Sc++)
		    ;
		continue;
	}

	if (!strncmp(So,"include ", 8)) {	// вызов файла
	    __get_tag_name(8,"#]");
	    shablon::loopStr(Sn, recurse_level);
	    continue;
	}

	if (!strncmp(So,"loop ", 5)) {	// вызов цикла
	    __get_tag_name(5,"#]");
	    if (shablon::loopFor(Sn, 1)<0)
		return 0;
	    continue;
	}

	if (!strncmp(So,"if ", 3) || !strncmp(So,"ifn ", 4)) {		// условные операторы
	    if ((Sc = shablon::loopIF(So, So[2], 1))==NULL)
		return 0;
	    continue;
	}

	// вызов функции
	__get_tag_name(0,"#]");
	shablon::_tagExec(Sn, "", recurse_level);
    }
    shablon::putStr(Sc);

    return 1;
}

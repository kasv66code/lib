/* buffClear.cpp								2018-05-15

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_shablon.hpp"


void	shablon::buffClear(void)
{
    shablonBuffOutput *q = shablon::buffOutputFirst;
    for(; q; q=q->next)
	q->len = 0;
    shablon::buffOutputCurr = shablon::buffOutputFirst;
    shablon::lengthFull = 0;
    shablon::formatCurrent = 0;
    shablon::spaceMode = 0;
    shablon::lastSymbol = 0;
}

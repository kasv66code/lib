/* shablon.cpp									2018-05-15

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_shablon.hpp"


void	*shablon::operator new(size_t size, mem *m)
{
    if (m) {
        shablon* q = (shablon*)m->Calloc(size);
        q->M = m;
        return q;
    }
    return calloc(1, size);
}

void	shablon::operator delete(void *p)
{
    shablon* q = (shablon*)p;
    mem* m = q->M;
    if (m)
        m->Free(p);
    else
        free(p);
}

void    shablon::operator delete(void* p, mem* m)
{
    if (m)
        m->Free(p);
    else
        free(p);
}

// Конструктор ------------------------------------------------------------------------------------
shablon::shablon(void)
{
    tags = new(M) treeStr(treeStr::modeKeyCmp::ascii);
    newBuffOutput();
}

// ------------------------------------------------------------------------------------------------
shablon::~shablon(void)
{
    // удаление не реализовано: после выполнения шаблон удаляется вместе с памятью или программа завершается
    // если удаление когда-нибудь понадобится, необходимо освободить всю выделенную память
}

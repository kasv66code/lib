/* put_Str.cpp									2018-05-15

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_shablon.hpp"
#include <stdio.h>


void	shablon::_putCharInBuff(char c)
{
    if (shablon::buffOutputCurr->len >= shablon::buffOutputCurr->size)
	shablon::newBuffOutput();

    shablon::buffOutputCurr->s[shablon::buffOutputCurr->len++] = c;
    shablon::lengthFull++;
    shablon::lastSymbol = c;
}

void	shablon::_putStrInBuff(const char *str, int len)
{
    if (!str)
	return;
 
    char c, s[32];
    int i;
    for(i=0, *s=0; (!len || i<len) && (c=str[i])!=0; i++) {
	if (shablon::formatCurrent) {
	    // преобразование служебных символов
	    if (shablon::formatCurrent & shablonFORMAT_URL) {
		if ((c<' ') || c==127 || strchr("\"?;<>/:&=%+\\#'", c)) {
		    sprintf(s, "%%%02x", ((int)c) & 0xff);
		}
	    } else {
		switch (c) {
		    case '>':
			if (shablon::formatCurrent & (shablonFORMAT_HTML | shablonFORMAT_HTML_TEXTAREA)) {
			    strcpy(s, "&gt;");
			}
			break;
		    case '<':
			if (shablon::formatCurrent & (shablonFORMAT_HTML | shablonFORMAT_HTML_TEXTAREA)) {
			    strcpy(s, "&lt;");
			}
			break;
		    case '"':
			if (shablon::formatCurrent & shablonFORMAT_HTML) {
			    strcpy(s, "&quot;");
			} else if (shablon::formatCurrent & (shablonFORMAT_JS | shablonFORMAT_JSON)) {
			    strcpy(s, "\\\"");
			}
			break;
		    case '\'':
			if (shablon::formatCurrent & (shablonFORMAT_HTML)) {
			    strcpy(s, "&#39;");
			} else if (shablon::formatCurrent & shablonFORMAT_JS) {
			    strcpy(s, "\\'");
			}
			break;
		    case '\\':
			if (shablon::formatCurrent & shablonFORMAT_HTML) {
			    strcpy(s, "&#92;");
			} else if (shablon::formatCurrent & (shablonFORMAT_JS | shablonFORMAT_JSON)) {
			    strcpy(s, "\\\\");
			}
			break;

		    case '\n':
			if (shablon::formatCurrent & shablonFORMAT_NL_to_BR) {
			    if (shablon::formatCurrent & shablonFORMAT_HTML) {
				strcpy(s, "&lt;br&gt;");
			    } else {
				strcpy(s, "<br>");
			    }
			    break;
			}
			if (shablon::formatCurrent & (shablonFORMAT_JS | shablonFORMAT_JSON)) {
			    strcpy(s, "\\n");
			}
			break;
		    case '\t':
			if (shablon::formatCurrent & (shablonFORMAT_JS | shablonFORMAT_JSON)) {
			    strcpy(s, "\\t");
			}
			break;
		}
	    }
	}

	if (shablon::spaceNotUpdate == false) {	    // убирать лишние разделители
	    if (c<0 || c>' ' || *s) {
		if (shablon::spaceMode != shablonSpaceMode_Empty) {
		    int cp=0;
		    switch (shablon::spaceMode) {
			case shablonSpaceMode_Space:
			    cp = ' ';
			    break;
			case shablonSpaceMode_Tab:
			    cp = '\t';
			    break;
			case shablonSpaceMode_NL:
			    cp = '\n';
			    break;
		    }
		    shablon::spaceMode = shablonSpaceMode_Empty;
		    if (cp) {
			shablon::_putCharInBuff(cp);
		    }
		}
	    } else {
		if (shablon::spaceMode < shablonSpaceMode_NL) {
		    shablonSpaceMode pn=shablonSpaceMode_Empty;
		    switch (c) {
			case '\r':
			case '\n':
			    pn = shablonSpaceMode_NL;
			    break;
			case '\t':
			    pn = shablonSpaceMode_Tab;
			    break;
			default:
			    pn = shablonSpaceMode_Space;
		    }
		    if (pn > shablon::spaceMode)
			shablon::spaceMode = pn;
		}
		continue;
	    }
	}

	if (*s) {
	    int j;
	    for(j=0; (c=s[j])!=0; j++) {
		shablon::_putCharInBuff(c);
	    }
	    *s = 0;
	} else {
	    shablon::_putCharInBuff(c);
	}
    }
}

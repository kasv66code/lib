/* run.cpp									2018-05-15

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_shablon.hpp"


int	shablon::run(void)
{
    if (runFunction) {
        runFunction(this);
    } else if (runString) {
        if (*runString) {
            char* s = (M) ? M->StrDup(runString) : strdup(runString);
            if (s) {
                shablon::loopStr(s, 0);
                if (M) M->Free(s); else free(s);
            }
        }
    } else {
        if (!shablon::dirName || !*shablon::dirName) {
            shablon::putStr(_shablon_ExecuteError "empty html dir name\n");
            return 0;
        }
        if (!shablon::files) {
            shablon::putStr(_shablon_ExecuteError "empty class cacheFiles\n");
            return 0;
        }
        if (!shablon::runFileName || !*shablon::runFileName) {
            shablon::putStr(_shablon_ExecuteError "empty file name\n");
            return 0;
        }
        enaInclude = 1;
        shablon::loopFile(shablon::runFileName, 0);
    }

//    if (shablon::lastSymbol != '\n') shablon::_putCharInBuff('\n');

    // записать 0 на всякий случай (если буфер заполнен полностью, то 0 в конце уже есть)
    if (shablon::buffOutputCurr->len < shablon::buffOutputCurr->size)
	shablon::buffOutputCurr->s[shablon::buffOutputCurr->len] = 0;

    if (shablon::func_aftereffect)
	shablon::func_aftereffect(this);

    return 1;
}

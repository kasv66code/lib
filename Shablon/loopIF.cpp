/* loopIF.c									2018-05-15

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_shablon.hpp"


char	*shablon::loopIF(char *So, char c, int recurse_level)
{
    char *Se, *Sn, *Sc, *Sr;
    int ena=0;
    char st[_shablon_MAX_LEN_TAG_NAME+1];

    __get_tag_name(3+((c=='n')?1:0),"#]");

    if (strlen(Sn)>_shablon_MAX_LEN_TAG_NAME-10) {
err:	;
	shablon::putStr(_shablon_ExecuteError "error shablon IF = ");
	shablon::putStr(Sn, 20);
	shablon::putStr("...\n");
	return NULL;
    }

    ena = shablon::_tagExec(Sn, NULL, recurse_level);
    if (c=='n')
	ena = (ena) ? 0 : 1;

    strcpy(st, "[#endif ");
    strcat(st, Sn);
    strcat(st, "#]");

    Se = strstr(Sc,st);
    if (!Se)
	goto err;

    *Se = 0;
    Se += strlen(st);

    strcpy(st, "[#else ");
    strcat(st, Sn);
    strcat(st, "#]");

    Sr = strstr(Sc,st);
    if (Sr) {
	*Sr = 0;
	Sr += strlen(st);
	loopStr( (ena) ? Sc : Sr, recurse_level );
    } else if (ena)
	loopStr( Sc, recurse_level );

    return Se;
}

/* newBuffOutput.cpp								2018-05-15

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_shablon.hpp"


void    shablon::newBuffOutput(void)
{
    shablonBuffOutput *q=NULL;

    if (shablon::buffOutputCurr && shablon::buffOutputCurr->next) {
	q = shablon::buffOutputCurr->next;
	shablon::buffOutputCurr = q;
	return;
    }

    q = (shablonBuffOutput*)((M) ? M->Calloc(sizeof(shablonBuffOutput)) : calloc(1, sizeof(shablonBuffOutput)));
    q->s = (char *)((M)?M->Alloc(_shablon_MEM_BUF_SIZE+1) : malloc(_shablon_MEM_BUF_SIZE+1));
    q->size = _shablon_MEM_BUF_SIZE;
    q->s[_shablon_MEM_BUF_SIZE] = 0;

    if (shablon::buffOutputCurr) {
	shablon::buffOutputCurr->next = q;
	shablon::buffOutputCurr = q;
    } else {
	shablon::buffOutputFirst = q;
	shablon::buffOutputCurr = q;
    }
}

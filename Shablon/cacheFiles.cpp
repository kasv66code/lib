/* cacheFiles.cpp								2018-05-14

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "../conv_numb.hpp"
#include "../conv_str.hpp"
#include "../shablon.hpp"
#include "../exceptions.hpp"
#include "../file.hpp"
#include "../tools.hpp"
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>


void	*cacheFiles::operator new(size_t size, class mem *m)
{
    if (m) {
        cacheFiles* q = (cacheFiles*)m->Calloc(size);
        q->M = m;
        return q;
    }
    cacheFiles* q = (cacheFiles*)calloc(1, size);
    return q;
}

void	cacheFiles::operator delete(void *p)
{
    class cacheFiles *q = (class cacheFiles *)p;
    mem* m = q->M;
    if (m)
        m->Free(p);
    else
        free(p);
}

void    cacheFiles::operator delete(void* p, mem* m)
{
    if (m)
        m->Free(p);
    else
        free(p);
}

// Конструктор ------------------------------------------------------------------------------------
static void	cacheFilesFreeData(void* Qx, nodeStr v)
{
    if (!v || !v->data) return;
    cacheFileData *q = (cacheFileData*)v->data;
    mem* M = (mem*)Qx;
    if (M) {
        if (q->f_data) M->Free(q->f_data);
        M->Free(q);
    } else {
        if (q->f_data)free(q->f_data);
        free(q);
    }
}

cacheFiles::cacheFiles(void)
{
    cacheFiles::tree = new(M) treeStr(treeStr::modeKeyCmp::none, M, NULL, cacheFilesFreeData);
}
// ------------------------------------------------------------------------------------------------


// Деструктор ------------------------------------------------------------------------------------
cacheFiles::~cacheFiles(void)
{
    delete tree;
}
// ------------------------------------------------------------------------------------------------


cacheFileData	*cacheFiles::getFile(const char *dir, const char *name)
{
    char *fn = makeFileNameFull(dir, name);
    if (!fn) __exNull();

    // поискать в кэше
    nodeStr v = cacheFiles::tree->Insert(fn);
    if (!v) __exNull();
    if (!(v->data)) {
	// файл ранее не запрашивался - создать структуры и добавить в дерево
        if (M)
	    v->data = M->Calloc(sizeof(cacheFileData));
        else
	    v->data = calloc(1, sizeof(cacheFileData));
    }

    cacheFileData *q = (cacheFileData *)v->data;
    if (!q) __exNull();

    time_t curr_time;
    getCurrTime_s(curr_time);

    if (q->t_check_tm < curr_time) {
	q->t_check_tm = curr_time + CACHE_LIFE_SECOND;

	int length;
	time_t mtime;
	if (_fileGetStat(fn, &length, &mtime)) {
	    if (q->f_mtime != mtime || q->f_length != length) {
		// необходимо обновить файл
		q->f_mtime = mtime;
		q->f_length = length;

                q->f_data = FileToStr(M, fn, q->f_length);
	    }
	} else {
	    q->f_NotFound = 1;
	    if (q->f_data) {
                if (M) M->Free(q->f_data);
                else free(q->f_data);
		q->f_data = NULL;
	    }
	    q->f_length = 0;
	    q->f_mtime = 0;
	}
    }

    return (q->f_data) ? q : NULL;
}

char	*cacheFiles::makeFileNameFull(const char *dn, const char *fn)
{
    if (!dn || !*dn || !fn || !*fn)
	return NULL;

    int ld = strlen(dn);
    int lf = strlen(fn);
    int slash = (dn[ld-1] != '/')?1:0;

    int len = ld+lf+slash+1;

    char* s = NULL;
    
    if (M) s = (char*)M->Alloc(len); else s = (char*)malloc(len);

    if (!_fileNameMake(fileNameMakeMode_makeOnly, s, dn, fn, NULL)) __exAlgorithm();

    return s;
}

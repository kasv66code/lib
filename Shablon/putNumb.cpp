/* putStr.cpp									2018-05-15

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_shablon.hpp"
#include <stdio.h>


// ------------------------------------------------------------------------------------------------
void	shablon::putNumb(int d)
{
    char s[32];
    sprintf(s, "%d", d);
    shablon::_putStrInBuff(s);
}

void	shablon::putNumb(long long int d)
{
    char s[32];
    sprintf(s, "%lld", d);
    shablon::_putStrInBuff(s);
}

void	shablon::putNumbHex(int d)
{
    char s[32];
    sprintf(s, "%x", d);
    shablon::_putStrInBuff(s);
}

void	shablon::putNumbHex(long long int d)
{
    char s[32];
    sprintf(s, "%llx", d);
    shablon::_putStrInBuff(s);
}


// ------------------------------------------------------------------------------------------------
void	shablon::putTime(time_t t)
{
    char s[32];
    sprintf(s, "%llx", (long long int)t);
    shablon::_putStrInBuff(s);
}

void	shablon::putTime(time_t t, const char* sh, const char** month)
{
    if (!sh || !*sh) sh = StringTimeFormat_Default;
    int l = strlen(sh);
    if (month) l += 10;
    char* So = (char*)alloca(l * 3);
    if (!So) __exNull();
    convToStr_timeFormat(So, t, sh, month);
    shablon::_putStrInBuff(So);
}


// ------------------------------------------------------------------------------------------------
void	shablon::putDouble(double d)
{
    char s[50];
    convToStr_double(s,d);
    shablon::_putStrInBuff(s);
}

void	shablon::putDouble(double d, int n)
{
    char s[50];
    convToStr_double_AccuracyCount(s,d,n);
    shablon::_putStrInBuff(s);
}

void	shablon::putDoubleAuto(double d)
{
    char s[50];
    convToStr_double_AccuracyAuto(s,d);
    shablon::_putStrInBuff(s);
}


// ------------------------------------------------------------------------------------------------
void	shablon::putNumbDelimited(int d)
{
    char s[32];
    sprintf(s, "%d", d);

    char so[50];
    convToStr_StrNumber(s, so);
    shablon::_putStrInBuff(so);
}

void	shablon::putDoubleDelimited(double d, int n)
{
    char s[50];
    convToStr_double_AccuracyCount(s,d,n);

    char so[50];
    convToStr_StrNumber(s, so);
    shablon::_putStrInBuff(so);
}

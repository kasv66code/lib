/* putJsonValue.cpp									2020-04-27

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_shablon.hpp"


// ------------------------------------------------------------------------------------------------
void	shablon::putJsonBool(const char* tagName, bool v)
{
    putPrintf("\"%s\":%s", tagName, v ? "true" : "false");
}

void	shablon::putJsonInt(const char* tagName, int v)
{
    putPrintf("\"%s\":%d", tagName, v);
}

void	shablon::putJsonInt(const char* tagName, long long int v)
{
    putPrintf("\"%s\":%lld", tagName, v);
}

void	shablon::putJsonTime(const char* tagName, time_t v)
{
    putPrintf("\"%s\":%lld", tagName, (long long int)v);
}

void	shablon::putJsonDouble(const char* tagName, double v)
{
    putPrintf("\"%s\":", tagName);
    putDouble(v);
}

void	shablon::putJsonDouble(const char* tagName, double v, int n)
{
    putPrintf("\"%s\":", tagName);
    putDouble(v, n);
}

// ------------------------------------------------------------------------------------------------
void	shablon::putJsonStr(const char* tagName, const char* v)
{
    putPrintf("\"%s\":\"", tagName);
    putStrF("s",v);
    putChar('\"');
}

void	shablon::putJsonStr(const char* tagName, const wchar_t* v)
{
    putPrintf("\"%s\":\"", tagName);
    putStrF("s",v);
    putChar('\"');
}

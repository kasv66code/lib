/* loopFile.cpp									2018-05-15

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_shablon.hpp"


void	shablon::loopFile(const char *shablon_file_name, int recurse_level)
{
    if (!enaInclude) {
	shablon::putStr(_shablon_ExecuteError "include disable!\n");
	return;
    }

    if (recurse_level > _shablon_MAX_INCLUDE_RECURSE_LEVEL) {
	shablon::putStr(_shablon_ExecuteError "excess include max recurse level = ");
	shablon::putNumb(recurse_level);
	shablon::putStr("\n");
	return;
    }

    cacheFileData *FD = shablon::files->getFile(shablon::dirName, shablon_file_name);
    if (!FD) {
	if (shablon::dirNameAlt && *shablon::dirNameAlt)
	    FD = shablon::files->getFile(shablon::dirNameAlt, shablon_file_name);
	if (!FD) {
	    shablon::putStr(_shablon_ExecuteError "failed open file = ");
	    shablon::putStr(shablon_file_name);
	    shablon::putStr("\n");
	    return;
	}
    }

    if (FD->f_length > 0) {
	char* s;
	if (M) s = (char *)M->Alloc(FD->f_length+1);
	else s = (char *)malloc(FD->f_length+1);
	if (!s) __exNull();

	memcpy(s, FD->f_data, FD->f_length);
	s[FD->f_length]=0;

	shablon::loopStr( s, recurse_level+1 );
    }
}

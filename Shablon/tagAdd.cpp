/* tagAdd.cpp									2018-05-15

    Author: Kachkovsky Sergey V.
    kasv@list.ru

	присваивает функцию для обслуживания тэга
	tag:
	    NULL - проверка условия;
	    ""	 - начало обслуживания;
	    "tag"- вызов для обслуживания локального имени
 */


#include "_shablon.hpp"


bool	shablon::tagAdd(const char *tag, __sh_proc_tag_t func)
{
    if (!tag || !*tag)
	return false;

    nodeStr v = shablon::tags->Insert((char *)tag);
    if (v->data == NULL)
	v->data = (M) ? M->Calloc(sizeof(_shablonProcedure)) : calloc(1, sizeof(_shablonProcedure));

    _shablonProcedure *q = (_shablonProcedure *)v->data;
    q->_procedure = func;

    return true;
}

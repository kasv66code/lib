/* putStr.cpp									2018-05-15

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_shablon.hpp"
#include <stdio.h>


// ------------------------------------------------------------------------------------------------
void	shablon::putChar(wchar_t c)
{
    if (!c) return;

    char S[10];
    int n = wctomb(S, c);
    if (n > 0) {
        S[n] = 0;
        _putStrInBuff(S);
    }
}

// ------------------------------------------------------------------------------------------------
void	shablon::_putStrInBuff(const wchar_t *str, int len)
{
    if (!str || !*str) return;

    for (int i = 0; str[i]; i++) {
        if (len > 0 && i >= len)
            break;
        shablon::putChar(str[i]);
    }
}

// ------------------------------------------------------------------------------------------------
void	shablon::putStr(const wchar_t *str, int len)
{
    shablon::_putStrInBuff(str, len);
}

void	shablon::putStrF(const char *format, const wchar_t *str, int len)
{
    if (!str || !*str) return;

    format_Push();
    shablon::formatGet(format);
    shablon::_putStrInBuff(str, len);
    format_Pop();
}


#include <stdarg.h> 

void	shablon::putPrintf(const wchar_t *fmt, ...)
{
    if (fmt) {
	va_list argp;
	va_start(argp, fmt);
	int l = vswprintf(NULL, 0, fmt, argp);
	va_end(argp);

	if (l > 0) {
            wchar_t* S = (wchar_t*)((M) ? M->Alloc((l + 1) * sizeof(wchar_t)) : malloc((l + 1) * sizeof(wchar_t)));
            va_start(argp, fmt);
	    vswprintf(S, l, fmt, argp);
	    va_end(argp);

	    shablon::putStr(S);

            if (M) M->Free(S); else free(S);
        }
    }
}

/* putStr.cpp									2018-05-15

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_shablon.hpp"
#include <stdio.h>


// ------------------------------------------------------------------------------------------------
void	shablon::putChar(char c)
{
    shablon::_putCharInBuff(c);
}

void	shablon::putStr(const char *str, int len)
{
    shablon::_putStrInBuff(str, len);
}

void	shablon::putStrF(const char *format, const char *str, int len)
{
    format_Push();
    shablon::formatGet(format);
    shablon::_putStrInBuff(str, len);
    format_Pop();
}


#include <stdarg.h> 

void	shablon::putPrintf(const char *fmt, ...)
{
    if (fmt) {
	va_list argp1;
	va_start(argp1, fmt);

	va_list argp2;
        va_copy (argp2, argp1) ;

	int l = vsnprintf(NULL, 0, fmt, argp1);
	va_end(argp1);

	if (l > 0) {
	    char* S = (char*)((M) ? M->Alloc(l + 1) : malloc(l + 1));
	    if (!S) __exNull();

	    vsnprintf(S, l+1, fmt, argp2);
	    S[l] = 0;
	    shablon::putStr(S);
	    if (M) M->Free(S); else free(S);
	}
	va_end(argp2);
    }
}

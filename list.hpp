/* list.hpp										2012-2020
			Класс для построения списков

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#ifndef	list_hpp
#define	list_hpp


#include "exceptions.hpp"
#include "mem.hpp"
#include <mutex> 



typedef struct nodeList_t {
    int			numb;		// переменная integer для пользователя
    void		*data;		// структура данных пользователя
    struct nodeList_t	*prev, *next;	// указатели для построения двунаправленного списка
} *nodeList;


typedef void	(*__freeData_nodeList_t)(void* Qx, nodeList v);
typedef void	(*__newData_nodeList_t)(void* Qx, nodeList v);
typedef int	(*__sort_compare_nodeList)(const nodeList *, const nodeList *);


// --- класс описания списка ----------------------------------------------------------------------
class list {
    public:
	void *operator new(size_t size, mem *m=NULL);
	void operator delete(void* p, mem* m);
	void operator delete(void* p);

	list(void* _Qx = NULL, __newData_nodeList_t _newData = NULL, __freeData_nodeList_t _freeData = NULL);
	~list(void);

	void   lockClose(void);		// закрыть mutex, если не предполагается вызывать десктруктор

	nodeList	AddFirst(void);			// добавить узел в начало списка
	nodeList	AddLast(void);			// добавить узел в конец списка

	nodeList	AddBefore(nodeList);		// добавить узел перед элементом списка
	nodeList	AddAfter(nodeList);		// добавить узел после элемента списка

	void		Delete(nodeList);		// удалить узел, если указана freeData, то вместе с данными

	nodeList	getFirst(void);			// узел в начале списка
	nodeList	getLast(void);			// узел в конеце списка

	// формирует массив из элементов списка и возвращает элемент массива по индексу idx
	nodeList	operator [] (int idx);

	void		sort(__sort_compare_nodeList fsort_compare);	// формирует и сортирует массив из элементов списка

	int		count(void);


    private:
	mem		*M;
	int		Count;
	nodeList	first, last;
	std::mutex	*lockL;

	nodeList	*mas;
	bool		update;
	int		sizeMasMem;

	void			*Qx;		// структура пользователя
	__newData_nodeList_t	newData;	// функция для создания данных
	__freeData_nodeList_t	freeData;	// функция для удаления данных

	nodeList	NewNode(void);
	void		MasMake(void);
	nodeList	getIndex(int idx);
};
// ---------------------------------------------------------------------- класс описания списка ---



#endif

/* tree_double.hpp								        2013-2020

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#ifndef	tree_double_hpp
#define	tree_double_hpp


#include "exceptions.hpp"
#include "mem.hpp"
#include "types.hpp"
#include <time.h> 
#include <mutex> 


typedef struct nodeDouble_t {
    void* data;		// структура данных пользователя
    int			numb;		// переменная integer для пользователя

    struct nodeDouble_t* next;		// список для неуникальных записей
    double		key;		// уникальный ключ
} *nodeDouble;


typedef int	(*__sort_compare_nodeDouble)(const nodeDouble*, const nodeDouble*);
typedef void	(*__freeData_nodeDouble_t)(void* Qx, nodeDouble v);
typedef void	(*__newData_nodeDouble_t)(void* Qx, nodeDouble v);


// --- класс дерева Time ------------------------------------------------------------------
class treeDouble {
    public:
	void* operator new(size_t size, class mem* m = NULL);
	void operator delete(void* p, mem* m);
	void operator delete(void* p);

	treeDouble(void* _Qx = NULL, __newData_nodeDouble_t _newData = NULL, __freeData_nodeDouble_t _freeData = NULL);
	~treeDouble(void);

	void	    lockClose(void);		// закрыть mutex, если не предполагается вызывать десктруктор

	void	    treeFree(void);
	void	    treeMasFree(void);

	nodeDouble	    First(void);
	nodeDouble	    Last(void);

	nodeDouble	    Find(double key);			// поиск по совпадению - возвращает адрес узла
	nodeDouble	    Insert(double key);			// добавить узел - возвращает адрес узла
	void		    Delete(double key);			// удаляет из дерева ключ вместе с данными (если указана __free_nodeStringData)
	nodeDouble	    AppendDataInListLast(nodeDouble q);	// добавляет элемент в конец списка (для неуникальных записей)
	nodeDouble	    AppendDataInListFirst(nodeDouble q);	// добавляет элемент в начало списка (для неуникальных записей)

	// формирует массив из элементов дерева и возвращает элемент массива по индексу idx
	nodeDouble	operator [] (int idx);

	int	            count(void);

	// формирует и сортирует массив из элементов дерева
	void	    sort(__sort_compare_nodeDouble fsort_compare);

	friend void*    _treeDoubleKeyNew(class _Mem* m, void* qu, const void* key);
	friend void     _treeDoubleKeyFree(class _Mem* m, void* qu, void* key);
    private:
	tree* T;
	mem* M;
	std::mutex* lockT;

	void*       makeNodeKey(const void* key);
	void	makeNodeKey(nodeDouble q, double key);

	void* Qx;		// структура пользователя
	__newData_nodeDouble_t	newData;	// функция для создания данных ключа
	__freeData_nodeDouble_t	freeData;	// функция для удаления данных ключа
};
// ------------------------------------------------------------------ класс дерева Time ---



#endif

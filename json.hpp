/*  json.hpp								                2020

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#ifndef	_json_hpp
#define	_json_hpp

#include <time.h>
#include "types.hpp"
#include "list.hpp"
#include "tree_str.hpp"


class JSON {
    public:
        void* operator new(size_t size, mem* m = NULL);
        void operator delete(void* p, mem* m);
        void operator delete(void* p);
        
        JSON(void);
        ~JSON(void);

        enum class typeValueJSON {
            Empty=0,
            VALUE_Null,
            VALUE_False,
            VALUE_True,
            VALUE_Number,
            VALUE_Double,
            STR,
            MAS,
            OBJ
        };

        treeStr*    Tj;

        bool        LoadFromStr(const char *strJSON, const char** Serr=NULL);
        bool        LoadFromFile(const char *fileName);

        void        DelNode(int n, void* d);

        typeValueJSON   getValueType(int numb);
        bool            getValueBool(int numb, void* data);
        i32             getValueI32(int numb, void* data);
        i64             getValueI64(int numb, void* data);
        double          getValueDouble(int numb, void* data);
        char*           getValueStr(int numb, void* data);
        list*           getValueMas(int numb, void* data);
        treeStr*        getValueObj(int numb, void* data);

        bool            getValueBool(const char *field, treeStr *T=NULL);
        i32             getValueI32(const char* field, treeStr* T = NULL);
        i64             getValueI64(const char* field, treeStr* T = NULL);
        double          getValueDouble(const char* field, treeStr* T = NULL);
        char*           getValueStr(const char* field, treeStr* T = NULL);
        list*           getValueMas(const char* field, treeStr* T = NULL);
        treeStr*        getValueObj(const char* field, treeStr* T = NULL);

    private:
        mem*        M;
        list*       newList(void);
        treeStr*    newTree(void);
        char*       newStr(const char *);

        bool        ParseObject(char ** s, treeStr **T);
        const char* nextChar(char ** s);
        bool        ParseElement(char ** s, treeStr*);
        bool        ParseElement(char ** s, list*);

        bool        checkValue(char *s, typeValueJSON& t);
        char*       parseValue(char **s, typeValueJSON &t);
        char*       parseString(char **s);
        bool        ParseMassive(char ** s, list **L);

};


#endif

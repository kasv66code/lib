/* tree_time.hpp								        2013-2020

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#ifndef	tree_time_hpp
#define	tree_time_hpp


#include "exceptions.hpp"
#include "mem.hpp"
#include "types.hpp"
#include <time.h> 
#include <mutex> 


typedef struct nodeTime_t {
    void* data;		// структура данных пользователя
    int			numb;		// переменная integer для пользователя

    struct nodeTime_t* next;		// список для неуникальных записей
    time_t		key;		// уникальный ключ
} *nodeTime;


typedef int	(*__sort_compare_nodeTime)(const nodeTime*, const nodeTime*);
typedef void	(*__freeData_nodeTime_t)(void* Qx, nodeTime v);
typedef void	(*__newData_nodeTime_t)(void* Qx, nodeTime v);


// --- класс дерева Time ------------------------------------------------------------------
class treeTime {
public:
    void* operator new(size_t size, class mem* m = NULL);
    void operator delete(void* p, mem* m);
    void operator delete(void* p);

    treeTime(void* _Qx = NULL, __newData_nodeTime_t _newData = NULL, __freeData_nodeTime_t _freeData = NULL);
    ~treeTime(void);

    void	    lockClose(void);		// закрыть mutex, если не предполагается вызывать десктруктор

    void	    treeFree(void);
    void	    treeMasFree(void);

    nodeTime	    First(void);
    nodeTime	    Last(void);

    nodeTime	    Find(time_t key);			// поиск по совпадению - возвращает адрес узла
    nodeTime	    Insert(time_t key);			// добавить узел - возвращает адрес узла
    void	    Delete(time_t key);			// удаляет из дерева ключ вместе с данными (если указана __free_nodeStringData)
    nodeTime	    AppendDataInListLast(nodeTime q);	// добавляет элемент в конец списка (для неуникальных записей)
    nodeTime	    AppendDataInListFirst(nodeTime q);	// добавляет элемент в начало списка (для неуникальных записей)

    // формирует массив из элементов дерева и возвращает элемент массива по индексу idx
    nodeTime	operator [] (int idx);

    int	            count(void);

    // формирует и сортирует массив из элементов дерева
    void	    sort(__sort_compare_nodeTime fsort_compare);

    friend void*    _treeTimeKeyNew(class _Mem* m, void* qu, const void* key);
    friend void     _treeTimeKeyFree(class _Mem* m, void* qu, void* key);
private:
    tree* T;
    mem* M;
    std::mutex* lockT;

    void*       makeNodeKey(const void* key);
    void	makeNodeKey(nodeTime q, time_t key);

    void* Qx;		// структура пользователя
    __newData_nodeTime_t	newData;	// функция для создания данных ключа
    __freeData_nodeTime_t	freeData;	// функция для удаления данных ключа
};
// ------------------------------------------------------------------ класс дерева Time ---



#endif

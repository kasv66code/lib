/* json.сpp										2020-05-06

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_json.hpp"


// ------------------------------------------------------------------------------------------------
void* JSON::operator new(size_t size, class mem* m)
{
    if (m) {
        class JSON* q = (class JSON*)m->Calloc(size);
        q->M = m;
        return q;
    }
    class treeList* q = (class treeList*)calloc(1, size);
    return q;
}

void	JSON::operator delete(void* p)
{
    class JSON* q = (class JSON*)p;
    class mem* m = q->M;
    if (m)
        m->Free(p);
    else
        free(p);
}

void    JSON::operator delete(void* p, mem* m)
{
    if (m)
        m->Free(p);
    else
        free(p);
}

// ------------------------------------------------------------------------------------------------
void        JSON::DelNode(int n, void *d)
{
    if (n<=0 || !d) return;

    switch ((JSON::typeValueJSON)n) {
        case JSON::typeValueJSON::STR:
            {
                if (M)
                    M->Free(d);
                else
                    free(d);
            }
            break;
        case JSON::typeValueJSON::MAS:
            {
                list* L = (list*)d;
                delete L;
            }
            break;
        case JSON::typeValueJSON::OBJ:
            {
                treeStr* T = (treeStr*)d;
                delete T;
            }
            break;
    }
}

static void	__freeData_nodeObjJSON(void* Qx, nodeStr v)
{
    if (!Qx) return;

    JSON *Q = (JSON *)Qx;
    Q->DelNode(v->numb, v->data);
}

static void	__freeData_nodeListJSON(void* Qx, nodeList v)
{
    if (!Qx) return;

    JSON *Q = (JSON *)Qx;
    Q->DelNode(v->numb, v->data);
}

list* JSON::newList(void)
{
    return new(M) list(this, NULL, __freeData_nodeListJSON);
}

treeStr* JSON::newTree(void)
{
    return new(M) treeStr(treeStr::modeKeyCmp::ascii, this, NULL, __freeData_nodeObjJSON);
}

char* JSON::newStr(const char*s)
{
    if (!s) return NULL;
    return (M) ? M->StrDup(s) : strdup(s);
}

// ------------------------------------------------------------------------------------------------
JSON::JSON(void)
{
}

JSON::~JSON(void)
{
    if (Tj) delete Tj;
    Tj = NULL;
}

/* jsonGetValue.сpp									2020-05-07

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_json.hpp"


// ------------------------------------------------------------------------------------------------
JSON::typeValueJSON   JSON::getValueType(int numb)
{
    return (JSON::typeValueJSON)numb;
}


// ------------------------------------------------------------------------------------------------
bool    JSON::getValueBool(int numb, void* data)
{
    switch ((JSON::typeValueJSON)numb) {
        case JSON::typeValueJSON::VALUE_Null:
        case JSON::typeValueJSON::VALUE_False:
            return 0;
        case JSON::typeValueJSON::VALUE_True:
            return 1;
        case JSON::typeValueJSON::VALUE_Number:
            return (convStrTo_i32((char*)data) == 0) ? 0 : 1;
        case JSON::typeValueJSON::VALUE_Double:
            return (convStrTo_double((char*)data) == 0) ? 0 : 1;
        case JSON::typeValueJSON::STR:
            return (data && *((char *)data)) ? 1 : 0;
        case JSON::typeValueJSON::MAS:
        {
            list* L = (list*)data;
            return (L && L->count() > 0) ? 1 : 0;
        }
        case JSON::typeValueJSON::OBJ:
        {
            treeStr* L = (treeStr*)data;
            return (L && L->count() > 0) ? 1 : 0;
        }
    }
    return 0;
}


// ------------------------------------------------------------------------------------------------
i32     JSON::getValueI32(int numb, void* data)
{
    switch ((JSON::typeValueJSON)numb) {
        case JSON::typeValueJSON::VALUE_Null:
        case JSON::typeValueJSON::VALUE_False:
            return 0;
        case JSON::typeValueJSON::VALUE_True:
            return 1;
        case JSON::typeValueJSON::VALUE_Number:
        case JSON::typeValueJSON::STR:
            return convStrTo_i32((char*)data);
        case JSON::typeValueJSON::VALUE_Double:
            return (int)(convStrTo_double((char*)data));
        case JSON::typeValueJSON::MAS:
        case JSON::typeValueJSON::OBJ:
            break;
    }
    return 0;
}

i64     JSON::getValueI64(int numb, void* data)
{
    switch ((JSON::typeValueJSON)numb) {
        case JSON::typeValueJSON::VALUE_Null:
        case JSON::typeValueJSON::VALUE_False:
            return 0;
        case JSON::typeValueJSON::VALUE_True:
            return 1;
        case JSON::typeValueJSON::VALUE_Number:
        case JSON::typeValueJSON::STR:
            return convStrTo_i64((char*)data);
        case JSON::typeValueJSON::VALUE_Double:
            return (i64)(convStrTo_double((char*)data));
        case JSON::typeValueJSON::MAS:
        case JSON::typeValueJSON::OBJ:
            break;
    }
    return 0;
}


// ------------------------------------------------------------------------------------------------
double  JSON::getValueDouble(int numb, void* data)
{
    switch ((JSON::typeValueJSON)numb) {
        case JSON::typeValueJSON::VALUE_Null:
        case JSON::typeValueJSON::VALUE_False:
            return 0;
        case JSON::typeValueJSON::VALUE_True:
            return 1;
        case JSON::typeValueJSON::VALUE_Number:
        case JSON::typeValueJSON::VALUE_Double:
        case JSON::typeValueJSON::STR:
            return (convStrTo_double((char*)data));
        case JSON::typeValueJSON::MAS:
        case JSON::typeValueJSON::OBJ:
            break;
    }
    return 0;
}


// ------------------------------------------------------------------------------------------------
char* JSON::getValueStr(int numb, void* data)
{
    switch ((JSON::typeValueJSON)numb) {
        case JSON::typeValueJSON::VALUE_Null:
        case JSON::typeValueJSON::VALUE_False:
        case JSON::typeValueJSON::VALUE_True:
        case JSON::typeValueJSON::VALUE_Number:
        case JSON::typeValueJSON::VALUE_Double:
        case JSON::typeValueJSON::STR:
            return (char*)data;
        case JSON::typeValueJSON::MAS:
        case JSON::typeValueJSON::OBJ:
            break;
    }
    return NULL;
}


// ------------------------------------------------------------------------------------------------
list* JSON::getValueMas(int numb, void* data)
{
    if ((JSON::typeValueJSON)numb == JSON::typeValueJSON::MAS)
        return (list*)data;
    return NULL;
}

treeStr* JSON::getValueObj(int numb, void* data)
{
    if ((JSON::typeValueJSON)numb == JSON::typeValueJSON::OBJ)
        return (treeStr*)data;
    return NULL;
}

/* jsonGetField.сpp									2020-05-07

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_json.hpp"


// ------------------------------------------------------------------------------------------------
#define GET_FIELD()     if (!T) T=Tj; \
                        nodeStr v = T->Find(field)

// ------------------------------------------------------------------------------------------------
bool    JSON::getValueBool(const char* field, treeStr* T)
{
    GET_FIELD();
    if (v && v->numb && v->data) return getValueBool(v->numb, v->data);
    return 0;
}

i32     JSON::getValueI32(const char* field, treeStr* T)
{
    GET_FIELD();
    if (v && v->numb && v->data) return getValueI32(v->numb, v->data);
    return 0;
}

i64     JSON::getValueI64(const char* field, treeStr* T)
{
    GET_FIELD();
    if (v && v->numb && v->data) return getValueI64(v->numb, v->data);
    return 0;
}

double  JSON::getValueDouble(const char* field, treeStr* T)
{
    GET_FIELD();
    if (v && v->numb && v->data) return getValueDouble(v->numb, v->data);
    return 0;
}

char* JSON::getValueStr(const char* field, treeStr* T)
{
    GET_FIELD();
    if (v && v->numb && v->data) return getValueStr(v->numb, v->data);
    return NULL;
}

list* JSON::getValueMas(const char* field, treeStr* T)
{
    GET_FIELD();
    if (v && v->numb && v->data) return getValueMas(v->numb, v->data);
    return NULL;
}

treeStr* JSON::getValueObj(const char* field, treeStr* T)
{
    GET_FIELD();
    if (v && v->numb && v->data) return getValueObj(v->numb, v->data);
    return NULL;
}

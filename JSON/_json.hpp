/* _json.hpp										2020

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "../json.hpp"
#include "../conv_str.hpp"
#include "../conv_numb.hpp"
#include "../file.hpp"
#include <string.h>


#ifdef _WIN32
    #define strdup  _strdup
#endif

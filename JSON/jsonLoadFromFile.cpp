/* jsonLoadFromFile.сpp									2020-05-11

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_json.hpp"


// ------------------------------------------------------------------------------------------------
bool	JSON::LoadFromFile(const char* fileName)
{
    int len;
    char *S = FileToStr(fileName, len);
    bool R = LoadFromStr(S);
    free(S);
    return R;
}

/* jsonLoadFromStr.сpp									2020-05-07

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_json.hpp"


// ------------------------------------------------------------------------------------------------
bool    JSON::LoadFromStr(const char* So, const char **Serr)
{
    if (!So || !*So) return 0;

    char* Sx = (M) ? M->StrDup(So) : strdup(So);
    if (!Sx) return 0;

    char* s = Sx;
    bool R = ParseObject(&s, &Tj);
    if (M) M->Free(Sx); else free(Sx);

    if (!R) {
        if (Serr) *Serr = So + (s-Sx);
//        __logErr("> %.300s", So + (s - Sx));
    }

    return R;
}

// ------------------------------------------------------------------------------------------------
bool    JSON::ParseObject(char **s, treeStr** To)
{
    const char *Sn = nextChar(s);    // пропустить разделители

    if (!Sn || !*Sn) return 0;
    if (*Sn != '{') return 0;

    (*s)++;
    Sn = nextChar(s);
    if (*Sn == '}') {
        // пустой объект
        (*s)++;
        return 1;
    }

    treeStr* T = newTree();
    if (!T) return 0;
    *To = T;

    for (; ParseElement(s, T);) {
        Sn = nextChar(s);
        if (!Sn || !*Sn) break;

        if (*Sn == ',') {
            (*s)++;
            continue;
        }

        if (*Sn == '}') {
            (*s)++;
            return 1;
        }

        break;
    }
    return 0;
}

const char* JSON::nextChar(char **s)
{
    const char* Sn;
    for (;;(*s)++) {
        Sn = *s;
        if (*Sn == 0) break;
        if (*Sn > 0 && *Sn <= ' ') continue;
        break;
    }
    return Sn;
}

// ------------------------------------------------------------------------------------------------
bool    JSON::ParseElement(char** s, treeStr*T)
{
    const char* Sn = nextChar(s);    // пропустить разделители
    if (!Sn || !*Sn) return 0;
    if (*Sn != '"') return 0;

    char *Name = parseString(s);
    if (!Name || !*Name) return 0;
    Sn = nextChar(s);
    if (*Sn != ':') return 0;
    (*s)++;
    Sn = nextChar(s);

    switch (*Sn) {
        case '"':
        {
            char* StrValue = parseString(s);
            if (!StrValue) return 0;
            nodeStr v = T->Insert(Name);
            if (!v || v->data || v->numb) return 0;
            v->data = (M) ? M->StrDup(StrValue) : strdup(StrValue);
            v->numb = (int)JSON::typeValueJSON::STR;
            break;
        }
        case '{':
        {
            treeStr* Tx = NULL;
            if (!ParseObject(s, &Tx)) return 0;
            nodeStr v = T->Insert(Name);
            if (!v || v->data || v->numb) return 0;
            v->data = Tx;
            v->numb = (int)JSON::typeValueJSON::OBJ;
            break;
        }
        case '[':
        {
            list* Lx = NULL;
            if (!ParseMassive(s, &Lx)) return 0;
            nodeStr v = T->Insert(Name);
            if (!v || v->data || v->numb) return 0;
            v->data = Lx;
            v->numb = (int)JSON::typeValueJSON::MAS;
            break;
        }
        default:
        {
            typeValueJSON typeValue = JSON::typeValueJSON::Empty;
            char* StrValue = parseValue(s, typeValue);
            if (!StrValue) return 0;
            nodeStr v = T->Insert(Name);
            if (!v || v->data || v->numb) return 0;
            v->data = StrValue;
            v->numb = (int)typeValue;
            break;
        }
    }
    return 1;
}

// ------------------------------------------------------------------------------------------------
char* JSON::parseValue(char** s, typeValueJSON& t)
{
    char *se = strpbrk(*s, ",]}");
    if (!se) return NULL;

    int len = (int)(se - *s);
    if (len < 1) return NULL;

    char* Sr = (char*)((M) ? M->Calloc(len + 1) : calloc(1, len + 1));
    if (!Sr) return 0;

    strncpy(Sr, *s, len);
    convStr_clearSpace(Sr);
    *s = se;

    if (checkValue(Sr, t)) return Sr;

    if (M) M->Free(Sr); else free(Sr);
    return NULL;
}

bool    JSON::checkValue(char* s, typeValueJSON& t)
{
    if (!strcmp(s, "null")) {
        t = JSON::typeValueJSON::VALUE_Null;
        return 1;
    }
    if (!strcmp(s, "true")) {
        t = JSON::typeValueJSON::VALUE_True;
        return 1;
    }
    if (!strcmp(s, "false")) {
        t = JSON::typeValueJSON::VALUE_False;
        return 1;
    }

    bool isDouble = 0;
    for (char* si = s; *si; si++) {
        if (*si >= '0' && *si <= '9') continue;
        if (*si=='+' || *si=='-') continue;
        if (*si == 'e' || *si == 'E' || *si == '.') {
            isDouble = 1;
            continue;
        }
        return 0;
    }

    char* endptr=NULL;
    double d = strtod(s, &endptr);
    if (endptr > s && *endptr == 0) {
        t = (isDouble) ? JSON::typeValueJSON::VALUE_Double: JSON::typeValueJSON::VALUE_Number;
        return 1;
    }

    return 0;
}


// ------------------------------------------------------------------------------------------------
char* JSON::parseString(char** s)
{
    if (**s != '"') return NULL;
    (*s)++;

    char* Sr=*s;
    char* se=Sr;
    for (;;) {
        se = strchr(*s, '"');
        if (!se) return NULL;
        if (*(se - 1) == '\\') {
            se++;
            continue;
        }
        break;
    }
    *se = 0;
    *s = se+1;

    return Sr;
}


// ------------------------------------------------------------------------------------------------
bool    JSON::ParseMassive(char** s, list** Lo)
{
    const char* Sn = nextChar(s);    // пропустить разделители

    if (!Sn || !*Sn) {
        return 0;
    }
    if (*Sn != '[') {
        return 0;
    }

    (*s)++;
    Sn = nextChar(s);
    if (*Sn == ']') {
        // пустой объект
        (*s)++;
        return 1;
    }

    list* L = newList();
    if (!L) {
        return 0;
    }
    *Lo = L;

    for (; ParseElement(s, L);) {
        Sn = nextChar(s);
        if (!Sn || !*Sn) break;

        if (*Sn == ',') {
            (*s)++;
            continue;
        }

        if (*Sn == ']') {
            (*s)++;
            return 1;
        }

        break;
    }
    return 0;
}


// ------------------------------------------------------------------------------------------------
bool    JSON::ParseElement(char** s, list* L)
{
    const char* Sn = nextChar(s);    // пропустить разделители
    if (!Sn || !*Sn) return 0;

    switch (*Sn) {
        case '"':
        {
            char* StrValue = parseString(s);
            if (!StrValue) return 0;
            nodeList v = L->AddLast();
            if (!v) return 0;
            v->data = (M) ? M->StrDup(StrValue) : strdup(StrValue);
            v->numb = (int)JSON::typeValueJSON::STR;
            break;
        }
        case '{':
        {
            treeStr* Tx = NULL;
            if (!ParseObject(s, &Tx)) return 0;
            nodeList v = L->AddLast();
            if (!v) return 0;
            v->data = Tx;
            v->numb = (int)JSON::typeValueJSON::OBJ;
            break;
        }
        case '[':
        {
            list* Lx = NULL;
            if (!ParseMassive(s, &Lx)) return 0;
            nodeList v = L->AddLast();
            if (!v) return 0;
            v->data = Lx;
            v->numb = (int)JSON::typeValueJSON::MAS;
            break;
        }
        default:
        {
            typeValueJSON typeValue = JSON::typeValueJSON::Empty;
            char* StrValue = parseValue(s, typeValue);
            if (!StrValue) return 0;
            nodeList v = L->AddLast();
            if (!v) return 0;
            v->data = StrValue;
            v->numb = (int)typeValue;
            break;
        }
    }
    return 1;
}

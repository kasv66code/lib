/* memClear.cpp								2014-03-21

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_mem.hpp"


// обнуление структур без освобождения памяти
void	_Mem::memClear(void)
{
    LockMem();

    _Mem::treeAlloc->treeClear();
    _Mem::treeFree->treeClear();

    _Mem::TableStructuresOfFragment_CloseAll();
    _Mem::TableNodes_CloseAll();
    _Mem::block_CloseAll();

#if enaStatistic==1
	_Mem::countNodes_Curr = 0;
	_Mem::countFragments_Curr = 0;
	_Mem::memorySize_Curr = 0;
	_Mem::countNodesTbl_Curr = 0;
	_Mem::countFragmentsTbl_Curr = 0;
	_Mem::countBlocks_Curr = 0;
#endif

    _Mem::TableStructuresOfFragment_Create();
    _Mem::TableNodes_Create();
    _Mem::block_Create( 0 );

    unLockMem();
}


// быстрое освобождение всей выделенной памяти
void	 _Mem::memFree(void)
{
    LockMem();

    TableStructuresOfFragment_CloseAll();
    TableNodes_CloseAll();
    block_CloseAll();

    treeAlloc->treeFree();
    treeFree->treeFree();

#if enaStatistic==1
	_Mem::countNodes_Curr = 0;
	_Mem::countFragments_Curr = 0;
	_Mem::memorySize_Curr = 0;
	_Mem::countNodesTbl_Curr = 0;
	_Mem::countFragmentsTbl_Curr = 0;
	_Mem::countBlocks_Curr = 0;
#endif

    _Mem::TableStructuresOfFragment_Create();
    _Mem::TableNodes_Create();
    _Mem::block_Create( 0 );

    unLockMem();
}

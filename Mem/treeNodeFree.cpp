/* treeNodeFree.cpp							2013-10-30

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/



#include "_tree.hpp"



// освобождение памяти структуры node -------------------------------------------------------------
void	tree::treeNodeFree(node q)
{
    if (q) {
        if (M) M->node_Free(q);
        else free(q);
    }
}

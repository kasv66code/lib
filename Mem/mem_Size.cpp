/* mem_Size.cpp									15.10.2013

    Author: Kachkovsky Sergey V.
    kasv@list.ru

		Методы класса локального распределения памяти
			- mem_Size()
*/


#include "_mem.hpp"



// ------------------------------------------------------------------------------------------------
int	_Mem::_mem_Size(void *addr)
{
    mem_StructureOfFragment_t qx, *q;

    qx.fragment_Address = addr;

    q = (mem_StructureOfFragment_t *)_Mem::treeAlloc->treeFind(&qx);
    if (q)
	return q->fragment_Size;

    return 0;
}

// Функция возвращает выделенный размер фрагмента памяти ------------------------------------------
int	_Mem::mem_Size(void* addr)
{
    int R;
    LockMem();
    R = _mem_Size(addr);
    unLockMem();
    return R;
}

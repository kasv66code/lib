/* mem_Calloc.cpp								15.10.2013

    Author: Kachkovsky Sergey V.
    kasv@list.ru

		Методы класса локального распределения памяти
			- mem_Calloc()
*/


#include "_mem.hpp"



// Функция выделения и очистки памяти в блоке -----------------------------------------------------
void	*_Mem::mem_Calloc(int len)
{
    void *addr = _Mem::mem_Alloc(len);
    if (addr)
	memset(addr, 0, len);
    return addr;
}
// ------------------------------------------------------------------------------------------------

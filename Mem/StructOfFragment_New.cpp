/* StructOfFragment_New.cpp							16.10.2013

    Author: Kachkovsky Sergey V.
    kasv@list.ru

		Методы класса локального распределения памяти
			- StructOfFragment_New()
*/


#include "_mem.hpp"


/* Функция выделяет структуру для описания фрагмента ----------------------------------------------
	1. Если есть освобожденные структуры - то взять одну из них
	2. Если нет свободных структур в таблице структур описания фрагментов -
	   то выделить новую таблицу
	3. Взять очередную структуру из таблицы структур описания фрагментов
 ----------------------------------------------------------------------------------------------- */
mem_StructureOfFragment_t	*_Mem::StructOfFragment_New(void)
{
    mem_StructureOfFragment_t *q=NULL;

    if (_Mem::list_StructureOfFragmentFree) {
	q = _Mem::list_StructureOfFragmentFree;
	_Mem::list_StructureOfFragmentFree = q->next_ListOfFreeStructureOfFragment;
    } else {
	if (!_Mem::table_StructuresOfFragment || _Mem::table_StructuresOfFragment->masCountUsed == _Mem::table_StructuresOfFragment->masSize)
	    _Mem::TableStructuresOfFragment_Create();

	q = &(_Mem::table_StructuresOfFragment->mas[_Mem::table_StructuresOfFragment->masCountUsed]);
	_Mem::table_StructuresOfFragment->masCountUsed++;
    }
    memset(q, 0, sizeof(mem_StructureOfFragment_t));
    return q;
}
// ------------------------------------------------------------------------------------------------

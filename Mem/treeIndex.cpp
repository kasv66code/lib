/* treeIndex.cpp							2013-10-30

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/



#include "_tree.hpp"


// возвращает элемент массива по индексу idx
const void	*tree::treeIndex(int idx)
{
    if (idx < 0) __exValue();
    if (idx >= tree::count) __exValue();

    if (tree::update) {
        tree::treeMasMake();
    }

    return tree::mas[idx];
}

/* block_Create.cpp								16.10.2013

    Author: Kachkovsky Sergey V.
    kasv@list.ru

		Методы класса локального распределения памяти
			- block_Create()
*/


#include "_mem.hpp"


/* Функция создает новый блок памяти --------------------------------------------------------------
	јдрес предыдущего блока заносится в ->block_Prev
 ----------------------------------------------------------------------------------------------- */
void	_Mem::block_Create(int uLen)
{
    int blockHeaderSize = sizeUstifyFragment(sizeof(mem_Block_t));
    int size = (!_Mem::block) ? _Mem::blockSizeFirst : _Mem::blockSizeNext;

    if (uLen > 0) {
	uLen = sizeUstifyFragment(uLen);
	if (size < uLen + blockHeaderSize)
	    size = uLen + blockHeaderSize;
    }

    mem_Block_t *q = (mem_Block_t *)malloc(size);
    if (!q) __exAlloc();

    memset(q, 0, blockHeaderSize);
    q->block_Prev = this->block;
    q->remain_Address = this->address_Offset(q, blockHeaderSize);
    q->remain_Size = size - blockHeaderSize;

    this->block = q;

#if enaStatistic==1
	_Mem::countBlocks_Curr++;
	if (_Mem::countBlocks_Curr > _Mem::countBlocks_Max) {
	    _Mem::countBlocks_Max = _Mem::countBlocks_Curr;
	}
#endif
}

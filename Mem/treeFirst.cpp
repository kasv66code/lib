/* treeFirst.cpp							2014-03-17

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/



#include "_tree.hpp"



// Поиск первого ключа в дереве -------------------------------------------------------------------
const void	*tree::treeFirst(void)
{
    if (tree::root) {
	node root = tree::root;

	while (root->left) {
	    root = root->left;
	}

	return root->key;
    }
    return NULL;
}
// ------------------------------------------------------------------------------------------------

/* fragment_Free.cpp								16.10.2013

    Author: Kachkovsky Sergey V.
    kasv@list.ru

		Методы класса базового локального распределения памяти
			- fragment_Free()
*/



#include "_mem.hpp"


/* Функция заносит освобождаемый фрагмент памяти в список освобожденных фрагментов и --------------
   по возможности выполняет слияние со смежными фрагментами в блоке или 
   с остатком блока, если освобождается последний фрагмент
 ----------------------------------------------------------------------------------------------- */
void	_Mem::fragment_Free(mem_StructureOfFragment_t *q)
{
    q->fragment_Len = 0;

#if enaStatistic==1
	if (_Mem::countFragments_Curr)
	    _Mem::countFragments_Curr--;
#endif

    // Возможные варианты:
    // A. Слияние с остатком в последнем блоке (+ возможно слияние со свободным фрагментом слева) - для последнего фрагмента в последнем блоке
    // B. Слияние и слева и справа в один фрагмент
    // C. Слияние только слева
    // D. Слияние только справа
    // E. Без слияний - простое освобождение фрагмента

    // Проверяем вариант A - для последнего фрагмента в последнем блоке возможность слияния с остатком
    if (_Mem::_check_IntegrationWithRemain(q)) {
	_Mem::_IntegrationWithRemain(q);
	return;
    }

    // Проверяем вожможность слияния для вариантов B-D
    if (_Mem::_check_IntegrationWithAdjacent(q)) {
	_Mem::_IntegrationWithAdjacent(q);
	return;
    }

    // Вариянт E - без слияний - простое освобождение фрагмента
    _Mem::fragment_ListFreeOfSize_Append(q);
}
// ------------------------------------------------------------------------------------------------


// Проверяем, является ли освобождаемый фрагмент последним в последнем блоке
int	_Mem::_check_IntegrationWithRemain(mem_StructureOfFragment_t *q)
{
    return (q->nextFragmentOfHisBlock == NULL && q->block_His == _Mem::block && block->fragment_Last == q && _Mem::block->remain_Size>0)?1:0;
}

// Слияние освобождаемого фрагмента с остатком последнего блока
void	_Mem::_IntegrationWithRemain(mem_StructureOfFragment_t *q)
{
    mem_StructureOfFragment_t *qPrev = q->prevFragmentOfHisBlock;
    mem_Block_t *block = q->block_His;

    // если предыдущий фрагмент освобожденный, то выполнить слияние двух блоков
    if (qPrev && qPrev->fragment_Len==0) {
	// оключить из списка свободных размеров блок слева
//__logDbg("_IntegrationWithRemain A");
	_Mem::fragment_ListFreeOfSize_Delete(qPrev);

	// суммировать размер в первом блоке
	qPrev->fragment_Size += q->fragment_Size;

	// установить конец списка
	qPrev->nextFragmentOfHisBlock = NULL;

	// освободить более не нужную структуру описания фрагмента
	_Mem::StructOfFragment_Free(q);

	// далее работаем с объединенным блоком
	q = qPrev;
    }

    // слияние освобождаемого фрагмента с остатком блока

    // суммировать размер
    block->remain_Size += q->fragment_Size;

    // перенести адрес начала оставшейся нераспределенной памяти
    block->remain_Address = q->fragment_Address;

    // скорректировать список фрагментов внутри блока
    qPrev = q->prevFragmentOfHisBlock;
    if (qPrev)
	qPrev->nextFragmentOfHisBlock = NULL;

    // адрес структуры последнего фрагмента в этом блоке
    block->fragment_Last = qPrev;

    // освободить более не нужную структуру описания фрагмента
    _Mem::StructOfFragment_Free(q);
}

int	_Mem::_check_IntegrationWithAdjacent(mem_StructureOfFragment_t *q)
{
    mem_StructureOfFragment_t *qPrev = q->prevFragmentOfHisBlock, *qNext = q->nextFragmentOfHisBlock;

    if (qPrev && qPrev->fragment_Len==0 && qPrev->block_His == q->block_His)
	return 1;
    if (qNext && qNext->fragment_Len==0 && qNext->block_His == q->block_His)
	return 1;

    return 0;
}

void	_Mem::_IntegrationWithAdjacent(mem_StructureOfFragment_t *q)
{
    mem_StructureOfFragment_t *qPrev = q->prevFragmentOfHisBlock, *qNext = q->nextFragmentOfHisBlock;

    // B. Слияние и слева и справа в один фрагмент
    if (qPrev && qNext && qPrev->fragment_Len==0 && qNext->fragment_Len==0 && qPrev->block_His == q->block_His && qNext->block_His == q->block_His) {
	// оключить из списка свободных размеров блок слева
//__logDbg("_IntegrationWithAdjacent B1");
	_Mem::fragment_ListFreeOfSize_Delete(qPrev);

	// оключить из списка свободных размеров блок справа
//__logDbg("_IntegrationWithAdjacent B2");
	_Mem::fragment_ListFreeOfSize_Delete(qNext);

	// объединить все три фрагмента в один
	// суммировать размер
	qPrev->fragment_Size += q->fragment_Size + qNext->fragment_Size;

	// скорректировать список фрагментов внутри блока
	qPrev->nextFragmentOfHisBlock = qNext->nextFragmentOfHisBlock;
	if (qPrev->nextFragmentOfHisBlock)
	    qPrev->nextFragmentOfHisBlock->prevFragmentOfHisBlock = qPrev;

	// освободить более не нужные структуры описания фрагментов
	_Mem::StructOfFragment_Free(qNext);
	_Mem::StructOfFragment_Free(q);

	// занести объединенный фрагмент в список освобожденных фрагментов
	_Mem::fragment_ListFreeOfSize_Append(qPrev);
	return;
    }

    // C. Слияние только слева
    if (qPrev && qPrev->fragment_Len==0 && qPrev->block_His == q->block_His) {
	// оключить из списка свободных размеров блок слева
//__logDbg("_IntegrationWithAdjacent C");
	_Mem::fragment_ListFreeOfSize_Delete(qPrev);

	// объединить оба фрагмента в один
	// суммировать размер
	qPrev->fragment_Size += q->fragment_Size;

	// скорректировать список фрагментов внутри блока
	qPrev->nextFragmentOfHisBlock = qNext;
	if (qNext)
	    qNext->prevFragmentOfHisBlock = qPrev;

	// освободить более не нужные структуры описания фрагментов
	_Mem::StructOfFragment_Free(q);

	// занести объединенный фрагмент в список освобожденных фрагментов
	_Mem::fragment_ListFreeOfSize_Append(qPrev);
	return;
    }

    // D. Слияние только справа
    if (qNext && qNext->fragment_Len==0 && qNext->block_His == q->block_His) {

    // оключить из списка свободных размеров блок справа
//__logDbg("_IntegrationWithAdjacent D");
	_Mem::fragment_ListFreeOfSize_Delete(qNext);

	// объединить оба фрагмента в один
	// суммировать размер
	q->fragment_Size += qNext->fragment_Size;

	// скорректировать список фрагментов внутри блока
	q->nextFragmentOfHisBlock = qNext->nextFragmentOfHisBlock;
	if (q->nextFragmentOfHisBlock)
	    q->nextFragmentOfHisBlock->prevFragmentOfHisBlock = q;

	// освободить более не нужные структуры описания фрагментов
	_Mem::StructOfFragment_Free(qNext);

	// занести объединенный фрагмент в список освобожденных фрагментов
	_Mem::fragment_ListFreeOfSize_Append(q);
	return;
    }

    __exAlgorithm();
}

/*  _Mem-base.hpp									2020-04-12

			Локальное распределение памяти

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#ifndef	mem_base_hpp
#define	mem_base_hpp


#include "tree-base.hpp"
//#include <new>


#include <mutex> 
using namespace std;


#define enaStatistic	0


// ************************************************************************************************

// Структура описания блока
typedef struct mem_Block_s {
    struct mem_Block_s	*block_Prev;		// адрес предыдущего блока памяти
    void		*remain_Address;	// адрес начала оставшейся нераспределенной памяти
    int			 remain_Size;		// размер остатка нераспределенной памяти в блоке
    void		*fragment_Last;		// адрес структуры последнего фрагмента в этом блоке (актуально только для последнего, текущего блока - пока есть остаток нераспределенной памяти в блоке)
} mem_Block_t;


// Структура описания фрагмента
typedef struct mem_StructureOfFragment_s {
    void	*fragment_Address;	// адрес фрагмента памяти размером size

    int		fragment_Size;		// размер выделенного фрагмента памяти
    int		fragment_Len;		// >0 используемый размер памяти; 0 не используется, освобожден

    mem_Block_t	*block_His;		// адрес структуры своего блока

    struct mem_StructureOfFragment_s	*prevFragmentOfHisBlock, *nextFragmentOfHisBlock;	// указатели для списка структур выделенных фрагментов (в пределах своего блока!)
    struct mem_StructureOfFragment_s	*prevFreeOfSize, *nextFreeOfSize;			// указатели для списка структур освобожденной памяти одного размера (в пределах памяти всех блоков)
    struct mem_StructureOfFragment_s	*next_ListOfFreeStructureOfFragment;
} mem_StructureOfFragment_t;


// Таблица фрагментов
typedef struct mem_TableStructuresOfFragment_s {
    struct mem_TableStructuresOfFragment_s	*prevTableStructuresOfFragment;	// адрес предыдущей таблицы фрагментов
    int				masCountUsed;			// количество использованных элементов массива
    int				masSize;
    mem_StructureOfFragment_t	*mas;
} mem_TableStructuresOfFragment_t;


// Таблица node
typedef struct mem_TableNodes_s {
    struct mem_TableNodes_s	*prevTableNodes;	// адрес предыдущей таблицы node
    int			masCountUsed;		// количество использованных элементов массива
    int			masSize;
    struct node_t	*mas;
} mem_TableNodes_t;

// ************************************************************************************************



class _Mem {
    public:
	_Mem(void);		// размеры блоков по-умолчанию
	// конструкторы с указанием размеров блоков
	_Mem(int _blockSizeFirst);
	_Mem(int _blockSizeFirst, int _tblFragmentSizeFirst, int _tblNodeSizeFirst);
	_Mem(int _blockSizeFirst, int _blockSizeNext, int _tblFragmentSizeFirst, int _tblFragmentSizeNext, int _tblNodeSizeFirst, int _tblNodeSizeNext);
	~_Mem(void);

	void	 memClear(void);	// обнуление структур без освобождения памяти
	void	 memFree(void);		// быстрое освобождение всей выделенной памяти

	void	*mem_Alloc(int size);	// выделить память
	void	*mem_Calloc(int size);	// выделить память и заполнить 0
	void	 mem_Free(void *addr);	// освободить память
	int	 mem_Size(void *addr);	// возвращает выделенный размер фрагмента памяти
	int	 mem_Len(void *addr);	// возвращает запрошенный размер фрагмента памяти
	char	*mem_StrDup(const char *Str);	// создает дубликат строки

#if enaStatistic==1
	const char	*name;
#endif

    private:
	mutex	*lockMEM;
	mutex	*lockMEM2;

	void	*_mem_Alloc(int size);	// выделить память
	int	 _mem_Size(void *addr);	// возвращает выделенный размер фрагмента памяти
	int	 _mem_Len(void *addr);	// возвращает запрошенный размер фрагмента памяти

	// свойства -------------------------------------------------------------------------------
	mem_Block_t	*block;		// текущий (последний) блок
	// размеры блоков
	int blockSizeFirst, blockSizeNext, tblFragmentSizeFirst, tblFragmentSizeNext, tblNodeSizeFirst, tblNodeSizeNext;

	tree	*treeAlloc;	// дерево выделенных фрагментов
	tree	*treeFree;	// дерево освобожденных размеров

	mem_StructureOfFragment_t	*list_StructureOfFragmentFree;	// список освобожденных структур описания фрагментов
	mem_TableStructuresOfFragment_t	*table_StructuresOfFragment;	// текущая (последняя) таблица описаний фрагментов

	node			list_NodeFree;		// список освобожденных node
	mem_TableNodes_t	*table_Nodes;		// текущая (последняя) таблица описаний node

	// методы ---------------------------------------------------------------------------------
	void	*address_Offset(void *addr, int offset);	// функция изменяет адрес на offset байт

	void	TableNodes_Create(void);	// создать новую таблицу описаний node
	void	TableNodes_CloseAll(void);	// освободить память всех таблиц описаний node

	mem_StructureOfFragment_t	*StructOfFragment_New();	// выделить структуру для описания фрагмента
	void	StructOfFragment_Free(mem_StructureOfFragment_t *);	// освободить структуру для описания фрагмента (занести в список свободных структур)
	void	TableStructuresOfFragment_Create(void);			// создать новую таблицу описаний фрагментов
	void	TableStructuresOfFragment_CloseAll(void);		// освободить память всех таблиц описаний фрагментов

	void	block_Create(int uLen);		// выделение нового блока
	void	block_CloseAll(void);		// освободить память всех блоков

	void	fragment_Free(mem_StructureOfFragment_t *);
	void	fragment_ListFreeOfSize_Append(mem_StructureOfFragment_t *);
	void	fragment_ListFreeOfSize_Delete(mem_StructureOfFragment_t *);

	int	_check_IntegrationWithRemain(mem_StructureOfFragment_t *q);
	void	_IntegrationWithRemain(mem_StructureOfFragment_t *q);

	int	_check_IntegrationWithAdjacent(mem_StructureOfFragment_t *q);
	void	_IntegrationWithAdjacent(mem_StructureOfFragment_t *q);

	mem_StructureOfFragment_t	*fragment_LoadFromFree(int size);
	mem_StructureOfFragment_t	*fragment_Create(int size);
	mem_StructureOfFragment_t	*fragment_MakeInBlock(int size);

	void	memInit(void);


#if enaStatistic==1
	// статистика -----------------------------------------------------------------------------
	int	countNodes_Curr;
	int	countFragments_Curr;
	int	memorySize_Curr;
	int	countNodesTbl_Curr;
	int	countFragmentsTbl_Curr;
	int	countBlocks_Curr;

	int	countNodes_Max;
	int	countFragments_Max;
	int	memorySize_Max;
	int	countNodesTbl_Max;
	int	countFragmentsTbl_Max;
	int	countBlocks_Max;
#endif

	friend class tree;
    private:
	node	node_New();		// выделить структуру для node
	void	node_Free(node);	// освободить структуру для node
};



#endif

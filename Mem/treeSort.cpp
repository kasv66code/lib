/* treeSort.cpp								2013-10-30

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/



#include "_tree.hpp"



// сортировка массива
void	tree::treeSort(__tree_sort_compare_t fsort_compare)
{
    if (!fsort_compare) __exNull();

    if (tree::update)
	tree::treeMasMake();

    if (tree::count>1)
	qsort(tree::mas, tree::count, sizeof(void *), fsort_compare);
}

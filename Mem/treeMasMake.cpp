/* treeMasMake.cpp							2013-10-30

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/



#include "_tree.hpp"



void	tree::treeMasMake(void)
{
    if (tree::count>0) {
	int size = tree::count * sizeof(void*);
	if (!tree::mas || !M || M->mem_Size(tree::mas)<size) {
	    if (tree::mas) {
		if (M) M->mem_Free(tree::mas);
		else free(tree::mas);
	    }
	    tree::mas = (const void **)((M)?M->mem_Alloc(size):malloc(size));
	}
	tree::i = 0;
	tree::treeMasWalk(tree::root);
    }
    tree::update = false;
}

/* TableStructuresOfFragment_CloseAll.cpp					16.10.2013

    Author: Kachkovsky Sergey V.
    kasv@list.ru

		Методы класса локального распределения памяти
			- TableStructuresOfFragment_CloseAll()
*/


#include "_mem.hpp"


// Функция освобождает память всех таблиц описаний фрагментов -------------------------------------
void	_Mem::TableStructuresOfFragment_CloseAll(void)
{
    mem_TableStructuresOfFragment_t *q, *qfree;

    for(q=_Mem::table_StructuresOfFragment; q; free(qfree)) {
	if (q->mas)
	    free(q->mas);
	qfree = q;
	q = q->prevTableStructuresOfFragment;
    }
    _Mem::table_StructuresOfFragment = NULL;
    _Mem::list_StructureOfFragmentFree = NULL;
}
// ------------------------------------------------------------------------------------------------

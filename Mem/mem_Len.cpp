/* mem_Len.cpp									15.10.2013

    Author: Kachkovsky Sergey V.
    kasv@list.ru

		Методы класса локального распределения памяти
			- mem_Len()
*/


#include "_mem.hpp"



// Функция возвращает запрошенный размер выделенного ранее фрагмента памяти -----------------------
int	_Mem::_mem_Len(void *addr)
{
    mem_StructureOfFragment_t qx, *q;

    qx.fragment_Address = addr;

    q = (mem_StructureOfFragment_t *)_Mem::treeAlloc->treeFind(&qx);
    if (q)
	return q->fragment_Len;

    return 0;
}
// ------------------------------------------------------------------------------------------------
// Функция возвращает запрошенный размер выделенного ранее фрагмента памяти -----------------------
int	_Mem::mem_Len(void *addr)
{
    int R;
    LockMem();
    R = _mem_Len(addr);
    unLockMem();
    return R;
}

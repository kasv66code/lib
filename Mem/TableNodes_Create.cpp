/* TableNodes_Create.cpp							15.10.2013

    Author: Kachkovsky Sergey V.
    kasv@list.ru

		Методы класса локального распределения памяти
			- tableNodes_Create()
*/


#include "_mem.hpp"


/* Функция создает новую таблицу node -------------------------------------------------------------
	јдрес предыдущей таблицы заносится в ->prevTableNodes
 ----------------------------------------------------------------------------------------------- */
void	_Mem::TableNodes_Create(void)
{
    mem_TableNodes_t *q = (mem_TableNodes_t *)calloc(1,sizeof(mem_TableNodes_t));
    if (!q) __exAlloc();

    int size = (!_Mem::table_Nodes) ? _Mem::tblNodeSizeFirst : _Mem::tblNodeSizeNext;
    q->mas = (struct node_t *)calloc(1,sizeof(struct node_t)*size);
    if (!q->mas) __exAlloc();

    q->masSize = size;

    q->prevTableNodes = _Mem::table_Nodes;
    q->masCountUsed = 0;
    _Mem::table_Nodes = q;

#if enaStatistic==1
	_Mem::countNodesTbl_Curr++;
	if (_Mem::countNodesTbl_Curr > _Mem::countNodesTbl_Max) {
	    _Mem::countNodesTbl_Max = _Mem::countNodesTbl_Curr;
	}
#endif
}
// ------------------------------------------------------------------------------------------------

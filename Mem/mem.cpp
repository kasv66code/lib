/* _Mem.cpp								16.10.2013

    Author: Kachkovsky Sergey V.
    kasv@list.ru

		Методы класса локального распределения памяти
			- _Mem()
			- ~_Mem()
*/


#include "_mem.hpp"


static int	_compareAlloc(const void *a, const void *b);
static int	_compareSize(const void *a, const void *b);
static void	*_key_copy(_Mem *m, void *, const void *key);



// Конструкторы - инициализация переменных --------------------------------------------------------
void	_Mem::memInit(void)
{
    _Mem::lockMEM = new mutex;
    _Mem::lockMEM2 = new mutex;

    treeAlloc = new tree(this, NULL, false, _compareAlloc, _key_copy, NULL);
    if (!treeAlloc) __exNull();
    treeFree = new tree(this, NULL, false, _compareSize, _key_copy, NULL);
    if (!treeFree) __exNull();

    _Mem::TableStructuresOfFragment_Create();
    _Mem::TableNodes_Create();
    _Mem::block_Create( 0 );

#if enaStatistic==1
    _Mem::name = "";
#endif
}

_Mem::_Mem(void)
{
    memset(this, 0, sizeof(*this));

    _Mem::blockSizeFirst = defaultBlockSize;
    _Mem::blockSizeNext = defaultBlockSize;
    _Mem::tblFragmentSizeFirst = mem_SizeDefault_TableStructuresOfFragment;
    _Mem::tblFragmentSizeNext = mem_SizeDefault_TableStructuresOfFragment;
    _Mem::tblNodeSizeFirst = mem_SizeDefault_TableNodes;
    _Mem::tblNodeSizeNext = mem_SizeDefault_TableNodes;

    _Mem::memInit();
}

static int	_initSizes(int value, int value_default, int value_min, int value_max, int m)
{
    if (!value) return value_default;
    if (value >= value_max) return value_max;
    if (value <= value_min) return value_min;
    if (m) return sizeUstifyBlock(value);
    return sizeUstifyFragment(value);
}

_Mem::_Mem(int _blockSizeFirst, int _blockSizeNext, int _tblFragmentSizeFirst, int _tblFragmentSizeNext, int _tblNodeSizeFirst, int _tblNodeSizeNext)
{
    memset(this, 0, sizeof(*this));

    _Mem::blockSizeFirst = _initSizes(_blockSizeFirst, defaultBlockSize, minBlockSize, maxBlockSize, 1);
    _Mem::blockSizeNext = _initSizes(_blockSizeNext, defaultBlockSize, minBlockSize, maxBlockSize, 1);

    _Mem::tblFragmentSizeFirst = _initSizes(_tblFragmentSizeFirst, mem_SizeDefault_TableStructuresOfFragment, mem_SizeMin_TableStructuresOfFragment, mem_SizeMax_TableStructuresOfFragment, 0);
    _Mem::tblFragmentSizeNext = _initSizes(_tblFragmentSizeNext, mem_SizeDefault_TableStructuresOfFragment, mem_SizeMin_TableStructuresOfFragment, mem_SizeMax_TableStructuresOfFragment, 0);

    _Mem::tblNodeSizeFirst = _initSizes(_tblNodeSizeFirst, mem_SizeDefault_TableNodes, mem_SizeMin_TableNodes, mem_SizeMax_TableNodes, 0);
    _Mem::tblNodeSizeNext = _initSizes(_tblNodeSizeNext, mem_SizeDefault_TableNodes, mem_SizeMin_TableNodes, mem_SizeMax_TableNodes, 0);

    _Mem::memInit();
}
// ------------------------------------------------------------------------------------------------

_Mem::_Mem(int _blockSizeFirst)
{
    memset(this, 0, sizeof(*this));

    _Mem::blockSizeFirst = _initSizes(_blockSizeFirst, defaultBlockSize, minBlockSize, maxBlockSize, 1);
    _Mem::blockSizeNext = defaultBlockSize;
    _Mem::tblFragmentSizeFirst = mem_SizeDefault_TableStructuresOfFragment;
    _Mem::tblFragmentSizeNext = mem_SizeDefault_TableStructuresOfFragment;
    _Mem::tblNodeSizeFirst = mem_SizeDefault_TableNodes;
    _Mem::tblNodeSizeNext = mem_SizeDefault_TableNodes;

    _Mem::memInit();
}

_Mem::_Mem(int _blockSizeFirst, int _tblFragmentSizeFirst, int _tblNodeSizeFirst)
{
    memset(this, 0, sizeof(*this));

    _Mem::blockSizeFirst = _initSizes(_blockSizeFirst, defaultBlockSize, minBlockSize, maxBlockSize, 1);
    _Mem::blockSizeNext = defaultBlockSize;

    _Mem::tblFragmentSizeFirst = _initSizes(_tblFragmentSizeFirst, mem_SizeDefault_TableStructuresOfFragment, mem_SizeMin_TableStructuresOfFragment, mem_SizeMax_TableStructuresOfFragment, 0);
    _Mem::tblFragmentSizeNext = mem_SizeDefault_TableStructuresOfFragment;

    _Mem::tblNodeSizeFirst = _initSizes(_tblNodeSizeFirst, mem_SizeDefault_TableNodes, mem_SizeMin_TableNodes, mem_SizeMax_TableNodes, 0);
    _Mem::tblNodeSizeNext = mem_SizeDefault_TableNodes;

    _Mem::memInit();
}

// ------------------------------------------------------------------------------------------------


static int	_compareAlloc(const void *a, const void *b)
{
    const mem_StructureOfFragment_t *Qa = (const mem_StructureOfFragment_t *)a, *Qb = (const mem_StructureOfFragment_t *)b;
    return (int)((const char *)Qa->fragment_Address - (const char *)Qb->fragment_Address);
}

static int	_compareSize(const void *a, const void *b)
{
    const mem_StructureOfFragment_t *Qa = (const mem_StructureOfFragment_t *)a, *Qb = (const mem_StructureOfFragment_t *)b;
    return (int)(Qa->fragment_Size - Qb->fragment_Size);
}

static void	*_key_copy(_Mem *m, void *qu, const void *key)
{
    return (void *)key;
}
// ------------------------------------------------------------------------------------------------



// ƒеструктор - освобождение памяти ---------------------------------------------------------------
_Mem::~_Mem(void)
{
#if enaStatistic==1
    __logDbg("_Mem %s> statistic: memorySize_Max=%d, countBlocks_Max=%d, countFragments_Max=%d, countFragmentsTbl_Max=%d, countNodes_Max=%d, countNodesTbl_Max=%d",
		name,
		memorySize_Max,
		countBlocks_Max,
		countFragments_Max,
		countFragmentsTbl_Max,
		countNodes_Max,
		countNodesTbl_Max );
#endif

    delete treeAlloc;
    delete treeFree;

    TableStructuresOfFragment_CloseAll();
    TableNodes_CloseAll();
    block_CloseAll();

    if (lockMEM) delete lockMEM;
    if (lockMEM2) delete lockMEM2;
}

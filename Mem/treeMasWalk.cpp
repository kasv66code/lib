/* treeMasWalk.cpp							2013-10-30

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/



#include "_tree.hpp"



void	tree::treeMasWalk(node root)
{
    if (!root)
	return;

    if (root->left)
	tree::treeMasWalk(root->left);

    if (tree::i >= tree::count) __exAlgorithm();

    tree::mas[tree::i] = root->key;
    tree::i++;

    if (root->right)
	tree::treeMasWalk(root->right);
}

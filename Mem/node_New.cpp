/* node_New.cpp									15.10.2013

    Author: Kachkovsky Sergey V.
    kasv@list.ru

		Методы класса локального распределения памяти
			- node_New()
*/


#include "_mem.hpp"


/* Функция выделяет структуру для node ------------------------------------------------------------
	1. Если есть освобожденные структуры - то взять одну из них
	2. Если нет свободных структур в таблице node -
	   то выделить новую таблицу
	3. Взять очередную структуру из таблицы node
 ----------------------------------------------------------------------------------------------- */
node	_Mem::node_New(void)
{
    LockMem2();
#if enaStatistic==1
	_Mem::countNodes_Curr++;
	if (_Mem::countNodes_Curr > _Mem::countNodes_Max) {
	    _Mem::countNodes_Max = _Mem::countNodes_Curr;
	}
#endif

    node q=NULL;

    if (_Mem::list_NodeFree) {
	q = _Mem::list_NodeFree;
	_Mem::list_NodeFree = _Mem::list_NodeFree->left;
    } else {
	if (!_Mem::table_Nodes || _Mem::table_Nodes->masCountUsed == _Mem::table_Nodes->masSize)
	    _Mem::TableNodes_Create();

	mem_TableNodes_t *v = _Mem::table_Nodes;
	q = v->mas + v->masCountUsed;
	v->masCountUsed++;
    }
    unLockMem2();

    memset(q, 0, sizeof(struct node_t));
    return q;
}
// ------------------------------------------------------------------------------------------------

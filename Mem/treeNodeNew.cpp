/* treeNodeNew.cpp							2013-10-30

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/



#include "_tree.hpp"



// Выделение памяти для структуры node ------------------------------------------------------------
node	tree::treeNodeNew()
{
    if (M) return M->node_New();
    return (node)calloc(1, sizeof(struct node_t));
}

/* treeClear.cpp							2014-03-21

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/



#include "_tree.hpp"


void	tree::treeClear(void)
{
    mas = NULL;
    root = NULL;

    update = false;

    count = 0;
    i = 0;
}


void	tree::treeFree(void)
{
    treeMasFree();
    if (root) treeDestroy(root);

    treeClear();
}

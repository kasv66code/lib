/* block_CloseAll.cpp								16.10.2013

    Author: Kachkovsky Sergey V.
    kasv@list.ru

		Методы класса локального распределения памяти
			- block_CloseAll()
*/


#include "_mem.hpp"



// Функция освобождает память всех выделенных блоков ----------------------------------------------
void	_Mem::block_CloseAll(void)
{
    mem_Block_t *q, *qfree;

    for(q=block; q; ) {
	qfree = q;
	q = q->block_Prev;

	free(qfree);
    }

    block = NULL;
}

/* mem_Alloc.cpp								16.10.2013

    Author: Kachkovsky Sergey V.
    kasv@list.ru

		Методы класса локального распределения памяти
			- mem_Alloc()
*/


#include "_mem.hpp"



// ------------------------------------------------------------------------------------------------
void	*_Mem::_mem_Alloc(int len)
{
    if (len<=0)
	return NULL;

    // выровнять размер по заданой границе - TimesSizeOf
    int size = sizeUstifyFragment((len+1));

    mem_StructureOfFragment_t *fragment;

    // поискать подходящий размер в освобожденной памяти
    fragment = _Mem::fragment_LoadFromFree(size);
    if (!fragment) {
	fragment = _Mem::fragment_Create(size);
    }

    fragment->fragment_Len = len;

    // занести в дерево выделенной памяти структуру описания фрагмента
    _Mem::treeAlloc->treeInsert(fragment);

#if enaStatistic==1
	_Mem::memorySize_Curr += fragment->fragment_Size;
	if (_Mem::memorySize_Curr > _Mem::memorySize_Max) {
	    _Mem::memorySize_Max = _Mem::memorySize_Curr;
	}
#endif

    if (!fragment->fragment_Address) __exNull();

    return fragment->fragment_Address;
}

// Функция выделения фрагмента памяти -------------------------------------------------------------
void    *_Mem::mem_Alloc(int len)
{
    void* R;
    LockMem();
    R = _mem_Alloc(len);
    unLockMem();
    return R;
}

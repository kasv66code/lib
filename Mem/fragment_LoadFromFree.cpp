/* fragment_LoadFromFree.cpp							05.03.2015

    Author: Kachkovsky Sergey V.
    kasv@list.ru

		Методы класса локального распределения памяти
			- fragment_LoadFromFree()
*/


#include "_mem.hpp"


/* Функция ищет подходящий фрагмент из ранее освобожденных и выделяет его -------------------------
    1. Искать подходящий размер среди ранее освобожденных фрагментов
    2. Если такой найден - вытащить его из освобожденных
    3. Если размер велик - создать фрагмент из остатка и занести его в освобожденные
------------------------------------------------------------------------------------------------ */
mem_StructureOfFragment_t	*_Mem::fragment_LoadFromFree(int size)
{
    // 1. Искать подходящий размер среди ранее освобожденных фрагментов
    mem_StructureOfFragment_t Qx;
    Qx.fragment_Size = size;
    mem_StructureOfFragment_t *q = (mem_StructureOfFragment_t *)_Mem::treeFree->treeFindGT(&Qx);
    if (!q) {
	// не нашлось подходящего фрагмента
	return NULL;
    }

    q = q->nextFreeOfSize;
    if (!q) __exNull();

    // 2. извлечь фрагмент из освобожденных
    _Mem::fragment_ListFreeOfSize_Delete(q);

    if (q->fragment_Size-size > minFragmentSize) {
	// 3. размер велик - разбить фрагмент на две части, создать фрагмент из остатка и занести его в освобожденные
	// a) выделить структуру описания фрагмента
	mem_StructureOfFragment_t *qFree = _Mem::StructOfFragment_New();

	// b) добавить структуру в список выделенных фрагментов в блоке сразу после выбранного фрагмента
	qFree->prevFragmentOfHisBlock = q;
	qFree->nextFragmentOfHisBlock = q->nextFragmentOfHisBlock;
	qFree->block_His = q->block_His;

	q->nextFragmentOfHisBlock = qFree;
	if (qFree->nextFragmentOfHisBlock)
	    qFree->nextFragmentOfHisBlock->prevFragmentOfHisBlock = qFree;

	// c) задать адрес и скорректировать размер
	qFree->fragment_Address = _Mem::address_Offset(q->fragment_Address, size);
	qFree->fragment_Size = q->fragment_Size - size;
	q->fragment_Size = size;
	q->fragment_Len = size;

	// d) освободить фрагмент с остатком (без слияния со смежными фрагментами!)
	_Mem::fragment_ListFreeOfSize_Append(qFree);
    }

    return q;
}


/*
    if (q->fragment_Size-size > minFragmentSize) return NULL;
*/

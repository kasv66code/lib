/* mem_StrDup.cpp								16.10.2013

    Author: Kachkovsky Sergey V.
    kasv@list.ru

		Методы класса локального распределения памяти
			- mem_Alloc()
*/


#include "_mem.hpp"



// Функция создает дубликат строки ----------------------------------------------------------------
char	*_Mem::mem_StrDup(const char *Str)
{
    if (!Str)
	return NULL;

    int len = strlen(Str)+1;
    char *NewStr = (char *)_Mem::mem_Alloc(len);

    if (NewStr)
	strcpy(NewStr, Str);
    return NewStr;
}
// ------------------------------------------------------------------------------------------------

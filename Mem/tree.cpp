/* tree.cpp								2013-10-30

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/



#include "_tree.hpp"


// Конструктор ------------------------------------------------------------------------------------
tree::tree(class _Mem* m, void* qu, bool destroy, __tree_key_compare_t fkey_compare, __tree_key_copy_t fkey_copy, __tree_key_free_t fkey_free)
{
    memset(this, 0, sizeof(*this));

    if (!fkey_compare || !fkey_copy) __exNull();

    tree::M = m;
    tree::Qu = qu;
    tree::destroy = destroy;

    tree::key_compare = fkey_compare;
    tree::key_copy = fkey_copy;
    tree::key_free = fkey_free;
}


// Деструктор -------------------------------------------------------------------------------------
tree::~tree(void)
{
    if (destroy) {
	treeMasFree();

	if (root)
	    treeDestroy(root);
    }
}

const void* tree::operator [] (int idx)
{
    return tree::treeIndex(idx);
}

int	tree::treeCount(void)
{
    return tree::count;
}

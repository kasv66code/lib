/* TableNodes_CloseAll.cpp							15.10.2013

    Author: Kachkovsky Sergey V.
    kasv@list.ru

		Методы класса локального распределения памяти
			- tableNodes_CloseAll()
*/


#include "_mem.hpp"


// Функция освобождает память всех таблиц описаний node -------------------------------------------
void	_Mem::TableNodes_CloseAll(void)
{
    mem_TableNodes_t *q, *qfree;

    for(q=_Mem::table_Nodes; q;) {
	if (q->mas)
	    free(q->mas);
	qfree = q;
	q = q->prevTableNodes;
	free(qfree);
    }
    _Mem::table_Nodes = NULL;
    _Mem::list_NodeFree = NULL;
}
// ------------------------------------------------------------------------------------------------

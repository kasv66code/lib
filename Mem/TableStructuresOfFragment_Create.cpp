/* TableStructuresOfFragment_Create.cpp						16.10.2013

    Author: Kachkovsky Sergey V.
    kasv@list.ru

		Методы класса локального распределения памяти
			- TableStructuresOfFragment_Create()
*/


#include "_mem.hpp"


/* Функция создает новую таблицу описаний фрагментов ----------------------------------------------
	јдрес предыдущей таблицы заносится в ->prevTableStructuresOfFragment
 ----------------------------------------------------------------------------------------------- */
void	_Mem::TableStructuresOfFragment_Create(void)
{
    mem_TableStructuresOfFragment_t *q = (mem_TableStructuresOfFragment_t *)calloc(1,sizeof(mem_TableStructuresOfFragment_t));
    if (!q) __exAlloc();

    int size = (!_Mem::table_StructuresOfFragment) ? _Mem::tblFragmentSizeFirst : _Mem::tblFragmentSizeNext;
    q->mas = (mem_StructureOfFragment_t *)calloc(1,sizeof(mem_StructureOfFragment_t)*size);
    if (!q->mas) __exAlloc();

    q->masSize = size;

    q->prevTableStructuresOfFragment = _Mem::table_StructuresOfFragment;
    q->masCountUsed = 0;
    _Mem::table_StructuresOfFragment = q;

#if enaStatistic==1
	_Mem::countFragmentsTbl_Curr++;
	if (_Mem::countFragmentsTbl_Curr > _Mem::countFragmentsTbl_Max) {
	    _Mem::countFragmentsTbl_Max = _Mem::countFragmentsTbl_Curr;
	}
#endif
}
// ------------------------------------------------------------------------------------------------

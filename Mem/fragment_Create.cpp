/* fragment_Create.cpp								16.10.2013

    Author: Kachkovsky Sergey V.
    kasv@list.ru

		Методы класса локального распределения памяти
			- fragment_Create()
*/


#include "_mem.hpp"


// Выделяется структура описания фрагмента и в нее заносятся сведения о выделенном фрагменте
mem_StructureOfFragment_t	*_Mem::fragment_MakeInBlock(int size)
{
    // проверить размеры
    {
	int rest = _Mem::block->remain_Size - size;
	if (rest < 0) __exAlgorithm();

	if (rest>0 && rest < minFragmentSize)
	    size = _Mem::block->remain_Size;
    }

    mem_StructureOfFragment_t* q = _Mem::StructOfFragment_New();
    mem_StructureOfFragment_t* qPrev = (mem_StructureOfFragment_t*)(_Mem::block->fragment_Last);

    q->fragment_Address = _Mem::block->remain_Address;
    q->fragment_Size = size;
    q->block_His = _Mem::block;

    // скорректировать остаток в блоке
    _Mem::block->remain_Size -= size;
    if (_Mem::block->remain_Size > 0) {
	_Mem::block->remain_Address = _Mem::address_Offset(_Mem::block->remain_Address, size);
	_Mem::block->fragment_Last = q;
    } else {
	_Mem::block->remain_Address = NULL;
	_Mem::block->fragment_Last = 0;
	_Mem::block->remain_Size = 0;
    }

    // занести новый фрагмент в список выделенных фрагментов в блоке
    if (qPrev) {
	qPrev->nextFragmentOfHisBlock = q;
    }

    q->prevFragmentOfHisBlock = qPrev;

    return q;
}


/* Функция выделяет новый фрагмент ----------------------------------------------------------------
    1. Если блок еще не выделялся - выделить блок
    2. Если недостаточно места в текущем блоке - остаток текущего блока регистрируется
       в дереве освобожденных фрагментов и выделяется новый блок
    3. Выделяется в блоке необходимый фрагмент
------------------------------------------------------------------------------------------------ */
mem_StructureOfFragment_t	*_Mem::fragment_Create(int size)
{
#if enaStatistic==1
	_Mem::countFragments_Curr++;
	if (_Mem::countFragments_Curr > _Mem::countFragments_Max) {
	    _Mem::countFragments_Max = _Mem::countFragments_Curr;
	}
#endif

    if (_Mem::block == NULL || _Mem::block->remain_Size==0) {
	// 1. Если блок еще не выделялся - выделить блок
	_Mem::block_Create(size);
    } else if (_Mem::block->remain_Size < size) {
	// 2. Если недостаточно места в текущем блоке

	// a) остаток текущего блока регистрируется в дереве освобожденных фрагментов

	// выделить новый фрагмент
	mem_StructureOfFragment_t *q = _Mem::fragment_MakeInBlock(_Mem::block->remain_Size);

	// b) выделить новый блок
	_Mem::block_Create(size);

	// занести фрагмент с остатком блока в список освобожденных фрагментов
	_Mem::fragment_Free(q);
    }

    // 3. Выделяем фрагмент из остатка текущего блока
    return _Mem::fragment_MakeInBlock(size);
}

/* treeInsert.cpp							2013-10-30
*/



#include "_tree.hpp"



static void	maybe_split_for_insert(node *rootp, node *parentp, node *gparentp, int p_r, int gp_r, int mode);



// Поиск или добавление ключа в дереве ------------------------------------------------------------
const void	*tree::treeInsert(const void *key)
{
  node q;
  node *parentp = NULL, *gparentp = NULL;
  node *rootp = (node *)(&(tree::root));
  node *nextp;
  int r = 0, p_r = 0, gp_r = 0; /* No they might not, Mr Compiler.  */

  /* This saves some additional tests below.  */
  if (*rootp != NULL)
    (*rootp)->red = 0;

  nextp = rootp;
  while (*nextp != NULL)
    {
      node root = *rootp;
      r = tree::key_compare(key, root->key);
      if (!r)
        return root->key;

      maybe_split_for_insert (rootp, parentp, gparentp, p_r, gp_r, 0);
      /* If that did any rotations, parentp and gparentp are now garbage. That doesn't matter, because the values they contain are never used again in that case.  */

      nextp = r < 0 ? &root->left : &root->right;
      if (*nextp == NULL)
        break;

      gparentp = parentp;
      parentp = rootp;
      rootp = nextp;

      gp_r = p_r;
      p_r = r;
    }

  q = tree::treeNodeNew();

  *nextp = q;
  q->key = tree::key_copy(tree::M, tree::Qu, key);
  q->red = 1;
  q->left = q->right = NULL;

  if (nextp != rootp)
    /* There may be two red edges in a row now, which we must avoid by rotating the tree.  */
    maybe_split_for_insert (nextp, rootp, parentp, r, p_r, 1);

  tree::update = true;
  tree::count++;

  return q->key;
}
// ------------------------------------------------------------------------------------------------



/* Possibly "split" a node with two red successors, and/or fix up two red
   edges in a row.  ROOTP is a pointer to the lowest node we visited, PARENTP
   and GPARENTP pointers to its parent/grandparent.  P_R and GP_R contain the
   comparison values that determined which way was taken in the tree to reach
   ROOTP.  MODE is 1 if we need not do the split, but must check for two red
   edges between GPARENTP and ROOTP.  */
static void	maybe_split_for_insert(node *rootp, node *parentp, node *gparentp, int p_r, int gp_r, int mode)
{
  node root = *rootp;
  node *rp, *lp;
  rp = &(*rootp)->right;
  lp = &(*rootp)->left;

  /* See if we have to split this node (both successors red).  */
  if (mode == 1
      || ((*rp) != NULL && (*lp) != NULL && (*rp)->red && (*lp)->red))
    {
      /* This node becomes red, its successors black.  */
      root->red = 1;
      if (*rp)
        (*rp)->red = 0;
      if (*lp)
        (*lp)->red = 0;

      /* If the parent of this node is also red, we have to do
         rotations.  */
      if (parentp != NULL && (*parentp)->red)
        {
          node gp = *gparentp;
          node p = *parentp;
          /* There are two main cases:
             1. The edge types (left or right) of the two red edges differ.
             2. Both red edges are of the same type.
             There exist two symmetries of each case, so there is a total of
             4 cases.  */
          if ((p_r > 0) != (gp_r > 0))
            {
              /* Put the child at the top of the tree, with its parent
                 and grandparent as successors.  */
              p->red = 1;
              gp->red = 1;
              root->red = 0;
              if (p_r < 0)
                {
                  /* Child is left of parent.  */
                  p->left = *rp;
                  *rp = p;
                  gp->right = *lp;
                  *lp = gp;
                }
              else
                {
                  /* Child is right of parent.  */
                  p->right = *lp;
                  *lp = p;
                  gp->left = *rp;
                  *rp = gp;
                }
              *gparentp = root;
            }
          else
            {
              *gparentp = *parentp;
              /* Parent becomes the top of the tree, grandparent and
                 child are its successors.  */
              p->red = 0;
              gp->red = 1;
              if (p_r < 0)
                {
                  /* Left edges.  */
                  gp->left = p->right;
                  p->right = gp;
                }
              else
                {
                  /* Right edges.  */
                  gp->right = p->left;
                  p->left = gp;
                }
            }
        }
    }
}

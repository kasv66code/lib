/* treeFind.cpp								2013-10-30
*/



#include "_tree.hpp"



// Поиск по совпадению ключа в дереве -------------------------------------------------------------
const void	*tree::treeFind(const void *key)
{
    if (tree::root) {
	node root = tree::root;
	int r;

	while (root) {
	    if (!(r = tree::key_compare(key, root->key)))
		return root->key;
	    root = (r < 0) ? root->left : root->right;
	}
    }
    return NULL;
}
// ------------------------------------------------------------------------------------------------

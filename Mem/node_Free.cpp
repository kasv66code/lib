/* node_Free.cpp								15.10.2013

    Author: Kachkovsky Sergey V.
    kasv@list.ru

		Методы класса локального распределения памяти
			- node_Free()
*/


#include "_mem.hpp"


// Функция освобождает node: заносит node в список свободных node ---------------------------------
void	_Mem::node_Free(node q)
{
    LockMem2();
#if enaStatistic==1
	if (_Mem::countNodes_Curr)
	    _Mem::countNodes_Curr--;
#endif

    memset(q, 0, sizeof(struct node_t));
    q->left = _Mem::list_NodeFree;
    _Mem::list_NodeFree = q;
    unLockMem2();
}
// ------------------------------------------------------------------------------------------------

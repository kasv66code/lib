/* treeMasFree.cpp							2014-05-01

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/



#include "_tree.hpp"



void	tree::treeMasFree(void)
{
    if (tree::mas) {
        if (M) M->mem_Free(tree::mas);
        else free(tree::mas);
    }

    tree::mas = NULL;
    tree::update = true;
}

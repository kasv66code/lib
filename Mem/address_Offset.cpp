/* address_Offset.cpp								15.10.2013

    Author: Kachkovsky Sergey V.
    kasv@list.ru

		Методы класса локального распределения памяти
			- address_Offset()
*/


#include "_mem.hpp"


// Функция изменяет адрес на offset байт ----------------------------------------------------------
void	*_Mem::address_Offset(void *addr, int offset)
{
    char *Mem = (char *)addr;
    Mem += offset;
    return (void *)Mem;
}

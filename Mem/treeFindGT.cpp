/* treeFindGT.cpp							2013-10-30

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/



#include "_tree.hpp"



// Поиск по совпадению или ближайшего большего ключа в дереве -------------------------------------
const void	*tree::treeFindGT(const void *key)
{
    if (tree::root) {
	node rp = NULL;
	node root = tree::root;
	int r;

	while (root) {
	    if (!(r = tree::key_compare(key, root->key)))
		return root->key;
	    if (r<0)
		rp = root;
	    root = (r < 0) ? root->left : root->right;
	}
	if (rp)
	    return rp->key;
    }
    return NULL;
}
// ------------------------------------------------------------------------------------------------

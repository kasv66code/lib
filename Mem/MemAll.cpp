/* MemAll.cpp								                2020-04-12

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#include "_mem.hpp"
#include "../mem.hpp"



mem::mem(int k)
{
    switch (k) {
        case 0:
            M = new _Mem(minBlockSize, mem_SizeMin_TableStructuresOfFragment, mem_SizeMin_TableNodes);
            break;
        default:
        case 1:
            M = new _Mem;
            break;
        case 2:
            M = new _Mem(maxBlockSize / 2, mem_SizeMax_TableStructuresOfFragment / 2, mem_SizeMax_TableNodes / 2);
            break;
    }
}

mem::~mem()
{
    delete M;
}


void	 mem::mClear(void)
{
    M->memClear();
}

void	 mem::mFree(void)
{
    M->memFree();
}

void	 *mem::Alloc(int size)
{
    return M->mem_Alloc(size);
}

void	 *mem::Calloc(int size)
{
    return M->mem_Calloc(size);
}

void	 mem::Free(void* addr)
{
    M->mem_Free(addr);
}

int     mem::Size(void* addr)
{
    return M->mem_Size(addr);
}

int     mem::Len(void* addr)
{
    return M->mem_Len(addr);
}

char	 *mem::StrDup(const char *Str)
{
    return M->mem_StrDup(Str);
}

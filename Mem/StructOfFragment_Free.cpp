/* StructOfFragment_Free.cpp							16.10.2013

    Author: Kachkovsky Sergey V.
    kasv@list.ru

		Методы класса локального распределения памяти
			- StructOfFragment_Free()
*/


#include "_mem.hpp"


// Функция освобождает структуру для описания фрагмента: заносит в список свободных структур ------
void	_Mem::StructOfFragment_Free(mem_StructureOfFragment_t *q)
{
    memset(q, 0, sizeof(mem_StructureOfFragment_t));
    q->next_ListOfFreeStructureOfFragment = _Mem::list_StructureOfFragmentFree;
    _Mem::list_StructureOfFragmentFree = q;
}
// ------------------------------------------------------------------------------------------------

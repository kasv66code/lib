/* treeLast.cpp								2014-03-17

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/



#include "_tree.hpp"



// Поиск последнего ключа в дереве ----------------------------------------------------------------
const void	*tree::treeLast(void)
{
    if (tree::root) {
	node root = tree::root;

	while (root->right) {
	    root = root->right;
	}

	return root->key;
    }
    return NULL;
}
// ------------------------------------------------------------------------------------------------

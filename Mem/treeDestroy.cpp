/* treeDestroy.cpp							2013-10-30

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/



#include "_tree.hpp"



// Рекурсивное освобождение всех элементов дерева -------------------------------------------------
void	tree::treeDestroy(node root)
{
    if (root->left)
	treeDestroy(root->left);
    if (root->right)
	treeDestroy(root->right);

    key_free(M, Qu, (void *)(root->key));
    treeNodeFree(root);
}
// ------------------------------------------------------------------------------------------------

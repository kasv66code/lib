/* mem_Free.cpp									16.10.2013

    Author: Kachkovsky Sergey V.
    kasv@list.ru

		Методы класса базового локального распределения памяти
			- mem_Free()
*/



#include "_mem.hpp"



// Функция освобождает фрагмент памяти ------------------------------------------------------------
void	_Mem::mem_Free(void *addr)
{
    LockMem();
    mem_StructureOfFragment_t qx, *q;
    qx.fragment_Address = addr;
    q = (mem_StructureOfFragment_t *)_Mem::treeAlloc->treeDelete(&qx);
    if (q) {
#if enaStatistic==1
	    if (_Mem::memorySize_Curr > 0) {
		_Mem::memorySize_Curr -= q->fragment_Size;
		if (_Mem::memorySize_Curr < 0) {
		    _Mem::memorySize_Curr = 0;
		}
	    }
#endif
	_Mem::fragment_Free(q);
    }
    unLockMem();
}
// ------------------------------------------------------------------------------------------------

/* fragment_ListFreeOfSize_Delete.cpp						16.10.2013

    Author: Kachkovsky Sergey V.
    kasv@list.ru

		Методы класса базового локального распределения памяти
			- fragment_ListFreeOfSize_Delete()
*/



#include "_mem.hpp"



/* Функция исключает освобожденный фрагмент из списка свободных фрагментов
 ----------------------------------------------------------------------------------------------- */
void	_Mem::fragment_ListFreeOfSize_Delete(mem_StructureOfFragment_t *q)
{
    // 1. Исключить фрагмент из списка освобожденных фрагментов
    mem_StructureOfFragment_t *qNext = q->nextFreeOfSize, *qPrev = q->prevFreeOfSize;
    if (!qPrev) __exNull();

    if (qNext)
	qNext->prevFreeOfSize = qPrev;
    qPrev->nextFreeOfSize = qNext;

    q->prevFreeOfSize = NULL;
    q->nextFreeOfSize = NULL;

    // 2. Если список по этому размеру пуст - то удалить этот размер из дерева
    if (qNext==NULL && qPrev->prevFreeOfSize==NULL) {
	_Mem::treeFree->treeDelete(qPrev);
	_Mem::StructOfFragment_Free(qPrev);
    }
}
// ------------------------------------------------------------------------------------------------

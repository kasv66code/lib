/* treeDelete.cpp							2013-10-30
*/


#include "_tree.hpp"


#define NodeStackSize	100


// Поиск по совпадению ключа в дереве и удаление его ----------------------------------------------
const void	*tree::treeDelete(const void *key)
{
  const void *retval;
  node p, q, r;
  int cmp;
  node *rootp = (node *)(&(tree::root));
  node root, unchained=NULL;
  /* Stack of nodes so we remember the parents without recursion.  It's
     _very_ unlikely that there are paths longer than 40 nodes.  The tree
     would need to have around 250.000 nodes.  */
  int stacksize = NodeStackSize;
  int sp = 0;

    node **nodestack = (node **)alloca(sizeof(node *)*stacksize);
    if (!nodestack) __exAlloc();

    p = *rootp;
    if (p == NULL)
	return NULL;

    while ((cmp = tree::key_compare(key, (*rootp)->key)) != 0) {
      if (sp == stacksize) {
	node **newstack;
	stacksize += NodeStackSize;
	newstack = (node **)alloca(sizeof(node *)*stacksize);
	if (!newstack) __exAlloc();
	nodestack = (node **)memcpy((node **)newstack, (node **)nodestack, sp * sizeof(node *));
      }

      nodestack[sp++] = rootp;
      p = *rootp;
      rootp = ((cmp < 0)
               ? &(*rootp)->left
               : &(*rootp)->right);
      if (*rootp == NULL) {
        return NULL;
      }
    }

  /* We don't unchain the node we want to delete. Instead, we overwrite
     it with its successor and unchain the successor.  If there is no
     successor, we really unchain the node to be deleted.  */

  root = *rootp;
  retval = root->key;
  root->key = NULL;

  r = root->right;
  q = root->left;

  if (q == NULL || r == NULL)
    unchained = root;
  else
    {
      node *parent = rootp, *up = &root->right;
      for (;;) {
	  if (sp == stacksize)	    {
	    node **newstack;
	    stacksize += NodeStackSize;
	    newstack = (node **)alloca(sizeof(node *)*stacksize);
	    if (!newstack) __exAlloc();
	    nodestack = (node **)memcpy((node **)newstack, (node **)nodestack, sp * sizeof(node *));
	  }
          nodestack[sp++] = parent;
          parent = up;
          if ((*up)->left == NULL)
            break;
          up = &(*up)->left;
        }
      unchained = *up;
    }

  /* We know that either the left or right successor of UNCHAINED is NULL.
     R becomes the other one, it is chained into the parent of UNCHAINED.  */
  r = unchained->left;
  if (r == NULL)
    r = unchained->right;
  if (sp == 0)
    *rootp = r;
  else
    {
      q = *nodestack[sp-1];
      if (unchained == q->right)
        q->right = r;
      else
        q->left = r;
    }

  if (unchained != root)
    root->key = unchained->key;

  if (!unchained->red)
    {
      /* Now we lost a black edge, which means that the number of black
         edges on every path is no longer constant.  We must balance the
         tree.  */
      /* NODESTACK now contains all parents of R.  R is likely to be NULL
         in the first iteration.  */
      /* NULL nodes are considered black throughout - this is necessary for
         correctness.  */
      while (sp > 0 && (r == NULL || !r->red))
        {
          node *pp = nodestack[sp - 1];
          p = *pp;
          /* Two symmetric cases.  */
          if (r == p->left)
            {
              /* Q is R's brother, P is R's parent.  The subtree with root
                 R has one black edge less than the subtree with root Q.  */
              q = p->right;
              if (q->red)
                {
                  /* If Q is red, we know that P is black. We rotate P left
                     so that Q becomes the top node in the tree, with P below
                     it.  P is colored red, Q is colored black.
                     This action does not change the black edge count for any
                     leaf in the tree, but we will be able to recognize one
                     of the following situations, which all require that Q
                     is black.  */
                  q->red = 0;
                  p->red = 1;
                  /* Left rotate p.  */
                  p->right = q->left;
                  q->left = p;
                  *pp = q;
                  /* Make sure pp is right if the case below tries to use
                     it.  */
                  nodestack[sp++] = pp = &q->left;
                  q = p->right;
                }
              /* We know that Q can't be NULL here.  We also know that Q is
                 black.  */
              if ((q->left == NULL || !q->left->red) && (q->right == NULL || !q->right->red)) {
                  /* Q has two black successors.  We can simply color Q red.
                     The whole subtree with root P is now missing one black
                     edge.  Note that this action can temporarily make the
                     tree invalid (if P is red).  But we will exit the loop
                     in that case and set P black, which both makes the tree
                     valid and also makes the black edge count come out
                     right.  If P is black, we are at least one step closer
                     to the root and we'll try again the next iteration.  */
                  q->red = 1;
                  r = p;
              } else {
                  /* Q is black, one of Q's successors is red.  We can
                     repair the tree with one operation and will exit the
                     loop afterwards.  */
                  if (q->right == NULL || !q->right->red) {
                      /* The left one is red.  We perform the same action as
                         in maybe_split_for_insert where two red edges are
                         adjacent but point in different directions:
                         Q's left successor (let's call it Q2) becomes the
                         top of the subtree we are looking at, its parent (Q)
                         and grandparent (P) become its successors. The former
                         successors of Q2 are placed below P and Q.
                         P becomes black, and Q2 gets the color that P had.
                         This changes the black edge count only for node R and
                         its successors.  */
                      node q2 = q->left;
                      q2->red = p->red;
                      p->right = q2->left;
                      q->left = q2->right;
                      q2->right = q;
                      q2->left = p;
                      *pp = q2;
                      p->red = 0;
		  } else {
                      /* It's the right one.  Rotate P left. P becomes black,
                         and Q gets the color that P had.  Q's right successor
                         also becomes black.  This changes the black edge
                         count only for node R and its successors.  */
                      q->red = p->red;
                      p->red = 0;

                      q->right->red = 0;

                      /* left rotate p */
                      p->right = q->left;
                      q->left = p;
                      *pp = q;
                  }

                  /* We're done.  */
                  sp = 1;
                  r = NULL;
                }
            }
          else
            {
              /* Comments: see above.  */
              q = p->left;
              if (q->red)
                {
                  q->red = 0;
                  p->red = 1;
                  p->left = q->right;
                  q->right = p;
                  *pp = q;
                  nodestack[sp++] = pp = &q->right;
                  q = p->left;
                }
              if ((q->right == NULL || !q->right->red)
                       && (q->left == NULL || !q->left->red))
                {
                  q->red = 1;
                  r = p;
                }
              else
                {
                  if (q->left == NULL || !q->left->red)
                    {
                      node q2 = q->right;
                      q2->red = p->red;
                      p->left = q2->right;
                      q->right = q2->left;
                      q2->left = q;
                      q2->right = p;
                      *pp = q2;
                      p->red = 0;
                    }
                  else
                    {
                      q->red = p->red;
                      p->red = 0;
                      q->left->red = 0;
                      p->left = q->right;
                      q->right = p;
                      *pp = q;
                    }
                  sp = 1;
                  r = NULL;
                }
            }
          --sp;
        }
      if (r != NULL)
        r->red = 0;
    }

    treeNodeFree(unchained);
    update = true;
    count--;

    if (key_free) {
	key_free(M, Qu, (void *)retval);
	retval = NULL;
    }

    return retval;
}
// ------------------------------------------------------------------------------------------------

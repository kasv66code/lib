/* fragment_ListFreeOfSize_Append.cpp						16.10.2013

    Author: Kachkovsky Sergey V.
    kasv@list.ru

		Методы класса базового локального распределения памяти
			- fragment_ListFreeOfSize_Append()
*/



#include "_mem.hpp"



/* Функция заносит освобожденный фрагмент в дерево свободных фрагментов
   в соответствии с его размером
 ----------------------------------------------------------------------------------------------- */
void	_Mem::fragment_ListFreeOfSize_Append(mem_StructureOfFragment_t *q)
{
    // 1. найти такой же размер среди освобожденных ранее фрагментов
    mem_StructureOfFragment_t *qNext, *qFree = (mem_StructureOfFragment_t *)_Mem::treeFree->treeFind(q);
    if (!qFree) {
	// такой размер еще не добавлялся в освобожденные - добавить
	qFree = _Mem::StructOfFragment_New();
	qFree->fragment_Size = q->fragment_Size;
	qFree = (mem_StructureOfFragment_t *)_Mem::treeFree->treeInsert(qFree);
    }

    // 2. скорректировать список - вставить освобождаемый элемент в начало списка
    qNext = qFree->nextFreeOfSize;
    qFree->nextFreeOfSize = q;
    q->prevFreeOfSize = qFree;
    q->nextFreeOfSize = qNext;
    if (qNext)
	qNext->prevFreeOfSize = q;
}
// ------------------------------------------------------------------------------------------------

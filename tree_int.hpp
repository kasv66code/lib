/* tree_int.hpp										2013-2020

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#ifndef	tree_int_hpp
#define	tree_int_hpp


#include "exceptions.hpp"
#include "mem.hpp"
#include <mutex> 


typedef struct nodeInt_t {
    void		*data;		// структура данных пользователя
    int			numb;		// переменная integer для пользователя

    struct nodeInt_t	*next;		// список для неуникальных записей
    int			key;		// уникальный ключ
} *nodeInt;


typedef int	(*__sort_compare_nodeInt)(const nodeInt *, const nodeInt *);
typedef void	(*__freeData_nodeInt_t)(void *Qx, nodeInt v);
typedef void	(*__newData_nodeInt_t)(void *Qx, nodeInt v);


// --- класс дерева Int ------------------------------------------------------------------
class treeInt {
    public:
	void *operator new(size_t size, class mem *m=NULL);
	void operator delete(void* p, mem* m);
	void operator delete(void *p);

	treeInt(void* _Qx = NULL, __newData_nodeInt_t _newData = NULL, __freeData_nodeInt_t _freeData = NULL);
	~treeInt(void);

	void	    lockClose(void);		// закрыть mutex, если не предполагается вызывать десктруктор

	void	    treeFree(void);
	void	    treeMasFree(void);

	nodeInt	    First(void);
	nodeInt	    Last(void);

	nodeInt	    Find(int key);			// поиск по совпадению - возвращает адрес узла
	nodeInt	    Insert(int key);			// добавить узел - возвращает адрес узла
	void	    Delete(int key);			// удаляет из дерева ключ вместе с данными (если указана __free_nodeStringData)
	nodeInt	    AppendDataInListLast(nodeInt q);	// добавляет элемент в конец списка (для неуникальных записей)
	nodeInt	    AppendDataInListFirst(nodeInt q);	// добавляет элемент в начало списка (для неуникальных записей)

	// формирует массив из элементов дерева и возвращает элемент массива по индексу idx
	nodeInt	operator [] (int idx);

	int	    count(void);

	// формирует и сортирует массив из элементов дерева
	void	    sort(__sort_compare_nodeInt fsort_compare);

	friend void	*_treeIntKeyNew(class _Mem* m, void* qu, const void* key);
	friend void     _treeIntKeyFree(class _Mem* m, void* qu, void* key);
    private:
	tree		*T;
	mem		*M;
	std::mutex	*lockT;

	void	*makeNodeKey(const void* key);
	void	makeNodeKey(nodeInt q, int key);

	void			*Qx;		// структура пользователя
	__newData_nodeInt_t	newData;	// функция для создания данных ключа
	__freeData_nodeInt_t	freeData;	// функция для удаления данных ключа
};
// ------------------------------------------------------------------ класс дерева Int ---



#endif

/* conv_str.hpp										2012-2020

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/



#ifndef	_conv_str_hpp
#define	_conv_str_hpp


// Преобразования строк
int	convStr_toLow_Anscii(char *buff, const char *str);
int	convStr_toLow_UTF(char *buff, const char *str);
int	convWStr_toLow(wchar_t *buff, const wchar_t *str);

char	*convStr_clearSpace(char *s);

const char	*convStr_nybble(const char *p, int *n);	// получает в n шестнадцатиричное число и возвращает указатель на следущий после числа символ
int	convStr_urlDecode(char *buff, const char *url);

int	convStr_urlToDomain(char *buff, const char *url, bool www);
int	convStr_urlToDomainLevel2(char *buff, const char *url);

int	strlenUTF(const char *str);

bool	punycodeDecode(const char *So, char *Sn);
bool	punycodeEncode(const char *So, char *Sn);

int	cmpStr_Low_UTF(const char *str1, const char* str2);


#endif

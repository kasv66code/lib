/*  mem.hpp										2013-2020

			Распределение памяти

    Author: Kachkovsky Sergey V.
    kasv@list.ru
*/


#ifndef	mem_hpp
#define	mem_hpp

#include <new>

class _Mem;
class tree;


class mem {
    public:
	mem(int k=1);
	~mem(void);

	void	*Alloc(int size);		// выделить память, байт
	void	*Calloc(int size);		// выделить память и заполнить 0, байт
	void	Free(void *addr);		// освободить память по ранее выделенному адресу
	int	Size(void *addr);		// возвращает выделенный размер памяти для запрошенного адреса, байт
	int	Len(void *addr);		// возвращает занятый размер памяти для запрошенного адреса, байт
	char	*StrDup(const char *Str);	// создает дубликат строки

	void	mClear(void);			// обнуление структур без освобождения блоков памяти
	void	mFree(void);			// освобождение блоков памяти


	friend class treeInt;
	friend class treeInt64;
	friend class treeTime;
	friend class treeDouble;
	friend class treeStr;
	friend class treeWStr;

    private:
	_Mem* M;
};



#endif
